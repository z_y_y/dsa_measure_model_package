import os
from VesselCenterlineExtraction import *

if __name__ == '__main__':
    root_dir = "D:\\Coding\\0_data\DAS_Benchmark"

    patients_name = []
    neck_diameter_records = []
    for pat_dir in os.listdir(root_dir):
        # if os.path.isdir(os.path.join(root_dir, pat_dir)):
        if pat_dir == "AI-20181213001815(BA)":
            if True:
                print(pat_dir)
                dsa_file = os.path.join(root_dir,pat_dir,"dsa.nii.gz")
                label_file = os.path.join(root_dir,pat_dir,"label.nii.gz")
                src_volume = VolumeImage()
                src_volume.get_image_from_nifti(dsa_file)
                label_volume = VolumeImage()
                label_volume.get_image_from_nifti(label_file)

                max_diameter_points, min_diameter_points = extract_vessel_centerline(src_volume, label_volume)
                save_line_points_to_slicer_landmarks_file(os.path.join(root_dir,pat_dir,"L_neck_max_d.mrk.json"),
                                                          max_diameter_points)
                save_line_points_to_slicer_landmarks_file(os.path.join(root_dir,pat_dir,"L_neck_min_d.mrk.json"),
                                                          min_diameter_points)
                neck_diameter_records.append([pat_dir,
                                              np.linalg.norm(max_diameter_points[0] - max_diameter_points[1]),
                                              np.linalg.norm(min_diameter_points[0] - min_diameter_points[1])
                                              ]
                                             )
    save_neck_records_to_csv(neck_diameter_records,os.path.join(root_dir,"neck_diameter.csv"))
                # final_vessel_points_ijk,diameter_records = extract_vessel_centerline(src_volume, label_volume)
                # for i in range(2):
                #     save_points_to_slicer_landmarks_file(os.path.join(root_dir,pat_dir,str(i) + "_centerline.mrk.json"),
                #                                          src_volume.convert_ijk_to_geometry_array(final_vessel_points_ijk[i]))
                #     save_diameter_records_to_csv(diameter_records[i],
                #                                  os.path.join(root_dir,pat_dir,str(i) + "_diameter.csv"),)
