import os.path

import numpy as np
from coordinates_system import *
from imageDataProcessor import VolumeImage,ConstantVolumeImage,ijk_within_size
from AngeiomaNeckExtraction import *
from utils import projecting_points
import trimesh
import torchio as ti

def preprocessing_image(src_nii_file,preprocessed_nii_file):
    image = ti.ScalarImage(src_nii_file)
    transform = ti.transforms.ZNormalization()
    image = transform(image)
    image.save(preprocessed_nii_file)

def get_angeioma_geometry(angeioma_volume:VolumeImage,angeioma_neck_info,output_dir):
    angeioma_points_ijk = np.array(np.where(angeioma_volume.image_data_array == ANGEIOMA_LABEL)).swapaxes(0,1)
    angeioma_points_geometry = np.array([angeioma_volume.convert_ijk_to_geometry(point) for point in angeioma_points_ijk])

    #points : [N,3]
    angeioma_neck_points, angeioma_neck_center_point,angeioma_neck_equation,angeioma_neck_noraml_euler = angeioma_neck_info
    angeioma_neck_noraml_polar = rectangular2polar(angeioma_neck_noraml_euler)

    ##################################
    ############ VOLUME ##############
    ##################################
    VOLUME = angeioma_points_ijk.shape[0] * angeioma_volume.spacing[0]* angeioma_volume.spacing[1]* angeioma_volume.spacing[2]


    ##################################
    ############ AREA ##############
    ##################################
    SURFACE_AREA = 0.0
    cloud = trimesh.PointCloud(angeioma_points_geometry)
    # 使用球面质心作为点云的估计质心
    mesh = cloud.convex_hull
    # 计算表面积
    SURFACE_AREA = mesh.area
    ##################################
    ############ HEIGHT ##############
    ##################################
    HEIGHT,HEIGHT_points = get_angeioma_height(angeioma_points_geometry,angeioma_neck_info)

    save_line_points_to_slicer_landmarks_file(os.path.join(output_dir,"HEIGHT.mrk.json"),
                                              HEIGHT_points)
    ##################################
    ########## WIDTH-HEIGHT ##########
    ##################################
    WH, WH_points = fast_get_angeioma_wh(angeioma_volume,angeioma_neck_info,HEIGHT_points)
    # get_angeioma_wh(angeioma_volume,angeioma_neck_info,height_points)

    save_line_points_to_slicer_landmarks_file(os.path.join(output_dir,"WH.mrk.json"),
                                              WH_points)

    ##################################
    ############## SIZE ##############
    ##################################
    SIZE, SIZE_points = get_angeioma_size(angeioma_points_geometry,angeioma_neck_info)

    save_line_points_to_slicer_landmarks_file(os.path.join(output_dir,"SIZE.mrk.json"),
                                              SIZE_points)
    ##################################
    ############## WIDTH ##############
    ##################################
    WIDTH,WIDTH_points = fast_get_angeioma_wh(angeioma_volume,angeioma_neck_info,SIZE_points)

    save_line_points_to_slicer_landmarks_file(os.path.join(output_dir,"WIDTH.mrk.json"),
                                              WIDTH_points)
    results = {
        "SIZE":SIZE,
        "WIDTH":WIDTH,
        "HEIGHT":HEIGHT,
        "WH":WH,
    "SURFACE_AREA":SURFACE_AREA,
    "VOLUME":VOLUME}
    return results

def get_angeioma_height(angeioma_points_geometry, angeioma_neck_info):
    angeioma_neck_points, angeioma_neck_center_point, angeioma_neck_equation, angeioma_neck_noraml_euler = angeioma_neck_info
    project_points = projecting_points(angeioma_points_geometry, angeioma_neck_equation)
    height_array = np.linalg.norm(angeioma_points_geometry - project_points, axis=1)
    height = np.max(height_array)
    height_index = np.argmax(height_array)
    height_points = [project_points[height_index], angeioma_points_geometry[height_index]]
    plot_points = [point for point in project_points]
    for point in inter_line(height_points[0],height_points[1]):
        plot_points.append(point)
    # plot_plane_and_points(np.array(plot_points),angeioma_neck_equation)
    return height,height_points

def get_angeioma_wh(angeioma_volume:VolumeImage,angeioma_neck_info,HEIGHT_LINE):
    angeioma_neck_points, _, angeioma_neck_equation, angeioma_neck_noraml_euler = angeioma_neck_info
    HEIGHT_EULER = HEIGHT_LINE[1] -HEIGHT_LINE[0]
    HEIGHT_POLAR = rectangular2polar(HEIGHT_EULER)
    HEIGHT_stride = 10
    HEIGHT_gap = 0.1
    i_HEIGHT = 0
    WIDTH_step = 1000
    WIDTH_gap = 0.1
    WIDTH_records = []
    WIDTH_points_records = []
    plot_points = []
    for point in angeioma_neck_points:
        plot_points.append(point)
    for point in inter_line(HEIGHT_LINE[0],HEIGHT_LINE[1]):
        plot_points.append(point)

    while i_HEIGHT < HEIGHT_POLAR[0] :
        anchor_point = GetNewPosition_PolarDirection(HEIGHT_LINE[0],[i_HEIGHT,HEIGHT_POLAR[1],HEIGHT_POLAR[2]])
        angeioma_volume.set_value_in_geometry(anchor_point, 10)
        i_HEIGHT += HEIGHT_gap
        i_WIDTH_records = []
        i_WIDTH_points_records = []
        for phi in range(180):
            phi = phi / 180 * math.pi
            phi_width_vector_polar,phi_width_vector_euler = GetVerticalVectorOfSpecificVector_Polor2Polar(HEIGHT_POLAR,phi)
            phi_width_vector_polar_anti,phi_width_vector_euler_anti = GetVerticalVectorOfSpecificVector_Polor2Polar(HEIGHT_POLAR,phi + math.pi)
            phi_radius = 0
            phi_radius_anti = 0
            phi_points = []
            # plot_points.append(GetNewPosition_PolarDirection(anchor_point,[10,phi_width_vector_polar[1],phi_width_vector_polar[2]]))
            #positve direction
            for width_index in range(WIDTH_step):
                width = width_index * WIDTH_gap
                i_point = GetNewPosition_PolarDirection(anchor_point,[width,phi_width_vector_polar[1],phi_width_vector_polar[2]])
                i_point_value = angeioma_volume.get_value_in_geometry(i_point)
                if i_point_value  == 0:
                    phi_radius = width
                    phi_points.append(i_point)
                    plot_points.append(i_point)
                    break
            for width_index in range(WIDTH_step):
                width = width_index * WIDTH_gap
                i_point = GetNewPosition_PolarDirection(anchor_point,[width,phi_width_vector_polar_anti[1],phi_width_vector_polar_anti[2]])
                i_point_value = angeioma_volume.get_value_in_geometry(i_point)
                if i_point_value == 0:
                    phi_radius_anti = width
                    phi_points.append(i_point)
                    plot_points.append(i_point)
                    break
            i_WIDTH_records.append(phi_radius + phi_radius_anti)
            i_WIDTH_points_records.append(phi_points)
        i_WIDTH = np.max(np.array(i_WIDTH_records))
        i_WIDTH_points = i_WIDTH_points_records[np.argmax(np.array(i_WIDTH_records))]
        WIDTH_records.append(i_WIDTH)
        WIDTH_points_records.append(i_WIDTH_points)


    WIDTH = np.max(np.array(WIDTH_records))
    WIDTH_points = WIDTH_points_records[np.argmax(np.array(WIDTH_records))]
    WIDTH_line = [WIDTH_points[0],WIDTH_points[1]]

    # for point in angeioma_neck_points:
    #     plot_points.append(point)
    # for point in inter_line(HEIGHT_LINE[0],HEIGHT_LINE[1]):
    #     plot_points.append(point)
    # for point in inter_line(WIDTH_points[0],WIDTH_points[1]):
    #     plot_points.append(point)

    # angeioma_volume.saveImageAsNIFTI(temp_file)
    # plot_plane_and_points(np.array(plot_points), angeioma_neck_equation)
    return WIDTH,WIDTH_points

def fast_get_angeioma_wh(angeioma_volume: VolumeImage, angeioma_neck_info, HEIGHT_LINE):
    angeioma_neck_points, _, angeioma_neck_equation, angeioma_neck_noraml_euler = angeioma_neck_info
    HEIGHT_EULER = HEIGHT_LINE[1] - HEIGHT_LINE[0]
    HEIGHT_POLAR = rectangular2polar(HEIGHT_EULER)
    HEIGHT_stride = 10
    HEIGHT_gap = 0.1
    i_HEIGHT = 0
    WIDTH_step = 1000
    WIDTH_gap = 0.1
    WIDTH_records = []
    WIDTH_points_records = []
    plot_points = []
    avg_spacing = np.average(np.array(angeioma_volume.spacing))
    for point in angeioma_neck_points:
        plot_points.append(point)
    for point in inter_line(HEIGHT_LINE[0], HEIGHT_LINE[1]):
        plot_points.append(point)

    roi_points,anchor_points = get_roi_cylinder_along_specific_line(HEIGHT_LINE, 0.5, 100, 0.1, 500)
    roi_points_ijk = angeioma_volume.convert_geometry_to_ijk_array(roi_points.reshape(-1,3))
    roi_points_ijk_temp = roi_points_ijk.reshape(roi_points.shape)
    image_value_roi_points = angeioma_volume.get_value_in_ijk_array(roi_points_ijk).reshape(roi_points.shape[:-1])
    distance_roi_points_radius = np.linalg.norm(roi_points - anchor_points,axis=-1).reshape(roi_points.shape[:-1])
    distance_roi_points_radius[np.where(image_value_roi_points <= 0)] = -1
    max_disance_R_dims = np.max(distance_roi_points_radius,axis=-1)
    distance_roi_points_diameter = max_disance_R_dims[:,:180] + max_disance_R_dims[:,180:]
    max_diameter = np.max(distance_roi_points_diameter)
    L,ANGLE = np.unravel_index(np.argmax(distance_roi_points_diameter), distance_roi_points_diameter.shape)
    R_POS = np.argmax(distance_roi_points_radius[L,ANGLE,:])
    R_NEG = np.argmax(distance_roi_points_radius[L,ANGLE + 180,:])
    plot_points.append(roi_points[L,ANGLE + 180,R_NEG])
    plot_points.append(roi_points[L,ANGLE,R_POS])
    plot_points.append(anchor_points[L,ANGLE,R_POS])
    WIDTH_points = [roi_points[L,ANGLE,R_POS],roi_points[L,ANGLE + 180,R_NEG]]
    WIDTH = max_diameter - avg_spacing
    return WIDTH, WIDTH_points


def get_angeioma_size(angeioma_points_geometry,angeioma_neck_info):
    angeioma_neck_points, angeioma_neck_center_point, angeioma_neck_equation, angeioma_neck_noraml_euler = angeioma_neck_info
    size_records = []
    for angeioma_point in angeioma_points_geometry:
        i_distance = np.linalg.norm(angeioma_neck_center_point - angeioma_point)
        size_records.append(i_distance)
    SIZE = np.max(np.array(size_records))
    SIZE_points = [angeioma_neck_center_point,angeioma_points_geometry[np.argmax(size_records)]]
    return SIZE,SIZE_points

if __name__ == '__main__':
    src_file = "G:\\0.data\\benchmark\\combine.nii.gz"
    label_file = "G:\\0.data\\benchmark\\angeioma_n.nii.gz"
    largescale_vessel_file = "G:\\0.data\\benchmark\\largescale.nii.gz"
    neat_vessel_file = "G:\\0.data\\benchmark\\dsa_neat_vessel_fast.nii.gz"
    distance_file = "G:\\0.data\\benchmark\\dsa_distance_pipline.nii.gz"
    neck_file = "G:\\0.data\\benchmark\\neck.nii.gz"



    rough_vessel_volume = VolumeImage()
    rough_vessel_volume.get_image_from_nifti(largescale_vessel_file)

    angeioma_volume = VolumeImage()
    angeioma_volume.get_image_from_nifti(label_file)
    angeioma_array =  angeioma_volume.image_data_array
    angeioma_array[np.where(angeioma_array!=ANGEIOMA_LABEL)] = 0
    angeioma_volume.set_image_data_array(angeioma_array)

    neck_volume = VolumeImage()
    neck_volume.get_image_from_nifti(neck_file)

    angeioma_neck_info = get_angeoma_neck_nomial_points_geometry(neck_volume)
    get_angeioma_geometry(angeioma_volume, angeioma_neck_info)