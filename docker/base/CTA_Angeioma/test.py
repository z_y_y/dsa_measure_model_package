import numpy as np
import SimpleITK as sitk
data = np.load("Y:\未确认 770573\ToothFairy_Dataset\Dataset\P15\data.npy")
label = np.load("Y:\未确认 770573\ToothFairy_Dataset\Dataset\P15\gt_sparse.npy")

src_img = sitk.GetImageFromArray(data.swapaxes(0,2))
src_img.SetSpacing([0.5,0.5,0.5])
src_img.SetOrigin([0,0,0,])
src_img.SetDirection([1,0,0,0,1,0,0,0,1])
sitk.WriteImage(src_img, "F:\\data.nii.gz")

label_img = sitk.GetImageFromArray(label.swapaxes(0,2))
label_img.SetSpacing([0.5,0.5,0.5])
label_img.SetOrigin([0,0,0,])
sitk.WriteImage(label_img, "F:\\label.nii.gz")

print(data.shape)