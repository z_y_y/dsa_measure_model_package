import numpy as np
import math

def polar2rectangular(polar):
    euler = np.zeros(3)
    euler[0] = polar[0] * math.sin(polar[1]) * math.cos(polar[2])
    euler[1] = polar[0] * math.sin(polar[1]) * math.sin(polar[2])
    euler[2] = polar[0] * math.cos(polar[1])
    return euler
def polar2rectangular_array(polar_array):
    #polar_array:[N,3]
    euler_array = np.zeros_like(polar_array)
    sin_array = np.sin(polar_array)
    cos_array = np.cos(polar_array)
    R_array = polar_array[:,0]
    euler_array[:,0] = R_array * sin_array[:,1] * cos_array[:,2]
    euler_array[:,1] = R_array * sin_array[:,1] * sin_array[:,2]
    euler_array[:,2] = R_array * cos_array[:,1]
    return  euler_array

def rectangular2polar(euler):
    polar = np.zeros(3)
    r = math.sqrt(
        math.pow(euler[0], 2) +
        math.pow(euler[1], 2) +
        math.pow(euler[2], 2))
    theta = 0
    phi = 0

    if r <= 0:
        return polar
    theta = math.acos(euler[2] / r)
    if euler[0] == 0 :
        if euler[1] > 0 :
            phi = math.pi / 2
        else:
            phi = - math.pi / 2
    else :
        phi = math.atan((euler[1]) / (euler[0]))

    if euler[1] == 0:
        if euler[0] > 0:
            phi = 0
        else:
            phi = math.pi



    if (euler[0] > 0 and euler[1] < 0):
        phi += 2 * math.pi
    elif (euler[0] < 0 and euler[1] > 0) :
        phi += math.pi
    elif (euler[0] < 0 and euler[1] < 0) :
        phi += math.pi

    polar[0] = r
    polar[1] = theta
    polar[2] = phi

    return polar

def get_angle_between_two_vecotr_polor(polar_1,polar_2):
    return polar_2 - polar_1

def get_angle_between_two_vecotr_euler(p1,p2):
    cos_theta = np.dot(p1,p2) /(np.linalg.norm(p1) * np.linalg.norm(p2))
    cos_theta = max(-1,cos_theta)
    cos_theta = min(1,cos_theta)
    theta1 = math.degrees(math.acos(cos_theta))
    sin_theta = np.cross(p1,p2)/(np.linalg.norm(p1) * np.linalg.norm(p2))
    if cos_theta == 0:
        return 90

    tan_theta = sin_theta/cos_theta
    theta2 = np.arctan2(sin_theta,cos_theta)
    print(cos_theta,sin_theta,tan_theta,)
    print(theta1,theta2)
    return theta1


def angle_between(u, v):
    """计算两个三维向量之间的夹角，单位为度数。"""
    dot_product = u[0]*v[0] + u[1]*v[1] + u[2]*v[2]
    u_norm = math.sqrt(u[0]**2 + u[1]**2 + u[2]**2)
    v_norm = math.sqrt(v[0]**2 + v[1]**2 + v[2]**2)
    cos_theta = dot_product / (u_norm * v_norm)
    theta = math.degrees(math.acos(cos_theta))
    return theta

def GetNewPosition_PolarDirection(old_position, direction_polar):
    delta = polar2rectangular(direction_polar)
    new_position = np.array(old_position) + delta
    return new_position

def GetNewPosition_EulerDirection(old_position, direction_euler):
    new_position = np.array(old_position) + direction_euler
    return new_position

# polar vector - > coordinates rotation matrix
# std -> specifc
def GetRotationMatrix(polar_vector):
    r,theta,phi = polar_vector
    polar_vector_4x4 = np.array([r,theta,phi,1])
    R_Y = np.array([
        [math.cos(theta),0,-math.sin(theta)],
        [0,1,0],
        [math.sin(theta),0,math.cos(theta)]
    ])
    R_Z = np.array([
        [math.cos(phi), math.sin(phi), 0],
        [-math.sin(phi), math.cos(phi), 0],
        [0,0,1],
    ])
    M = np.matmul(R_Y,R_Z)
    return M

def GetVerticalVectorOfSpecificVector_Polor2Polar(specific_vector_polar,phi):
    # M_inv: std -> spcific
    M_inv = GetRotationMatrix(specific_vector_polar)
    # M: spcific -> std
    M = np.linalg.inv(M_inv)
    vertical_vector_in_specific_polar = [1,0.5 * math.pi,phi]
    vertical_vector_in_specific_euler = polar2rectangular(vertical_vector_in_specific_polar)
    vertical_vector_in_std_euler = np.dot(M,vertical_vector_in_specific_euler)
    vertical_vector_in_std_polar = rectangular2polar(vertical_vector_in_std_euler)
    return vertical_vector_in_std_polar,vertical_vector_in_std_euler


if __name__ == '__main__':
    x1_polar = [1,math.pi/2,0]
    x1_euler = polar2rectangular(x1_polar)
    GetVerticalVectorOfSpecificVector_Polor2Polar(x1_polar,math.pi * 30 / 180 )