import numpy as np
import torchio as ti
import math
from utils import *
global_spacing = [0.1,0.1,0.1]
# global_spacing = [0.3,0.3,0.3]

def get_padding_size(origin_size,max_size,min_size):
    center_index = round(origin_size / 2)
    delta_max = max(0,origin_size - max_size)
    delta_min = abs(min(0,min_size))

    delta = max(delta_max,delta_min) + 50
    return int(delta)



def convert(angeioma_file,vessel_file,angeioma_file_s,vessel_file_s,angeioma_file_neat,output_file):
    angeioma_image = ti.ScalarImage(angeioma_file)
    print("angeioma size:")
    print(angeioma_image.shape)
    vessel_image = ti.ScalarImage(vessel_file)
    print("vessel size:")
    print(vessel_image.shape)

    # # set spacing to 0.8
    # transform = ti.transforms.Resample(global_spacing)
    # angeioma_image = transform(angeioma_image)
    # print("vessel_image after spacing size:")
    # print(angeioma_image.shape)
    #
    # vessel_image = transform(vessel_image)
    # print(" vessel_imageafter spacing size:")
    # print(vessel_image.shape)
    #
    # # same size
    # globle_size = np.maximum(np.array(angeioma_image.shape),np.array(vessel_image.shape))
    # transform = ti.transforms.CropOrPad(globle_size[1:], padding_mode=int(0))
    # angeioma_image = transform(angeioma_image)
    # print("angeioma_image after CropOrPad size:")
    # print(vessel_image.shape)
    # vessel_image = transform(vessel_image)
    # print("vessel_image after CropOrPad size:")
    # print(vessel_image.shape)
    # C,H,W,D = vessel_image.shape
    # vessel_image.save(vessel_file_s)
    # angeioma_image.save(angeioma_file_s)


    angeioma_volume = VolumeImage()
    angeioma_volume.get_image_from_nifti(angeioma_file)
    vessel_volume = VolumeImage()
    vessel_volume.get_image_from_nifti(vessel_file)

    angeioma_ijk_array = np.array(np.where(angeioma_volume.image_data_array==1)).swapaxes(1,0)
    angeioma_geometry_array = angeioma_volume.convert_ijk_to_geometry_array(angeioma_ijk_array)
    angeioma_to_vessel_array = vessel_volume.convert_geometry_to_ijk_array(angeioma_geometry_array)



    OH,OW,OD = vessel_volume.size

    max_i,max_j,max_k = np.max(angeioma_to_vessel_array,axis=0)
    min_i,min_j,min_k = np.min(angeioma_to_vessel_array,axis=0)
    pad_i,pad_j,pad_k = get_padding_size(OH,max_i,min_i), get_padding_size(OW,max_j,min_j),get_padding_size(OD,max_k,min_k)


    PH,PW,PD = np.array([OH,OW,OD]) + np.array([pad_i,pad_j,pad_k])
    # padding_vessel_array = np.zeros((PH,PW,PD))


    #
    transform = ti.transforms.CropOrPad([PH,PW,round(PD)], padding_mode=int(0))
    vessel_image = transform(vessel_image)
    print("vessel_image after CropOrPad size:")
    print(vessel_image.shape)
    vessel_image.save(vessel_file_s)



    # resample:

    padding_vessel_volume = VolumeImage()
    padding_vessel_volume.get_image_from_nifti(vessel_file_s)

    padding_angeioma_volume = ConstantVolumeImage(padding_vessel_volume,0)

    angeioma_to_padding_vessel_array = np.round(padding_vessel_volume.convert_geometry_to_ijk_array(angeioma_geometry_array)).astype("int")

    padding_angeioma_array = padding_angeioma_volume.image_data_array
    padding_vessel_array = padding_vessel_volume.image_data_array


    record_vessel = np.array(np.where(padding_vessel_array==1)).swapaxes(1,0)
    padding_vessel_array[angeioma_to_padding_vessel_array[:,0],angeioma_to_padding_vessel_array[:,1],angeioma_to_padding_vessel_array[:,2]] = 1
    # padding_vessel_array[record_vessel[:,0],record_vessel[:,1],record_vessel[:,2]] = 1
    padding_angeioma_array[angeioma_to_padding_vessel_array[:,0],angeioma_to_padding_vessel_array[:,1],angeioma_to_padding_vessel_array[:,2]] = 1
    padding_angeioma_array[record_vessel[:,0],record_vessel[:,1],record_vessel[:,2]] = 0

    padding_vessel_volume.set_image_data_array(padding_vessel_array)
    padding_angeioma_volume.set_image_data_array((padding_angeioma_array))

    padding_vessel_volume.saveImageAsNIFTI(output)
    padding_angeioma_volume.saveImageAsNIFTI(angeioma_file_neat)


    # downsaple
    neat_angeioma_image = ti.ScalarImage(angeioma_file_neat)
    print("neat_angeioma_image size:")
    print(neat_angeioma_image.shape)
    padding_vessel_image = ti.ScalarImage(output)
    print("padding_vessel_image size:")
    print(padding_vessel_image.shape)

    # # set spacing to 0.8
    transform = ti.transforms.Resample(global_spacing)
    neat_angeioma_image = transform(neat_angeioma_image)
    padding_vessel_image = transform(padding_vessel_image)
    print("vessel_image after spacing size:")
    print(neat_angeioma_image.shape)
    print(padding_vessel_image.shape)
    neat_angeioma_image.save(angeioma_file_neat)
    padding_vessel_image.save(output_file)




    return None

def convert2(angeioma_file, combine_src_file, angeioma_file_s, vessel_file_s, angeioma_file_neat, output_file):
    angeioma_image = ti.ScalarImage(angeioma_file)
    print("angeioma size:")
    print(angeioma_image.shape)
    combine_src_image = ti.ScalarImage(combine_src_file)
    print("vessel size:")
    print(combine_src_image.shape)

    # # set spacing to 0.8
    # transform = ti.transforms.Resample(global_spacing)
    # angeioma_image = transform(angeioma_image)
    # print("vessel_image after spacing size:")
    # print(angeioma_image.shape)
    #
    # vessel_image = transform(vessel_image)
    # print(" vessel_imageafter spacing size:")
    # print(vessel_image.shape)
    #
    # # same size
    # globle_size = np.maximum(np.array(angeioma_image.shape),np.array(vessel_image.shape))
    # transform = ti.transforms.CropOrPad(globle_size[1:], padding_mode=int(0))
    # angeioma_image = transform(angeioma_image)
    # print("angeioma_image after CropOrPad size:")
    # print(vessel_image.shape)
    # vessel_image = transform(vessel_image)
    # print("vessel_image after CropOrPad size:")
    # print(vessel_image.shape)
    # C,H,W,D = vessel_image.shape
    # vessel_image.save(vessel_file_s)
    # angeioma_image.save(angeioma_file_s)


    angeioma_volume = VolumeImage()
    angeioma_volume.get_image_from_nifti(angeioma_file)
    combine_src_volume = VolumeImage()
    combine_src_volume.get_image_from_nifti(combine_src_file)

    angeioma_ijk_array = np.array(np.where(angeioma_volume.image_data_array==1)).swapaxes(1,0)
    angeioma_geometry_array = angeioma_volume.convert_ijk_to_geometry_array(angeioma_ijk_array)
    angeioma_to_vessel_array = combine_src_volume.convert_geometry_to_ijk_array(angeioma_geometry_array)



    OH,OW,OD = combine_src_volume.size

    max_i,max_j,max_k = np.max(angeioma_to_vessel_array,axis=0)
    min_i,min_j,min_k = np.min(angeioma_to_vessel_array,axis=0)
    pad_i,pad_j,pad_k = get_padding_size(OH,max_i,min_i), get_padding_size(OW,max_j,min_j),get_padding_size(OD,max_k,min_k)


    PH,PW,PD = np.array([OH,OW,OD]) + np.array([pad_i,pad_j,pad_k])
    # padding_vessel_array = np.zeros((PH,PW,PD))


    #
    transform = ti.transforms.CropOrPad([PH,PW,round(PD)], padding_mode=int(0))
    combine_src_image = transform(combine_src_image)
    print("vessel_image after CropOrPad size:")
    print(combine_src_image.shape)
    combine_src_image.save(vessel_file_s)



    # resample:

    padding_vessel_volume = VolumeImage()
    padding_vessel_volume.get_image_from_nifti(vessel_file_s)

    padding_angeioma_volume = ConstantVolumeImage(padding_vessel_volume,0)

    angeioma_to_padding_vessel_array = np.round(padding_vessel_volume.convert_geometry_to_ijk_array(angeioma_geometry_array)).astype("int")

    padding_angeioma_array = padding_angeioma_volume.image_data_array
    padding_vessel_array = padding_vessel_volume.image_data_array


    record_vessel = np.array(np.where(padding_vessel_array==1)).swapaxes(1,0)
    padding_vessel_array[angeioma_to_padding_vessel_array[:,0],angeioma_to_padding_vessel_array[:,1],angeioma_to_padding_vessel_array[:,2]] = 1
    # padding_vessel_array[record_vessel[:,0],record_vessel[:,1],record_vessel[:,2]] = 1
    padding_angeioma_array[angeioma_to_padding_vessel_array[:,0],angeioma_to_padding_vessel_array[:,1],angeioma_to_padding_vessel_array[:,2]] = 1
    # padding_angeioma_array[record_vessel[:,0],record_vessel[:,1],record_vessel[:,2]] = 0

    padding_vessel_volume.set_image_data_array(padding_vessel_array)
    padding_angeioma_volume.set_image_data_array((padding_angeioma_array))

    padding_vessel_volume.saveImageAsNIFTI(output)
    padding_angeioma_volume.saveImageAsNIFTI(angeioma_file_neat)


    # downsaple
    neat_angeioma_image = ti.ScalarImage(angeioma_file_neat)
    print("neat_angeioma_image size:")
    print(neat_angeioma_image.shape)
    padding_vessel_image = ti.ScalarImage(output)
    print("padding_vessel_image size:")
    print(padding_vessel_image.shape)

    # # set spacing to 0.8
    transform = ti.transforms.Resample(global_spacing)
    neat_angeioma_image = transform(neat_angeioma_image)
    padding_vessel_image = transform(padding_vessel_image)
    print("vessel_image after spacing size:")
    print(neat_angeioma_image.shape)
    print(padding_vessel_image.shape)
    neat_angeioma_image.save(angeioma_file_neat)
    padding_vessel_image.save(output)




    return None

def split_segmentation_file(seg_file,output_dir):
    seg_volume = VolumeImage()
    seg_volume.get_image_from_nifti(seg_file)

    angeioma_volume = ConstantVolumeImage(seg_volume,0)
    vessel_volume = ConstantVolumeImage(seg_volume,0)

    seg_array = seg_volume.image_data_array
    angeioma_array = angeioma_volume.image_data_array
    vessel_array = vessel_volume.image_data_array
    angeioma_array[np.where(seg_array==2)] = 1
    vessel_array[np.where(seg_array>=1)] = 1

    angeioma_volume.set_image_data_array(angeioma_array)
    vessel_volume.set_image_data_array(vessel_array)

    angeioma_volume.saveImageAsNIFTI(os.path.join(output_dir,"label.nii.gz"))
    vessel_volume.saveImageAsNIFTI(os.path.join(output_dir,"dsa.nii.gz"))


if __name__ == '__main__':
    import os
    import shutil

    split_segmentation_file("D:\Coding\\0_data\\benchmark_cad\\4\\src.nii.gz","D:\Coding\\0_data\\benchmark_cad\\4")
    exit(0)
    src_dir = "D:\Coding\\0_data\\benchmark_06"
    for case in os.listdir(src_dir):
        case_dir = os.path.join(src_dir,case)
        if os.path.isdir(case_dir):
            print(case_dir)
            output_dir = os.path.join(case_dir,"output")
            if not os.path.exists(output_dir):
                os.mkdir(output_dir)

            angeioma_file = os.path.join(case_dir, "angeioma.nii.gz")
            vessel_file = os.path.join(case_dir,"vessel.nii.gz")
            combine_file = os.path.join(case_dir,"combine.nii.gz")

            f1s = os.path.join(output_dir,"angeioma_s.nii.gz")
            f1n = os.path.join(output_dir,"angeioma_n.nii.gz")
            f2s = os.path.join(output_dir,"vessel_s.nii.gz")
            output = os.path.join(output_dir,"combine.nii.gz")
            convert(angeioma_file,vessel_file,f1s,f2s,f1n,output)

    exit(0)
    src_dir = "D:\Coding\\0_data\\benchmark_05"
    for case in os.listdir(src_dir):
        case_dir = os.path.join(src_dir,case)
        if os.path.isdir(case_dir):
            print(case_dir)
            output_dir = os.path.join(case_dir,"output")
            if not os.path.exists(output_dir):
                os.mkdir(output_dir)

            angeioma_file = os.path.join(case_dir, "angeioma.nii.gz")
            combine_src_file = os.path.join(case_dir,"combine_src.nii.gz")
            if os.path.exists(angeioma_file):
                os.remove(angeioma_file)
            if os.path.exists(combine_src_file):
                os.remove(combine_src_file)
            case_files = []
            for file in os.listdir(case_dir):
                file_path = os.path.join(case_dir,file)
                if os.path.isfile(file_path):
                    case_files.append(file_path)


            stats_1 = os.stat(case_files[0])
            stats_2 = os.stat(case_files[1])

            if stats_1.st_size > stats_2.st_size:
                shutil.copy(case_files[0],angeioma_file)
                shutil.copy(case_files[1],combine_src_file)
            else:
                shutil.copy(case_files[1],angeioma_file)
                shutil.copy(case_files[0],combine_src_file)

            image_1 = ti.ScalarImage(angeioma_file)
            image_2 = ti.ScalarImage(combine_src_file)

            if image_1.shape[-1] > image_2.shape[-1]:
                tempfile = combine_src_file
                combine_src_file = angeioma_file
                angeioma_file = tempfile

            f1s = os.path.join(output_dir,"angeioma_s.nii.gz")
            f1n = os.path.join(output_dir,"angeioma_n.nii.gz")
            f2s = os.path.join(output_dir,"vessel_s.nii.gz")
            output = os.path.join(output_dir,"combine.nii.gz")
            convert2(angeioma_file,combine_src_file,f1s,f2s,f1n,output)