import SimpleITK as sitk
import numpy as np
import scipy


def ijk_within_size(index,box):
    i,j,k = index
    h,w,d = box
    if i < 0 or i > h - 1 or \
            j < 0 or j > w - 1 or \
            k < 0 or k > d - 1:
                return False
    return True

class VolumeImage():
    def __init__(self,):
        self.src_itk_image:sitk.Image = None
        self.affine_matrix = None
        self.origin = None
        self.spacing = None
        self.size = None
        self.direction = None
        self.image_data_array = None
        self.IndexToGeometryMatrix = np.identity(3)
        self.GeometryToIndexMatrix = np.identity(3)

    def get_image_from_nifti(self,src_file):
        self.src_itk_image = sitk.ReadImage(src_file)
        self.origin = self.src_itk_image.GetOrigin()
        self.spacing = self.src_itk_image.GetSpacing()
        self.size = self.src_itk_image.GetSize()
        self.direction = self.src_itk_image.GetDirection()
        self.image_data_array = sitk.GetArrayFromImage(self.src_itk_image).swapaxes(0,2) # [D,W,H] / [k,j,i]
        # self.image_data_array = self.image_data_array.swapaxes(0,2)
        self.IndexToGeometryMatrix = self.calculate_affine_matrix(np.array(self.spacing),np.array(self.direction).reshape((3,3)))
        self.GeometryToIndexMatrix = np.linalg.inv(self.IndexToGeometryMatrix)
    def calculate_affine_matrix(self,spacing,direction):
        affine_matrix = (spacing * np.identity(3)).dot(direction)
        return affine_matrix

    def get_value_in_ijk(self,position_ijk:np.array):
        def Interpolotion2D(g_value,s_value,ratio):
            return (1 - ratio) * s_value + ratio * g_value

        i,j,k = position_ijk
        if not ijk_within_size(np.round([i,j,k]),self.size):
            return -1024

        g_i,g_j,g_k = [int(pos) for pos in np.ceil(np.array([i,j,k]))]
        l_i,l_j,l_k =  [int(pos) for pos in np.floor(np.array([i,j,k]))]
        ratio_i,ratio_j,ratio_k = np.array([i,j,k]) - np.array([l_i,l_j,l_k])
        try:
            d_1 = Interpolotion2D(self.src_itk_image.GetPixel([g_i,l_j,l_k]),self.src_itk_image.GetPixel([l_i,l_j,l_k]),ratio_i)
            d_2 = Interpolotion2D(self.src_itk_image.GetPixel([g_i,g_j,l_k]),self.src_itk_image.GetPixel([l_i,g_j,l_k]),ratio_i)
            d_12 = Interpolotion2D(d_2,d_1,ratio_j)

            u_1 = Interpolotion2D(self.src_itk_image.GetPixel([g_i,l_j,g_k]),self.src_itk_image.GetPixel([l_i,l_j,g_k]),ratio_i)
            u_2 = Interpolotion2D(self.src_itk_image.GetPixel([g_i,g_j,g_k]),self.src_itk_image.GetPixel([l_i,g_j,g_k]),ratio_i)
            u_12 = Interpolotion2D(u_2,u_1,ratio_j)

            ud = Interpolotion2D(u_12,d_12,ratio_k)
            # print(d_1,d_2,d_12,u_1,u_2,u_12)
            # print(ratio_i,ratio_j,ratio_k)
        except:
            print("sth wrong")

        return ud

    def get_value_in_ijk_array(self,src_points_array:np.array):
        def Interpolotion2D_array(ceil_value, floor_value, ratio):
            return (1 - ratio) * floor_value + ratio * ceil_value

        points_array =  np.copy(src_points_array)

         #points_array:[N,3]
        points_array[:,0] = np.maximum(points_array[:,0],0)
        points_array[:,0] = np.minimum(points_array[:,0],self.size[0] - 1)

        points_array[:,1] = np.maximum(points_array[:,1],0)
        points_array[:,1] = np.minimum(points_array[:,1],self.size[1] - 1)

        points_array[:,2] = np.maximum(points_array[:,2],0)
        points_array[:,2] = np.minimum(points_array[:,2],self.size[2] - 1)


        #ceil_array:[N,3]
        ceil_array = np.ceil(points_array).astype("int")
        #floor_array:[N,3]
        floor_array = np.floor(points_array).astype("int")
        #ratio_array:[N,3]
        ratio_array = points_array - floor_array
        try:
            # box_points = np.zeros((ceil_array.shape[0],8,3))
            # box_points[:,0,:] = np.array([ceil_array[:,0],floor_array[:,1],floor_array[:,2]]).T.astype("int")
            # box_points[:,1,:] = np.array([floor_array[:,0],floor_array[:,1],floor_array[:,2]]).T.astype("int")
            # box_points[:,2,:] = np.array([ceil_array[:,0],ceil_array[:,1],floor_array[:,2]]).T.astype("int")
            # box_points[:,3,:] = np.array([floor_array[:,0],ceil_array[:,1],floor_array[:,2]]).T.astype("int")
            # box_points[:,4,:] = np.array([ceil_array[:,0],floor_array[:,1],ceil_array[:,2]]).T.astype("int")
            # box_points[:,5,:] = np.array([floor_array[:,0],floor_array[:,1],ceil_array[:,2]]).T.astype("int")
            # box_points[:,6,:] = np.array([ceil_array[:,0],ceil_array[:,1],ceil_array[:,2]]).T.astype("int")
            # box_points[:,7,:] = np.array([floor_array[:,0],ceil_array[:,1],ceil_array[:,2]]).T.astype("int")
            box_points_value = np.zeros((ceil_array.shape[0],8))
            box_points_value[:,0] = self.image_data_array[ceil_array[:,0],floor_array[:,1],floor_array[:,2]]
            box_points_value[:,1] = self.image_data_array[floor_array[:,0],floor_array[:,1],floor_array[:,2]]
            box_points_value[:,2] = self.image_data_array[ceil_array[:,0],ceil_array[:,1],floor_array[:,2]]
            box_points_value[:,3] = self.image_data_array[floor_array[:,0],ceil_array[:,1],floor_array[:,2]]
            box_points_value[:,4] = self.image_data_array[ceil_array[:,0],floor_array[:,1],ceil_array[:,2]]
            box_points_value[:,5] = self.image_data_array[floor_array[:,0],floor_array[:,1],ceil_array[:,2]]
            box_points_value[:,6] = self.image_data_array[ceil_array[:,0],ceil_array[:,1],ceil_array[:,2]]
            box_points_value[:,7] = self.image_data_array[floor_array[:,0],ceil_array[:,1],ceil_array[:,2]]

            d1 = Interpolotion2D_array(box_points_value[:,0],box_points_value[:,1],ratio_array[:,0])
            d2 = Interpolotion2D_array(box_points_value[:,2],box_points_value[:,3],ratio_array[:,0])
            d12 = Interpolotion2D_array(d2,d1,ratio_array[:,1])

            u1 = Interpolotion2D_array(box_points_value[:,4],box_points_value[:,5],ratio_array[:,0])
            u2 = Interpolotion2D_array(box_points_value[:,6],box_points_value[:,7],ratio_array[:,0])
            u12 = Interpolotion2D_array(u2,u1,ratio_array[:,1])

            ud = Interpolotion2D_array(u12,d12,ratio_array[:,2])

            out_of_frame_records = np.concatenate([
                np.where(src_points_array[:, 0] < 0)[0],
                np.where(src_points_array[:, 0] > self.size[0] - 1)[0],
                 np.where(src_points_array[:, 1] < 0)[0],
                 np.where(src_points_array[:, 1] > self.size[1] - 1)[0],
                 np.where(src_points_array[:, 2] < 0)[0],
                 np.where(src_points_array[:, 2] > self.size[2] - 1)[0],
                 ],axis = 0)

            ud[out_of_frame_records] = -1024
            return ud
        except:
            print("sth wrong")

    def get_value_in_geometry(self,position_geometry:np.array):
        position_ijk = self.convert_geometry_to_ijk(position_geometry)
        value = self.get_value_in_ijk(position_ijk)
        return value


    def get_value_in_geometry_array(self,points_array:np.array):
        position_ijk_array = self.convert_geometry_to_ijk_array(points_array)
        value = self.get_value_in_ijk_array(position_ijk_array)
        return value

    def set_value_in_ijk(self,position_ijk:np.array,label:int,flash_image_data_array = False):
        if not ijk_within_size(np.round(position_ijk), self.size):
            return
        i,j,k = np.round(position_ijk).astype(int)
        self.src_itk_image.SetPixel(int(i),int(j),int(k),label)
        if flash_image_data_array:
            self.fresh_image_data_array_from_itk_image()

    def set_image_data_array(self,data_array):
        self.image_data_array = np.copy(data_array)
        self.fresh_itk_image_from_image_data_array()

    def fresh_image_data_array_from_itk_image(self):
        self.image_data_array = sitk.GetArrayFromImage(self.src_itk_image).swapaxes(0,2)

    def fresh_itk_image_from_image_data_array(self):
        self.src_itk_image = sitk.GetImageFromArray(self.image_data_array.swapaxes(0,2))
        self.src_itk_image.SetSpacing(self.spacing)
        self.src_itk_image.SetOrigin(self.origin)
        self.src_itk_image.SetDirection(self.direction)


    def set_value_in_geometry(self,position_geometry:np.array,label:int):
        position_ijk = self.convert_geometry_to_ijk(position_geometry)
        if not ijk_within_size(np.round(position_ijk), self.size):
            return
        self.set_value_in_ijk(position_ijk,label)

    def convert_geometry_to_ijk(self,position_geometry):
        return np.array(self.src_itk_image.TransformPhysicalPointToContinuousIndex(position_geometry.astype(float)))

    def convert_geometry_to_ijk_array(self,position_geometry):
        value = np.matmul(self.GeometryToIndexMatrix,(position_geometry - np.array(self.origin)).T).T
        return value

    def convert_ijk_to_geometry(self,position_index):
        return np.array(self.src_itk_image.TransformContinuousIndexToPhysicalPoint(position_index.astype(float)))

    def convert_ijk_to_geometry_array(self,position_index):
        value = np.matmul(self.IndexToGeometryMatrix, position_index.T).T + np.array(self.origin)
        return value

    def saveImageArrayAsNIFTI(self,nii_file):
        img = sitk.GetImageFromArray(self.image_data_array.swapaxes(0,2))
        img.CopyInformation(self.src_itk_image)
        sitk.WriteImage(img, nii_file)

    def saveImageAsNIFTI(self,nii_file):
        sitk.WriteImage(self.src_itk_image, nii_file)

class ConstantVolumeImage(VolumeImage):
    def __init__(self, template_volume_image:VolumeImage,constant_label:int):
        super().__init__()
        self.image_data_array = np.ones_like(template_volume_image.image_data_array, dtype=np.int8)
        self.image_data_array = self.image_data_array * constant_label
        self.origin = template_volume_image.origin
        self.spacing = template_volume_image.spacing
        self.size = template_volume_image.size
        self.direction = template_volume_image.direction
        self.src_itk_image = sitk.GetImageFromArray(self.image_data_array.swapaxes(0,2))
        self.src_itk_image.CopyInformation(template_volume_image.src_itk_image)
        self.GeometryToIndexMatrix = template_volume_image.GeometryToIndexMatrix
        self.IndexToGeometryMatrix = template_volume_image.IndexToGeometryMatrix

if __name__ == '__main__':
    src_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181213001815(BA)\dsa.nii.gz"
    dst_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181213001815(BA)\dsa_ones.nii.gz"
    src_volume = VolumeImage()
    src_volume.get_image_from_nifti(src_file)
    src_volume.get_value_in_ijk(np.array([0,0,0],dtype=float))
    # ones_volume = ConstantVolumeImage(src_volume,255)
    # ones_volume.saveImageAsNIFTI(dst_file)
    # print(volume.get_value_in_geometry(np.array([100,100,100])))