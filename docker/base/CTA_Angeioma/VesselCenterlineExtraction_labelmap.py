import os.path

import numpy as np

from utils import *
from AngeiomaNeckExtraction import *
from AngeiomaGeomery import *


def calculate_vessel_volume(src_volume: VolumeImage, angeioma_gt_volume: VolumeImage):
    ####################### STEP 1 ###############################
    # region grow vessel tissue
    center_point_position_ijk, angeioma_volume = get_initial_parameters_of_angeioma(angeioma_gt_volume)
    nominal_angeioma_diameter = np.sqrt(angeioma_volume / math.pi)
    root_vessel_point = Vessel_Point()
    root_vessel_point.position_ijk = np.round(center_point_position_ijk).astype("int")
    root_vessel_point.position_geometry = src_volume.convert_ijk_to_geometry(root_vessel_point.position_ijk)

    growing_points, surface_points = FAST_FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_DISTANCE(src_volume,
                                                                                                      large_scale_vesel_volume,
                                                                                                      root_vessel_point,
                                                                                                      10000, 1000,
                                                                                                      nominal_angeioma_diameter + 20)
    # growing_points,surface_points,self_index_record,parent_index_record,dst_data_array = FAST_ARRAY_FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_DISTANCE(src_volume.image_data_array,dst_volume.image_data_array,root_vessel_point.position_ijk,10000,1000, (nominal_angeioma_diameter + 20)/np.average(src_volume.spacing))
    large_scale_vesel_volume.saveImageAsNIFTI(largescale_vessel_file)

    ####################### STEP 1.5 ###############################
    # get only vessel volume
    temp_all_vessel_array = np.copy(large_scale_vesel_volume.image_data_array)
    temp_angeioma_array = np.copy(angeioma_gt_volume.image_data_array)
    temp_all_vessel_array[np.where(temp_angeioma_array == ANGEIOMA_LABEL)] = 0
    neat_vessel_volume.set_image_data_array(temp_all_vessel_array)
    neat_vessel_volume.saveImageAsNIFTI(neat_vessel_file)


def combine_rough_centerline_points(centerlines):
    pass


def extract_vessel_centerline(src_volume: VolumeImage, angeioma_gt_volume: VolumeImage, output_dir):
    # TODO Temprory step for user angeioma volume contain vessel label
    temp_array = angeioma_gt_volume.image_data_array
    temp_array[np.where(temp_array != ANGEIOMA_LABEL)] = 0
    angeioma_gt_volume.set_image_data_array(temp_array)

    ########################STEP 0#####################
    # init image volume
    large_scale_vesel_volume = ConstantVolumeImage(src_volume, 0)
    neat_vessel_volume = ConstantVolumeImage(src_volume, 0)
    neck_volume = ConstantVolumeImage(src_volume, 0)

    ####################### STEP 1 ###############################
    # region grow vessel tissue
    center_point_position_ijk, angeioma_volume = get_initial_parameters_of_angeioma(angeioma_gt_volume)
    nominal_angeioma_diameter = np.sqrt(angeioma_volume / math.pi)
    root_vessel_point = Vessel_Point()
    root_vessel_point.position_ijk = np.round(center_point_position_ijk).astype("int")
    root_vessel_point.position_geometry = src_volume.convert_ijk_to_geometry(root_vessel_point.position_ijk)

    growing_points, surface_points = FAST_FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_DISTANCE(src_volume,
                                                                                                      large_scale_vesel_volume,
                                                                                                      root_vessel_point,
                                                                                                      1.5, 0.5,
                                                                                                      nominal_angeioma_diameter + 20)
    # growing_points,surface_points,self_index_record,parent_index_record,dst_data_array = FAST_ARRAY_FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_DISTANCE(src_volume.image_data_array,dst_volume.image_data_array,root_vessel_point.position_ijk,10000,1000, (nominal_angeioma_diameter + 20)/np.average(src_volume.spacing))
    large_scale_vesel_volume.saveImageAsNIFTI(os.path.join(output_dir, "largescale_vessel.nii.gz"))
    # large_scale_vesel_volume.saveImageAsNIFTI("C:\\Users\\superele\\Documents\\largescale.nii.gz")
    # neck_array = Get_Angeioma_Neck_Array_From_VesselImageArray_And_AngeiomaImageArray(angeioma_gt_volume.image_data_array,large_scale_vesel_volume.image_data_array)
    # neck_volume.set_image_data_array(neck_array)
    # neck_volume.saveImageAsNIFTI("C:\\Users\\superele\\Documents\\neck.nii.gz")
    # max_diameter_points, max_diameter, min_diameter_points, min_diameter =  Get_Angeioma_Neck_Geometry(neck_volume)
    #
    # refine_max_diameter_points = shrink_neck_diamter(max_diameter_points, - src_volume.spacing[0])
    # refine_min_diameter_points = shrink_neck_diamter(min_diameter_points, - src_volume.spacing[0])
    # save_line_points_to_slicer_landmarks_file("C:\\Users\\superele\\Documents\\L_neck_max_d.mrk.json",
    #                                           max_diameter_points)
    # save_line_points_to_slicer_landmarks_file("C:\\Users\\superele\\Documents\\L_neck_min_d.mrk.json",
    #                                           min_diameter_points)
    # return refine_max_diameter_points, refine_min_diameter_points
    ####################### STEP 1.5 ###############################
    # get only vessel volume
    temp_all_vessel_array = np.copy(large_scale_vesel_volume.image_data_array)
    temp_angeioma_array = np.copy(angeioma_gt_volume.image_data_array)
    temp_all_vessel_array[np.where(temp_angeioma_array == ANGEIOMA_LABEL)] = 0
    neat_vessel_volume.set_image_data_array(temp_all_vessel_array)
    # neat_vessel_volume.saveImageAsNIFTI(neat_vessel_file)

    ####################### STEP 1.6 ###############################
    # Calculate Distance Volume for Next Centerline Extration

    distance_file = os.path.join(output_dir,"distance.nii.gz")
    if os.path.exists(distance_file):
        distance_volume = VolumeImage()
        distance_volume.get_image_from_nifti(distance_file)
    else:
        distance_volume = ConstantVolumeImage(src_volume, 0)
        distance_data_array = distance_filter(neat_vessel_volume.image_data_array)
        distance_data_array = scipy.ndimage.gaussian_filter(distance_data_array, sigma=1.3, radius=3)
        distance_volume.set_image_data_array(distance_data_array)
        distance_volume.saveImageAsNIFTI(distance_file)

    ####################### STEP 2 ###############################
    # Identify Separate Surface and their center point
    surfaces = FAST_separate_vessel_surface_points([np.array(point.position_ijk) for point in surface_points])
    # if len(surfaces) > 3:
    #     size_record = [len(points) for points in surfaces]

    print("seperate vessel surface:" + str(len(surfaces)))
    vessel_section_seed_points = []
    candidates_vessel_point = []
    vessel_section_seed_points = []
    for surface_index, surface in enumerate(surfaces):
        surface = [surface_points[find_same_postion_ijk_in_vessel_surface_points(point, surface_points)] for point in
                   surface]
        for surface_vessel_point in surface:
            point_position_ijk = surface_vessel_point.position_ijk
            # point_position_ijk = surface_vessel_point
            # dst_volume.set_value_in_ijk(point_position_ijk, VESSEL_SURFACE_LABEL + surface_index)
        i_surface_center_point_index = find_center_point_in_vessel_surface_points(surface)
        vessel_section_seed_points.append(surface[i_surface_center_point_index])

    ####################### STEP 3 ###############################
    # Back trace parent points of each surface center point
    # points of every vessel section
    continuous_vessel_points_ijk = []
    for surface_index, seed_point in enumerate(vessel_section_seed_points):
        isinstance(seed_point, Vessel_Point)
        i_section_points = []
        parent_point_index = seed_point.parent_point_index
        while parent_point_index != 0:
            i_point = growing_points[parent_point_index]
            # dst_volume.set_value_in_ijk(i_point.position_ijk, VESSEL_CENTERLINE_LABEL + surface_index)
            parent_point_index = i_point.parent_point_index
            i_section_points.append(i_point.position_ijk)
        continuous_vessel_points_ijk.append(np.array(i_section_points))

    ######
    # TODO
    for vessel_index, rough_centerline_points in enumerate(continuous_vessel_points_ijk):
        save_points_to_slicer_landmarks_file(os.path.join(output_dir, "trace_" + str(vessel_index) + ".mrk.json"),
                                             src_volume.convert_ijk_to_geometry_array(rough_centerline_points))

    ####################### STEP 3.5 ###############################
    # select 2 farest vessel section if more than 2 section\
    if len(continuous_vessel_points_ijk) > 2:
        end_point = np.array([np.array(points[0]) for points in continuous_vessel_points_ijk])
        end_point_matrix = end_point[:, np.newaxis, :]
        end_point_matrix = np.repeat(end_point_matrix, end_point_matrix.shape[0], axis=1)
        end_point_matrix_T = end_point_matrix.swapaxes(0, 1)
        distance_matrix = np.sum(np.abs(end_point_matrix - end_point_matrix_T), axis=-1)
        max_index = np.where(distance_matrix == np.max(distance_matrix))[0]
        continuous_vessel_points_ijk = [continuous_vessel_points_ijk[i] for i in max_index]

    # if len(continuous_vessel_points_ijk ) > 2:
    #     end_point = np.array([np.array(points[0]) for points in continuous_vessel_points_ijk])
    #     end_point_matrix = end_point[:,np.newaxis,:]
    #     end_point_matrix = np.repeat(end_point_matrix,end_point_matrix.shape[0],axis=1)
    #     end_point_matrix_T = end_point_matrix.swapaxes(0,1)
    #     distance_matrix = np.sum(np.abs(end_point_matrix - end_point_matrix_T),axis=-1)
    #     max_index = np.where(distance_matrix == np.max(distance_matrix))[0]
    #     continuous_vessel_points_ijk = [continuous_vessel_points_ijk[i] for i in max_index]

    ####################### STEP 3.55 ###############################
    #### #delete points whithin and close to angeioma, make sure the line between two

    ####################### STEP 3.6 ###############################
    # delete points whithin and close to angeioma and
    close_threshold = 5
    delete_length = 0
    for vessel_index, rough_centerline_points in enumerate(continuous_vessel_points_ijk):
        value_rough_centerline_points = angeioma_gt_volume.get_value_in_ijk_array(rough_centerline_points)
        delete_index = np.where(value_rough_centerline_points == ANGEIOMA_LABEL)
        min_delete_index = min(delete_index[0])
        delete_length = max(delete_length, value_rough_centerline_points.shape[0] - min_delete_index)
    continuous_vessel_points_ijk[0] = continuous_vessel_points_ijk[0][:- delete_length - close_threshold]
    continuous_vessel_points_ijk[1] = continuous_vessel_points_ijk[1][:- delete_length - close_threshold]

    # fill gap between two vessel section
    p1 = continuous_vessel_points_ijk[0][-1]
    p2 = continuous_vessel_points_ijk[1][-1]
    fiil_points = np.array(inter_line(p1, p2, 10))
    continuous_vessel_points_ijk[0] = np.concatenate((continuous_vessel_points_ijk[0], fiil_points[:5]), axis=0)
    continuous_vessel_points_ijk[1] = np.concatenate((continuous_vessel_points_ijk[1], fiil_points[:4:-1]), axis=0)

    ####################### STEP 5 ###############################
    # Refine rough centerline points (contain sparse and resample)
    refine_vessel_points_ijk = []
    refine_vessel_points_geometry = []

    sparse_points_ijk = []
    # mi_points_ijk = []
    # for vessel_index, rough_centerline_points in enumerate(continuous_vessel_points_ijk):
    #     section_refine_vessel_points_ijk = np.empty((0,3))
    #     section_refine_vessel_points_geometry = np.empty((0,3))
    #     rough_centerline_points_array  = rough_centerline_points
    #     # stride_1_for_1mm = max(10 * 1 / np.linalg.norm(src_volume.spacing))
    #     section_mi_points_ijk = get_refine_mi_centerline_points_of_single_vessel_section(neat_vessel_volume, distance_volume, list(rough_centerline_points_array), 10)
    #     mi_points_ijk.append(section_mi_points_ijk)
    #     # sparse_points_ijk.insert(0,rough_centerline_points[0].position_ijk)
    # sparse_points_ijk.append(rough_centerline_points[-1])
    # save_points_to_slicer_landmarks_file("C:\\Users\\superele\\Documents\\sparse_"+str(1)+".mrk.json",src_volume.convert_ijk_to_geometry_array(np.array(sparse_points_ijk[1])))
    # fill gap between two vessel section
    # p1 = sparse_points_ijk[0][-1]
    # p2 = sparse_points_ijk[1][-1]
    # sparse_points_ijk[0].append((p1 + p2)/2)
    # sparse_points_ijk[1].append((p1 + p2)/2)

    # TODO
    # merge two vessel section to ONE VESSEL SECTION
    nominal_split_index = continuous_vessel_points_ijk[0].shape[0]
    merge_vessel_points = np.concatenate((continuous_vessel_points_ijk[0], continuous_vessel_points_ijk[1][::-1]),
                                         axis=0)

    save_points_to_slicer_landmarks_file(os.path.join(output_dir, "merge_rough.mrk.json"),
                                         src_volume.convert_ijk_to_geometry_array(merge_vessel_points))

    merge_mi_points_ijk = get_refine_mi_centerline_points_of_single_vessel_section(neat_vessel_volume,
                                                                                   distance_volume,
                                                                                   list(merge_vessel_points), 10)
    merge_easy_refine_points = easy_refine_points(merge_mi_points_ijk)

    save_points_to_slicer_landmarks_file(os.path.join(output_dir, "merge_mi_refine_first.mrk.json"),
                                         src_volume.convert_ijk_to_geometry_array(merge_mi_points_ijk))
    save_points_to_slicer_landmarks_file(os.path.join(output_dir, "merge_easy_refine_first.mrk.json"),
                                         src_volume.convert_ijk_to_geometry_array(merge_easy_refine_points))

    merge_easy_refine_points = get_refine_mi_centerline_points_of_single_vessel_section(neat_vessel_volume,
                                                                                        distance_volume,
                                                                                        list(merge_easy_refine_points),
                                                                                        2, 2)

    save_points_to_slicer_landmarks_file(os.path.join(output_dir, "merge_easy_refine_second.mrk.json"),
                                         src_volume.convert_ijk_to_geometry_array(merge_easy_refine_points))
    merge_easy_refine_points = easy_refine_points(merge_easy_refine_points)

    save_points_to_slicer_landmarks_file(os.path.join(output_dir, "merge_easy_refine_third.mrk.json"),
                                         src_volume.convert_ijk_to_geometry_array(merge_easy_refine_points))


    #delete same points
    merge_easy_refine_points = np.unique(merge_easy_refine_points,axis=0)


    # do distance refine second time

    merge_mean_points_geometry, merge_max_diameter_results, merge_min_diameter_results, merge_avg_diameter_results = get_diameter_of_centerline_points(
        neat_vessel_volume, src_volume.convert_ijk_to_geometry_array(merge_easy_refine_points))

    refine_merge_max_vessel_diameter = refine_vessel_diameter_both_side(np.array(merge_mean_points_geometry),
                                                                        [x[0] for x in merge_max_diameter_results],
                                                                        neat_vessel_volume)
    refine_merge_min_vessel_diameter = refine_vessel_diameter_both_side(np.array(merge_mean_points_geometry),
                                                                        [x[0] for x in merge_min_diameter_results],
                                                                        neat_vessel_volume)

    # merge_csv_data = []
    # refine_merge_csv_data = []
    # distance = 0
    # last_point = merge_mean_points_geometry[0]
    # for index, point in enumerate(merge_mean_points_geometry):
    #     distance += np.linalg.norm(point - last_point)
    #     last_point = point
    #     merge_csv_data.append([distance, merge_max_diameter_results[index][0], merge_min_diameter_results[index][0]])
    #     refine_merge_csv_data.append([distance, refine_merge_max_vessel_diameter[index], refine_merge_min_vessel_diameter[index]])
    # save_diameter_records_to_csv(merge_csv_data,os.path.join(output_dir,"merge_diameter_records.csv"))
    # save_diameter_records_to_csv(refine_merge_csv_data, os.path.join(output_dir,"refine_merge_diameter_records.csv"))

    ### adjust two lines balance
    # k = 10
    # nominal_split_index =int( merge_easy_refine_points.shape[0] / 2)
    # distance_array_points = np.linalg.norm(merge_easy_refine_points - center_point_position_ijk,axis=1)[nominal_split_index - k:nominal_split_index + k]
    # balance_split_index = np.argmin(distance_array_points)
    # balance_refine_points_1 = merge_easy_refine_points[: nominal_split_index - k + balance_split_index]
    # balance_refine_points_2 = merge_easy_refine_points[nominal_split_index - k + balance_split_index:][::-1]

    k = 10
    merge_mean_points_ijk = src_volume.convert_geometry_to_ijk_array(np.array(merge_mean_points_geometry))
    nominal_split_index = int(merge_mean_points_ijk.shape[0] / 2)
    distance_array_points = np.linalg.norm(merge_mean_points_ijk - center_point_position_ijk, axis=1)[
                            nominal_split_index - k:nominal_split_index + k]
    balance_split_index = np.argmin(distance_array_points)
    balance_refine_points_1 = merge_mean_points_ijk[: nominal_split_index - k + balance_split_index][::-1]
    balance_refine_points_2 = merge_mean_points_ijk[nominal_split_index - k + balance_split_index:]

    balance_refine_max_diameter_records_1 = refine_merge_max_vessel_diameter[
                                            : nominal_split_index - k + balance_split_index][::-1]
    balance_refine_max_diameter_records_2 = refine_merge_max_vessel_diameter[
                                            nominal_split_index - k + balance_split_index:]
    balance_refine_max_diameter_points_1 = [data[1] for data in
                                            merge_max_diameter_results[: nominal_split_index - k + balance_split_index][
                                            ::-1]]
    balance_refine_max_diameter_points_2 = [data[1] for data in
                                            merge_max_diameter_results[nominal_split_index - k + balance_split_index:]]

    balance_refine_min_diameter_records_1 = refine_merge_min_vessel_diameter[
                                            : nominal_split_index - k + balance_split_index][::-1]
    balance_refine_min_diameter_records_2 = refine_merge_min_vessel_diameter[
                                            nominal_split_index - k + balance_split_index:]

    balance_refine_min_diameter_points_1 = [data[1] for data in
                                            merge_min_diameter_results[: nominal_split_index - k + balance_split_index][
                                            ::-1]]
    balance_refine_min_diameter_points_2 = [data[1] for data in
                                            merge_min_diameter_results[nominal_split_index - k + balance_split_index:]]

    final_vessel_points_ijk = [balance_refine_points_1, balance_refine_points_2]
    final_vessel_max_diameter_records = [balance_refine_max_diameter_records_1, balance_refine_max_diameter_records_2]
    final_vessel_min_diameter_records = [balance_refine_min_diameter_records_1, balance_refine_min_diameter_records_2]
    final_vessel_max_diameter_points = [balance_refine_max_diameter_points_1, balance_refine_max_diameter_points_2]
    final_vessel_min_diameter_points = [balance_refine_min_diameter_points_1, balance_refine_min_diameter_points_2]
    max_5mm_diameter = 0
    max_5mm_diameter_center_point = 0
    max_5mm_diameter_points = []
    refine_max_5mm_diameter_points = []
    max_5mm_diameter_distance = 0
    min_5mm_diameter = 1000
    min_5mm_diameter_center_point = 0
    min_5mm_diameter_points = []
    refine_min_5mm_diameter_points = []
    min_5mm_diameter_distance = 0
    for vessel_index, secion_refine_vessel_points_ijk in enumerate(final_vessel_points_ijk):
        distance = 0
        csv_data = []
        secion_refine_vessel_points_geometry = src_volume.convert_ijk_to_geometry_array(
            np.array(secion_refine_vessel_points_ijk))
        last_point = secion_refine_vessel_points_geometry[0]
        for index, point in enumerate(secion_refine_vessel_points_geometry):
            distance += np.linalg.norm(point - last_point)
            last_point = point
            max_diameter = final_vessel_max_diameter_records[vessel_index][index]
            min_diameter = final_vessel_min_diameter_records[vessel_index][index]
            max_diameter_points = final_vessel_max_diameter_points[vessel_index][index]
            min_diameter_points = final_vessel_min_diameter_points[vessel_index][index]
            csv_data.append([distance, max_diameter, min_diameter] + list(point))
            if distance <= 5:
                if max_diameter > max_5mm_diameter:
                    max_5mm_diameter = max_diameter
                    max_5mm_diameter_points = max_diameter_points
                    max_5mm_diameter_center_point = (max_diameter_points[0] + max_diameter_points[1]) / 2
                    refine_max_5mm_diameter_points = get_nominal_diameter_points(max_diameter_points,
                                                                                 max_5mm_diameter_center_point,
                                                                                 max_diameter)
                    max_5mm_diameter_distance = distance
                if min_diameter < min_5mm_diameter:
                    min_5mm_diameter = min_diameter
                    min_5mm_diameter_points = min_diameter_points
                    min_5mm_diameter_center_point = (min_diameter_points[0] + min_diameter_points[1]) / 2
                    refine_min_5mm_diameter_points = get_nominal_diameter_points(min_diameter_points,
                                                                                 min_5mm_diameter_center_point,
                                                                                 min_diameter)
                    min_5mm_diameter_distance = distance
        save_diameter_records_to_csv(csv_data,
                                     os.path.join(output_dir, "final_diameter_records_" + str(vessel_index) + ".csv"))

    max_5mm_diameter_center_point_ijk = src_volume.convert_geometry_to_ijk(max_5mm_diameter_center_point)
    min_5mm_diameter_center_point_ijk = src_volume.convert_geometry_to_ijk(min_5mm_diameter_center_point)
    final_5mm_diameter_records = [
        [max_5mm_diameter_distance,
         max_5mm_diameter,
         max_5mm_diameter_center_point[0], max_5mm_diameter_center_point[1], max_5mm_diameter_center_point[2],
         max_5mm_diameter_points[0][0], max_5mm_diameter_points[0][1], max_5mm_diameter_points[0][2],
         max_5mm_diameter_points[1][0], max_5mm_diameter_points[1][1], max_5mm_diameter_points[1][2],
         ],
        [min_5mm_diameter_distance,
         min_5mm_diameter,
         min_5mm_diameter_center_point[0], min_5mm_diameter_center_point[1], min_5mm_diameter_center_point[2],
         min_5mm_diameter_points[0][0], min_5mm_diameter_points[0][1], min_5mm_diameter_points[0][2],
         min_5mm_diameter_points[1][0], min_5mm_diameter_points[1][1], min_5mm_diameter_points[1][2]]
    ]

    save_5mm_diameter_records_csv(final_5mm_diameter_records,
                                  os.path.join(output_dir, "final_5mm_diameter_records.csv"))

    save_points_to_slicer_landmarks_file(os.path.join(output_dir, "max_diameter_center_point.mrk.json"),
                                         (np.array([max_5mm_diameter_center_point])))
    save_line_points_to_slicer_landmarks_file(os.path.join(output_dir, "L_max_diameter_points.mrk.json"),
                                              max_5mm_diameter_points)
    save_line_points_to_slicer_landmarks_file(os.path.join(output_dir, "L_refine_max_diameter_points.mrk.json"),
                                              refine_max_5mm_diameter_points)
    save_points_to_slicer_landmarks_file(os.path.join(output_dir, "min_diameter_center_point.mrk.json"),
                                         (np.array([min_5mm_diameter_center_point])))
    save_line_points_to_slicer_landmarks_file(os.path.join(output_dir, "L_min_diameter_points.mrk.json"),
                                              min_5mm_diameter_points)
    save_line_points_to_slicer_landmarks_file(os.path.join(output_dir, "L_refine_min_diameter_points.mrk.json"),
                                              refine_min_5mm_diameter_points)
    VESSEL_GEOMETRY_DICT = {
        "VESSEL_MAX_DIAMETER": max_5mm_diameter,
        "VESSEL_MIN_DIAMETER": min_5mm_diameter
    }

    return VESSEL_GEOMETRY_DICT

    # for vessel_index, section_sparse_points_ijk in enumerate(sparse_points_ijk):
    #     section_refine_vessel_points_ijk = np.empty([0,3])
    #     for point_index in range(len(section_sparse_points_ijk) - 1):
    #         point_pair = [section_sparse_points_ijk[point_index], section_sparse_points_ijk[point_index + 1]]
    #         point_pair_geomtry = [src_volume.convert_ijk_to_geometry(point) for point in point_pair]
    #         resample_points_ijk = resample_centerline_between_two_points(distance_volume, neat_vessel_volume, point_pair)
    #         section_refine_vessel_points_ijk = np.concatenate((section_refine_vessel_points_ijk,resample_points_ijk),axis=0)
    #         # resample_points_geometry = resample_centerline_between_two_points_geometry(distance_volume,dst_volume,point_pair_geomtry)
    #         # section_refine_vessel_points_geometry = np.concatenate((section_refine_vessel_points_geometry,resample_points_geometry),axis=0)
    #     refine_vessel_points_ijk.append(section_refine_vessel_points_ijk)

    # easy_refine_points_1 = easy_refine_points(mi_points_ijk[0])
    # easy_refine_points_2 = easy_refine_points(mi_points_ijk[1])
    # plot_line_points(refine_vessel_points_ijk[0])

    ####################### STEP 5.1 ###############################
    ### adjust two lines balance
    # k = 10
    # distance_array_points_1 = np.linalg.norm(easy_refine_points_1 - center_point_position_ijk,axis=1)[-k:]
    # distance_array_points_2 = np.linalg.norm(easy_refine_points_2 - center_point_position_ijk,axis=1)[-k:]
    # if np.min(distance_array_points_1,axis=0) >  np.min(distance_array_points_2,axis=0):
    #     index = np.argmin(distance_array_points_2)
    #     balance_refine_points_1 = np.concatenate([easy_refine_points_1,easy_refine_points_2[:index - k:-1,:]],axis=0)
    #     balance_refine_points_2 = easy_refine_points_2[:index - k,:]
    # else:
    #     index = np.argmin(distance_array_points_1)
    #     balance_refine_points_2 = np.concatenate([easy_refine_points_2,easy_refine_points_1[:index - k:-1,:]],axis=0)
    #     balance_refine_points_1 = easy_refine_points_1[:index - k,:]

    # save_points_to_slicer_landmarks_file("C:\\Users\\superele\\Documents\\F1.mrk.json",src_volume.convert_ijk_to_geometry_array(refine_vessel_points_ijk[0]))
    # save_points_to_slicer_landmarks_file("C:\\Users\\superele\\Documents\\F2.mrk.json",src_volume.convert_ijk_to_geometry_array(refine_vessel_points_ijk[1]))
    # save_points_to_slicer_landmarks_file("C:\\Users\\superele\\Documents\\F_easy_1.mrk.json",src_volume.convert_ijk_to_geometry_array(easy_refine_points_1))
    # save_points_to_slicer_landmarks_file("C:\\Users\\superele\\Documents\\F_easy_2.mrk.json",src_volume.convert_ijk_to_geometry_array(easy_refine_points_2))
    # save_points_to_slicer_landmarks_file("C:\\Users\\superele\\Documents\\F_balance_1.mrk.json",src_volume.convert_ijk_to_geometry_array(balance_refine_points_1))
    # save_points_to_slicer_landmarks_file("C:\\Users\\superele\\Documents\\F_balance_2.mrk.json",src_volume.convert_ijk_to_geometry_array(balance_refine_points_2))

    ####################### STEP 6 ###############################
    # get all vessel centerline point diameter

    final_vessel_points_ijk = [balance_refine_points_1, balance_refine_points_2]
    diameter_records = []
    for vessel_index, secion_refine_vessel_points_ijk in enumerate(final_vessel_points_ijk):
        mean_points, max_diameter_results, min_diameter_results, avg_diameter_results = get_diameter_of_centerline_points(
            neat_vessel_volume, src_volume.convert_ijk_to_geometry_array(secion_refine_vessel_points_ijk))
        # save_points_to_slicer_landmarks_file("C:\\Users\\superele\\Documents\\F_mean_points.mrk.json", mean_points)
        # save_points_to_slicer_landmarks_file("C:\\Users\\superele\\Documents\\F_max_p1.mrk.json",
        #                                      [data[1][0] for data in max_diameter_results])
        # save_points_to_slicer_landmarks_file("C:\\Users\\superele\\Documents\\F_max_p2.mrk.json",
        #                                      [data[1][1] for data in max_diameter_results])
        # save_points_to_slicer_landmarks_file("C:\\Users\\superele\\Documents\\F_min_p1.mrk.json",
        #                                      [data[1][0] for data in min_diameter_results])
        # save_points_to_slicer_landmarks_file("C:\\Users\\superele\\Documents\\F_min_p2.mrk.json",
        #                                      [data[1][1] for data in min_diameter_results])

        # show diameter records
        nominal_seed_geometry = src_volume.convert_ijk_to_geometry(
            (final_vessel_points_ijk[0][-1] + final_vessel_points_ijk[1][-1]) / 2)
        max_d_plot_data = []
        min_d_plot_data = []
        distance = 0
        mean_points.reverse()
        max_diameter_results.reverse()
        min_diameter_results.reverse()
        last_point = nominal_seed_geometry
        merge_csv_data = []
        for index, point in enumerate(mean_points):
            distance += np.linalg.norm(point - last_point)
            merge_csv_data.append([distance, max_diameter_results[index][0], min_diameter_results[index][0]])
            max_d_plot_data.append(np.array([distance, max_diameter_results[index][0]]))
            min_d_plot_data.append(np.array([distance, min_diameter_results[index][0]]))
            last_point = point
        diameter_records.append(merge_csv_data)
        save_diameter_records_to_csv(merge_csv_data,
                                     "D:\\Coding\\0_data\\DAS_Benchmark\\AI-20181212002402(L-C4)\\" + str(
                                         vessel_index) + "_diameter.csv")
    # plot_diameter_records(max_d_plot_data,min_d_plot_data)

    # dst_volume.saveImageAsNIFTI(dst_file)

    return final_vessel_points_ijk, diameter_records


def get_nominal_diameter_points(orgin_points, center_point, nominal_diameter):
    p1 = orgin_points[0]
    p2 = orgin_points[1]
    radius = nominal_diameter / 2
    direction = p1 - p2
    p3 = GetNewPosition_EulerDirection(center_point, direction / np.linalg.norm(direction) * radius)
    p4 = GetNewPosition_EulerDirection(center_point, -direction / np.linalg.norm(direction) * radius)
    return [p3, p4]


def refine_vessel_diameter(vessel_points, diameter_records):
    # find bad points
    bad_index = []
    max_diameter = 10
    refine_diameter_records = []
    for index in range(1, vessel_points.shape[0] - 1):
        if diameter_records[index] >= max_diameter or \
                abs(diameter_records[index - 1] - diameter_records[index]) > 0.5 * diameter_records[index - 1]:
            # \
            # diameter_records[index - 1] > 2 * diameter_records[index] or diameter_records[index - 1] < 0.5 * diameter_records[index] or \
            #     diameter_records[index + 1] > 2 * diameter_records[index] or diameter_records[index +- 1] < 0.5 * diameter_records[index] :
            bad_index.append(index)

    for index, point in enumerate(vessel_points):
        # if diameter_records[index] >= 7 or
        pass


def refine_vessel_diameter_left_side(vessel_points, diameter_records):
    refine_diameter_records = [diameter_records[0]]

    for index in range(1, vessel_points.shape[0]):
        vessel_point = vessel_points[index]
        diameter = diameter_records[index]
        last_vessel_point = vessel_points[index - 1]
        last_refine_diameter = refine_diameter_records[index - 1]
        distance = np.linalg.norm(vessel_point - last_vessel_point)
        max_delta_diameter = 1.5 * distance
        if abs(diameter - last_refine_diameter) > max_delta_diameter:
            refine_diameter_records.append(last_refine_diameter + max_delta_diameter)
        else:
            refine_diameter_records.append(diameter)
    return refine_diameter_records


def refine_vessel_diameter_both_side(vessel_points_geometry, diameter_records, vessel_volume: VolumeImage):
    diameter_records = [max(1.5, x) for x in diameter_records]

    num_points = vessel_points_geometry.shape[0]
    fisrt_refine_diameter_records = [diameter_records[0]]
    second_refine_diameter_records = [diameter_records[-1]]

    for index in range(1, num_points):
        vessel_point = vessel_points_geometry[index]
        diameter = diameter_records[index]
        last_vessel_point = vessel_points_geometry[index - 1]
        last_refine_diameter = fisrt_refine_diameter_records[index - 1]
        distance = np.linalg.norm(vessel_point - last_vessel_point)
        max_delta_diameter = 1.5 * distance
        if diameter - last_refine_diameter > max_delta_diameter:
            fisrt_refine_diameter_records.append(last_refine_diameter + max_delta_diameter)
        else:
            fisrt_refine_diameter_records.append(diameter)

    for index in range(1, num_points):
        real_index = num_points - index - 1
        vessel_point = vessel_points_geometry[real_index]
        diameter = fisrt_refine_diameter_records[real_index]
        last_vessel_point = vessel_points_geometry[real_index + 1]
        last_refine_diameter = second_refine_diameter_records[index - 1]
        distance = np.linalg.norm(vessel_point - last_vessel_point)
        max_delta_diameter = 1.5 * distance
        if diameter - last_refine_diameter > max_delta_diameter:
            second_refine_diameter_records.append(last_refine_diameter + max_delta_diameter)
        else:
            second_refine_diameter_records.append(diameter)
    second_refine_diameter_records.reverse()
    return second_refine_diameter_records


def easy_refine_points(centerline_points_geometry, roi=2):
    point_num = centerline_points_geometry.shape[0]
    # [roi : point_nun - roi]
    easy_refine_pionts = []
    for i in range(0, roi):
        easy_refine_pionts.append(centerline_points_geometry[i])
    for i in range(roi, point_num - roi):
        roi_points = centerline_points_geometry[i - roi:i + roi + 1]
        direction_euler, mean_point = line_fitting_3d(roi_points)
        easy_refine_pionts.append(mean_point)
    for i in range(roi):
        easy_refine_pionts.append(centerline_points_geometry[point_num - roi + i])

    return np.array(easy_refine_pionts)


def get_diameter_of_centerline_points(angeioma_volume: VolumeImage, centerline_points_geometry, roi=2):
    point_num = centerline_points_geometry.shape[0]
    # [roi : point_nun - roi]
    max_diameter_results = []
    min_diameter_results = []
    avg_diameter_results = []
    mean_points_geometry = []
    r_each_step = 0.1
    max_r = 8
    for i in range(roi, point_num - roi):
        roi_points = centerline_points_geometry[i - roi:i + roi + 1]
        roi_L = np.linalg.norm(roi_points[-1] - roi_points[0])
        direction_euler, mean_point = line_fitting_3d(roi_points)
        mean_point = centerline_points_geometry[i]
        cylinder_roi_p1 = GetNewPosition_EulerDirection(mean_point, roi_L / roi / 2 * direction_euler)
        cylinder_roi_p2 = GetNewPosition_EulerDirection(mean_point, -roi_L / roi / 2 * direction_euler)
        if np.linalg.norm(cylinder_roi_p1 - cylinder_roi_p2) ==0:
            print("error")
        cylinder_roi_points, _ = get_roi_cylinder_along_specific_line([cylinder_roi_p1, cylinder_roi_p2], 0.1, 1,
                                                                      r_each_step, int(max_r / r_each_step))
        L_SIZE, THETA_SIZE, R_SIZE = cylinder_roi_points.shape[:-1]
        angeioma_value_roi_points = angeioma_volume.get_value_in_geometry_array(
            cylinder_roi_points.reshape((-1, 3))).reshape(L_SIZE, THETA_SIZE, R_SIZE)
        angeioma_value_roi_points[:, :, R_SIZE - 1] = 0
        angeioma_value_roi_points[:, :, :2] = 1
        boundry_R_index = np.argmin(angeioma_value_roi_points != 0, axis=-1)
        boundry_D_index = boundry_R_index[:, :int(THETA_SIZE / 2)] + boundry_R_index[:, int(THETA_SIZE / 2):]

        # max_diameter
        max_diameter_index = np.unravel_index(np.argmax(boundry_D_index), (L_SIZE, int(THETA_SIZE / 2)))
        max_diameter = boundry_D_index[max_diameter_index[0], max_diameter_index[1]] * r_each_step
        max_radius_index_1 = boundry_R_index[max_diameter_index[0], max_diameter_index[1]]
        max_radius_index_2 = boundry_R_index[max_diameter_index[0], max_diameter_index[1] + int(THETA_SIZE / 2)]
        max_dimaeter_point_1 = cylinder_roi_points[max_diameter_index[0], max_diameter_index[1], max_radius_index_1]
        max_dimaeter_point_2 = cylinder_roi_points[
            max_diameter_index[0], max_diameter_index[1] + int(THETA_SIZE / 2), max_radius_index_2]

        # min_diameter
        min_diameter_index = np.unravel_index(np.argmin(boundry_D_index), (L_SIZE, int(THETA_SIZE / 2)))
        min_diameter = boundry_D_index[min_diameter_index[0], min_diameter_index[1]] * r_each_step
        min_radius_index_1 = boundry_R_index[min_diameter_index[0], min_diameter_index[1]]
        min_radius_index_2 = boundry_R_index[min_diameter_index[0], min_diameter_index[1] + int(THETA_SIZE / 2)]
        min_dimaeter_point_1 = cylinder_roi_points[min_diameter_index[0], min_diameter_index[1], min_radius_index_1]
        min_dimaeter_point_2 = cylinder_roi_points[
            min_diameter_index[0], min_diameter_index[1] + int(THETA_SIZE / 2), min_radius_index_2]

        if min_diameter == 0:
            print("bingo")

        # avg_diameter
        avg_diameter = np.mean(boundry_D_index) * r_each_step

        mean_points_geometry.append(mean_point)
        max_diameter_results.append([max_diameter, [max_dimaeter_point_1, max_dimaeter_point_2]])
        min_diameter_results.append([min_diameter, [min_dimaeter_point_1, min_dimaeter_point_2]])
        avg_diameter_results.append(avg_diameter)
    return mean_points_geometry, max_diameter_results, min_diameter_results, avg_diameter_results







if __name__ == '__main__':

    src_dir = "D:\Coding\\0_data\\benchmark_03"
    output_dir = "D:\Coding\\0_data\\benchmark_03"
    src_nii_file = os.path.join(src_dir, "combine.nii.gz")
    label_nii_file = os.path.join(src_dir, "angeioma_n.nii.gz")
    if os.path.exists(src_nii_file) and os.path.exists(label_nii_file) and not os.path.exists(
            os.path.join(output_dir, "all_geometry.csv")):
        results_dict = {}
        # step 1 vessel extration
        src_volume = VolumeImage()
        src_volume.get_image_from_nifti(src_nii_file)
        angeioma_volume = VolumeImage()
        angeioma_volume.get_image_from_nifti(label_nii_file)
        vessel_geometry_dict = extract_vessel_centerline(src_volume, angeioma_volume, output_dir)

        # step 2 vessel neck extration
        largescale_vessel_volume = VolumeImage()
        largescale_vessel_volume.get_image_from_nifti(os.path.join(output_dir, "largescale_vessel.nii.gz"))

        neck_volume = ConstantVolumeImage(src_volume, 0)
        neck_array = Get_Angeioma_Neck_Array_From_VesselImageArray_And_AngeiomaImageArray(
            angeioma_volume.image_data_array, largescale_vessel_volume.image_data_array)
        neck_volume.set_image_data_array(neck_array)
        neck_volume.saveImageAsNIFTI(os.path.join(output_dir, "neck.nii.gz"))

        max_diameter_points, max_diameter, min_diameter_points, min_diameter, angeioma_neck_info = Get_Angeioma_Neck_Geometry(
            neck_volume)
        refine_max_diameter_points = shrink_neck_diamter(max_diameter_points, - src_volume.spacing[0])
        refine_min_diameter_points = shrink_neck_diamter(min_diameter_points, - src_volume.spacing[0])
        neck_max_diameter = np.linalg.norm(refine_max_diameter_points[0] - refine_max_diameter_points[1])
        neck_min_diameter = np.linalg.norm(refine_min_diameter_points[0] - refine_min_diameter_points[1])

        save_line_points_to_slicer_landmarks_file(os.path.join(output_dir, "L_neck_max_d.mrk.json"),
                                                  refine_max_diameter_points)
        save_line_points_to_slicer_landmarks_file(os.path.join(output_dir, "L_neck_min_d.mrk.json"),
                                                  refine_min_diameter_points)

        neck_geometry_dict = {
            "NECK_MAX_DIAMETER": neck_max_diameter,
            "NECK_MIN_DIAMETER": neck_min_diameter,
        }
        # step 3 angeioma geometry
        angeioma_geometry_dict = get_angeioma_geometry(angeioma_volume, angeioma_neck_info, output_dir)
        save_geometry_data_to_csv(dict(**neck_geometry_dict, **vessel_geometry_dict, **angeioma_geometry_dict),
                                  os.path.join(output_dir, "all_geometry.csv"))
