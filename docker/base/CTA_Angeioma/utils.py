import os

import SimpleITK as sitk
import numpy as np
import sys
import copy
import scipy
from scipy.interpolate import splprep, splev
import time
import matplotlib.pyplot as plt
import json
from imageDataProcessor import VolumeImage,ConstantVolumeImage,ijk_within_size
from data_struct import Vessel_Point
from coordinates_system import *
import pandas as pd
from skimage import morphology,measure

ANGEIOMA_LABEL = 1
VESSEL_LABEL = 1
VESSEL_SURFACE_LABEL = 20
VESSEL_CENTERLINE_LABEL = 30
VESSEL_CENTERLINE_LABEL_TEMP = 100
MAX_SEARCH_POINT = 50000
MAX_SEARCH_DISTANCE = 150
DEBUG_MODE = False

def show_3d_points(x,y,z,tiltle = "3D scatter plot"):
    fig = plt.figure(figsize=(10, 7))
    ax = plt.axes(projection="3d")
    ax.scatter3D(x,y,z, color="red")
    plt.title(tiltle)
    plt.show()

def get_region_3d(distance:int):
    region = []
    for i in range(-distance,distance + 1):
        for j in range(-distance,distance + 1):
            for k in range(-distance, distance + 1):
                if [i,j,k] != [0,0,0] and abs(i) + abs(j) + abs(k) < 3:
                    region.append(np.array([i,j,k]))
    return np.array(region)

def get_region_3d_27(distance:int):
    region = []
    for i in range(-distance,distance + 1):
        for j in range(-distance,distance + 1):
            for k in range(-distance, distance + 1):
                if [i,j,k] != [0,0,0]:
                    region.append(np.array([i,j,k]))
    return np.array(region)

def find_same_postion_ijk_in_vessel_surface_points(ijk, vessel_points):
    result = []
    i, j, k = [float(x) for x in ijk]
    for index,vessel_point in enumerate(vessel_points):
        ii, jj, kk = [float(x) for x in vessel_point.position_ijk]
        if i==ii and j==jj and k==kk:
            result.append(index)
    # if len(result) != 1:
        # print("Test")
    if len(result) >0:
        return result[-1]
    else:
        return None

def find_same_postion_ijk_in_points_list(ijk, vessel_points):
    result = []
    i, j, k = [float(x) for x in ijk]
    for index,vessel_point in enumerate(vessel_points):
        ii, jj, kk = [float(x) for x in vessel_point.position_ijk]
        if i==ii and j==jj and k==kk:
            result.append(index)
    return result

#TO DO
def find_center_point_in_vessel_surface_points(vessel_surface_points):
    totol_postion_ijk = np.array([0,0,0]).astype(float)
    for surface_vessel_point in vessel_surface_points:
        totol_postion_ijk += surface_vessel_point.position_ijk
    center_position_ijk = totol_postion_ijk / len(vessel_surface_points)
    min_distance = 1000
    center_point_index = -1
    for index,surface_vessel_point in enumerate(vessel_surface_points):
        i_distance = np.sum(np.abs(center_position_ijk - surface_vessel_point.position_ijk))
        if i_distance < min_distance:
            center_point_index = index
            min_distance = i_distance
    return center_point_index

def separate_vessel_surface_points(src_points:list):
    surfaces = []
    all_surface_points = copy.deepcopy(src_points)


    REGION = get_region_3d(1)
    while len(all_surface_points)!=0:
        i_surface_points = []
        vcGrowPt = []

        root_vessel_point = all_surface_points[0]
        all_surface_points.pop(0)
        i_surface_points.append(root_vessel_point)
        vcGrowPt.append(root_vessel_point)

        while len(vcGrowPt)!=0:
            anchor_point = vcGrowPt[0]
            vcGrowPt.pop(0)
            for neighbor in REGION:
                grow_point_position_ijk = anchor_point.position_ijk + np.array(neighbor)
                same_postion_index = find_same_postion_ijk_in_vessel_surface_points(grow_point_position_ijk, all_surface_points)
                if same_postion_index != None:
                    vcGrowPt.append(all_surface_points[same_postion_index])
                    i_surface_points.append(all_surface_points[same_postion_index])
                    all_surface_points.pop(same_postion_index)
        if len(i_surface_points) > 0:
            surfaces.append(i_surface_points)
    return surfaces

def FAST_separate_vessel_surface_points(src_points:list):
    surfaces = []
    REGION = get_region_3d(1)
    all_surface_points_array = np.array(copy.deepcopy(src_points))[:,np.newaxis,:]
    all_surface_points_array = np.repeat(all_surface_points_array,REGION.shape[0],axis=1)

    while all_surface_points_array.shape[0] > 0:
        i_surface_points = []
        vcGrowPt = []
        root_vessel_point = all_surface_points_array[0,0,:]
        all_surface_points_array = np.delete(all_surface_points_array,[0],axis=0)
        i_surface_points.append(root_vessel_point)
        vcGrowPt.append(root_vessel_point)

        while len(vcGrowPt)!=0:
            anchor_point = vcGrowPt[0]
            vcGrowPt.pop(0)
            grow_point_position_ijk_array = REGION + anchor_point
            delta = np.sum(np.abs(all_surface_points_array - grow_point_position_ijk_array),axis=-1)
            same_postion_index = np.where(delta==0)
            for index in same_postion_index[0]:
                i_point = all_surface_points_array[index,0,:]
                vcGrowPt.append(i_point)
                i_surface_points.append(i_point)
            all_surface_points_array = np.delete(all_surface_points_array,list(same_postion_index[0]),axis=0)
        if len(i_surface_points) > 0:
            surfaces.append(i_surface_points)
    return surfaces
#357 303 19
def FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_POINT(src_volume:VolumeImage,mask_volume:VolumeImage, dst_volume:VolumeImage, root_point:Vessel_Point, upper_value, lower_value, candidates_vessel_point):
    candidates_vessel_point.clear()
    all_searched_vessel_point = []
    vcGrowPt = []

    root_point.self_point_index = 0
    root_point.parent_point_index = -1
    vcGrowPt.append(root_point)
    all_searched_vessel_point.append(root_point)

    REGION = get_region_3d(1)
    searched_point_count = 0
    grow_time_start = time.time()
    while len(vcGrowPt)!=0 and searched_point_count < MAX_SEARCH_POINT:
        i_grow_time_start = time.time()
        anchor_point = vcGrowPt[0]
        vcGrowPt.pop(0)
        anchor_point_src_value = src_volume.get_value_in_ijk(anchor_point.position_ijk)
        for neighbor in REGION:
            grow_point_position_ijk = anchor_point.position_ijk + np.array(neighbor)
            if not ijk_within_size(grow_point_position_ijk,src_volume.size):continue

            grow_point_mask_value = mask_volume.get_value_in_ijk(grow_point_position_ijk)
            grow_point_src_value = src_volume.get_value_in_ijk(grow_point_position_ijk)

            if grow_point_mask_value == 0:
                mask_volume.set_value_in_ijk(grow_point_position_ijk, VESSEL_LABEL)
                searched_point_count += 1
                if upper_value > grow_point_src_value and \
                    lower_value < grow_point_src_value and \
                        anchor_point_src_value * 0.6 < grow_point_src_value:
                    dst_volume.set_value_in_ijk(grow_point_position_ijk, VESSEL_LABEL)
                    grow_vessel_point = Vessel_Point()
                    grow_vessel_point.position_ijk = grow_point_position_ijk
                    grow_vessel_point.position_geometry = src_volume.convert_ijk_to_geometry(
                        grow_point_position_ijk)
                    grow_vessel_point.parent_point_index = anchor_point.self_point_index
                    grow_vessel_point.self_point_index = len(all_searched_vessel_point)
                    vcGrowPt.append(grow_vessel_point)
                    all_searched_vessel_point.append(grow_vessel_point)

        print(f"each region grow time cost: +{(time.time() - i_grow_time_start):.4f}")

    print(f"region grow time cost: +{(time.time() - grow_time_start):.4f}")
    # for surface_vessel_point in vcGrowPt:
    #     dst_volume.set_value_in_ijk(surface_vessel_point.position_ijk, VESSEL_SURFACE_LABEL)

    dst_volume.fresh_image_data_array_from_itk_image()
    # dst_volume.image_data_array = distance_filter(dst_volume.image_data_array)
    # dst_volume.fresh_itk_image_from_image_data_array()
    # GetCenterlineOfVesselVolumeonErosion(dst_volume)
    # print("dest volume none zero points:" + str(((np.nonzero(dst_volume.image_data_array))[0]).size))


    mask_volume.fresh_image_data_array_from_itk_image()
    # print("mask volume none zero points:" + str(((np.nonzero(mask_volume.image_data_array))[0]).size))

    separate_surface_time_start = time.time()
    surfaces = separate_vessel_surface_points(vcGrowPt)
    print("seperate vessel surface:" + str(len(surfaces)))
    print(f"separate vesel surfce time cost: +{(time.time() - separate_surface_time_start):.4f}")

    vessel_section_seed_points = []

    for surface_index,surface in enumerate(surfaces):
        for surface_vessel_point in surface:
            dst_volume.set_value_in_ijk(surface_vessel_point.position_ijk, VESSEL_SURFACE_LABEL + surface_index)
        i_surface_center_point_index = find_center_point_in_vessel_surface_points(surface)
        vessel_section_seed_points.append(surface[i_surface_center_point_index])
    # points of every vessel section
    candidates_vessel_point.clear()
    for surface_index,seed_point in enumerate(vessel_section_seed_points):
        isinstance(seed_point,Vessel_Point)
        i_section_points = []
        parent_point_index = seed_point.parent_point_index
        while parent_point_index != 0:
            i_point = all_searched_vessel_point[parent_point_index]
            dst_volume.set_value_in_ijk(i_point.position_ijk, VESSEL_CENTERLINE_LABEL + surface_index)
            parent_point_index = i_point.parent_point_index
            i_section_points.append(i_point)
        candidates_vessel_point.append(i_section_points)

def FAST_FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_POINT(large_scale_vessel_volume:VolumeImage, dst_volume:VolumeImage, root_point:Vessel_Point, candidates_vessel_point):
    candidates_vessel_point.clear()
    mask_volume = ConstantVolumeImage(large_scale_vessel_volume,0)
    all_searched_vessel_point = []
    vcGrowPt = []

    root_point.self_point_index = 0
    root_point.parent_point_index = -1
    vcGrowPt.append(root_point)
    all_searched_vessel_point.append(root_point)

    REGION = get_region_3d(1)
    searched_point_count = 0
    grow_time_start = time.time()
    mask_data_array = copy.deepcopy(mask_volume.image_data_array)
    src_data_array = copy.deepcopy(large_scale_vessel_volume.image_data_array)
    dst_data_array = copy.deepcopy(dst_volume.image_data_array)
    while len(vcGrowPt)!=0 and searched_point_count < MAX_SEARCH_POINT:
        i_grow_time_start = time.time()
        anchor_point = vcGrowPt[0]
        vcGrowPt.pop(0)
        anchor_x,anchor_y,anchor_z = anchor_point.position_ijk.astype(int)
        anchor_point_src_value = src_data_array[anchor_x,anchor_y,anchor_z]

        grow_point_position_ijk_array = REGION + np.array([anchor_x,anchor_y,anchor_z])
        grow_point_position_ijk_array = np.minimum(grow_point_position_ijk_array, np.array(large_scale_vessel_volume.size) - 1)
        grow_point_position_ijk_array = np.maximum(grow_point_position_ijk_array,np.array([0,0,0]))
        grow_point_position_ijk_array = np.array(list(set([tuple(point) for point in grow_point_position_ijk_array])))
        grow_point_mask_value_array = mask_data_array[
            grow_point_position_ijk_array[:, 0], grow_point_position_ijk_array[:, 1], grow_point_position_ijk_array[:, 2]]
        unchecked_mask_point_ijk = grow_point_position_ijk_array[np.where(grow_point_mask_value_array == 0)]
        mask_data_array[
            unchecked_mask_point_ijk[:, 0], unchecked_mask_point_ijk[:, 1], unchecked_mask_point_ijk[:, 2]] = VESSEL_LABEL
        unchecked_grow_point_src_value_array =src_data_array[
            unchecked_mask_point_ijk[:, 0], unchecked_mask_point_ijk[:, 1], unchecked_mask_point_ijk[:,2]]
        labeling_index = np.where(unchecked_grow_point_src_value_array == VESSEL_LABEL)
        labeling_src_point_ijk = unchecked_mask_point_ijk[labeling_index]
        dst_data_array[
            labeling_src_point_ijk[:, 0], labeling_src_point_ijk[:, 1], labeling_src_point_ijk[:, 2]] = VESSEL_LABEL
        for grow_point_position_ijk in labeling_src_point_ijk:
            grow_vessel_point = Vessel_Point()
            grow_vessel_point.position_ijk = grow_point_position_ijk
            grow_vessel_point.position_geometry = large_scale_vessel_volume.convert_ijk_to_geometry(
                grow_point_position_ijk)
            grow_vessel_point.parent_point_index = anchor_point.self_point_index
            grow_vessel_point.self_point_index = len(all_searched_vessel_point)
            vcGrowPt.append(grow_vessel_point)
            all_searched_vessel_point.append(grow_vessel_point)
        searched_point_count+= len(unchecked_mask_point_ijk)
        # print(f"each region grow time cost: +{(time.time() - i_grow_time_start):.4f}")
    print(f"region grow time cost: +{(time.time() - grow_time_start):.4f}")
    # for surface_vessel_point in vcGrowPt:
    #     dst_volume.set_value_in_ijk(surface_vessel_point.position_ijk, VESSEL_SURFACE_LABEL)

    dst_volume.set_image_data_array(dst_data_array)
    # dst_volume.image_data_array = distance_filter(dst_volume.image_data_array)
    # dst_volume.fresh_itk_image_from_image_data_array()
    # GetCenterlineOfVesselVolumeonErosion(dst_volume)
    print("dest volume none zero points:" + str(((np.nonzero(dst_volume.image_data_array))[0]).size))


    mask_volume.set_image_data_array(mask_data_array)
    # print("mask volume none zero points:" + str(((np.nonzero(mask_volume.image_data_array))[0]).size))

    separate_surface_time_start = time.time()
    vcGrowPt_array = [np.array(point.position_ijk) for point in vcGrowPt]
    surfaces = FAST_separate_vessel_surface_points(vcGrowPt_array)
    print("seperate vessel surface:" + str(len(surfaces)))
    print(f"separate vesel surfce time cost: +{(time.time() - separate_surface_time_start):.4f}")

    vessel_section_seed_points = []

    for surface_index,surface in enumerate(surfaces):
        surface = [vcGrowPt[find_same_postion_ijk_in_vessel_surface_points(point,vcGrowPt)] for point in surface]
        for surface_vessel_point in surface:
            point_position_ijk = surface_vessel_point.position_ijk
            # point_position_ijk = surface_vessel_point
            dst_volume.set_value_in_ijk(point_position_ijk, VESSEL_SURFACE_LABEL + surface_index)
        i_surface_center_point_index = find_center_point_in_vessel_surface_points(surface)
        vessel_section_seed_points.append(surface[i_surface_center_point_index])
    # points of every vessel section
    candidates_vessel_point.clear()
    for surface_index,seed_point in enumerate(vessel_section_seed_points):
        isinstance(seed_point,Vessel_Point)
        i_section_points = []
        parent_point_index = seed_point.parent_point_index
        while parent_point_index != 0:
            i_point = all_searched_vessel_point[parent_point_index]
            # dst_volume.set_value_in_ijk(i_point.position_ijk, VESSEL_CENTERLINE_LABEL + surface_index)
            parent_point_index = i_point.parent_point_index
            i_section_points.append(i_point)
        candidates_vessel_point.append(i_section_points)

def FAST_FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_DISTANCE(src_volume:VolumeImage, dst_volume:VolumeImage, root_point:Vessel_Point, upper_value, lower_value,max_search_distance = MAX_SEARCH_DISTANCE):
    all_searched_vessel_point = []
    vcGrowPt = []

    root_point.self_point_index = 0
    root_point.parent_point_index = -1
    vcGrowPt.append(root_point)
    all_searched_vessel_point.append(root_point)

    REGION = get_region_3d(1)
    searched_point_count = 0
    grow_time_start = time.time()

    src_data_array = copy.deepcopy(src_volume.image_data_array)
    non_zero_count = np.nonzero(src_data_array)[0].shape[0]
    dst_data_array = copy.deepcopy(dst_volume.image_data_array)
    mask_volume = ConstantVolumeImage(src_volume,0)
    mask_data_array = copy.deepcopy(mask_volume.image_data_array)
    searched_point_distance = 0
    while len(vcGrowPt)!=0 and vcGrowPt[0].distance_from_root_point < max_search_distance and len(all_searched_vessel_point) < non_zero_count * 0.6:
        i_grow_time_start = time.time()
        anchor_point = vcGrowPt[0]
        vcGrowPt.pop(0)
        anchor_x,anchor_y,anchor_z = anchor_point.position_ijk.astype(int)
        anchor_point_src_value = src_data_array[anchor_x,anchor_y,anchor_z]

        grow_point_position_ijk_array = REGION + np.array([anchor_x,anchor_y,anchor_z])
        grow_point_position_ijk_array = np.minimum(grow_point_position_ijk_array,np.array(src_volume.size) - 1)
        grow_point_position_ijk_array = np.maximum(grow_point_position_ijk_array,np.array([0,0,0]))
        grow_point_position_ijk_array = np.array(list(set([tuple(point) for point in grow_point_position_ijk_array])))
        grow_point_mask_value_array = mask_data_array[
            grow_point_position_ijk_array[:, 0], grow_point_position_ijk_array[:, 1], grow_point_position_ijk_array[:, 2]]
        unchecked_mask_point_ijk = grow_point_position_ijk_array[np.where(grow_point_mask_value_array == 0)]
        mask_data_array[
            unchecked_mask_point_ijk[:, 0], unchecked_mask_point_ijk[:, 1], unchecked_mask_point_ijk[:, 2]] = VESSEL_LABEL
        unchecked_grow_point_src_value_array =src_data_array[
            unchecked_mask_point_ijk[:, 0], unchecked_mask_point_ijk[:, 1], unchecked_mask_point_ijk[:,2]]
        labeling_index = np.intersect1d(np.where(unchecked_grow_point_src_value_array < upper_value),
                                        np.where(unchecked_grow_point_src_value_array > lower_value),
                                        np.where(unchecked_grow_point_src_value_array > (0.9 * anchor_point_src_value)))
        if len(list(labeling_index)) > 0:
            labeling_src_point_ijk = unchecked_mask_point_ijk[labeling_index]
            dst_data_array[
                labeling_src_point_ijk[:, 0], labeling_src_point_ijk[:, 1], labeling_src_point_ijk[:, 2]] = VESSEL_LABEL
            for grow_point_position_ijk in labeling_src_point_ijk:
                grow_vessel_point = Vessel_Point()
                grow_vessel_point.position_ijk = grow_point_position_ijk
                grow_vessel_point.position_geometry = src_volume.convert_ijk_to_geometry(
                    grow_point_position_ijk)
                grow_vessel_point.parent_point_index = anchor_point.self_point_index
                grow_vessel_point.self_point_index = len(all_searched_vessel_point)
                grow_vessel_point.distance_from_root_point = np.linalg.norm(grow_vessel_point.position_geometry - anchor_point.position_geometry) + anchor_point.distance_from_root_point
                vcGrowPt.append(grow_vessel_point)
                all_searched_vessel_point.append(grow_vessel_point)
            searched_point_count+= len(unchecked_mask_point_ijk)

    print(f"region grow time cost: +{(time.time() - grow_time_start):.4f}")

    dst_volume.set_image_data_array(dst_data_array)
    # print("dest volume none zero points:" + str(((np.nonzero(dst_volume.image_data_array))[0]).size))
    mask_volume.set_image_data_array(mask_data_array)
    # print("mask volume none zero points:" + str(((np.nonzero(mask_volume.image_data_array))[0]).size))
    return all_searched_vessel_point,vcGrowPt

def FAST_ARRAY_FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_DISTANCE(src_data_array:np.array, dst_data_array:np.array, root_point:np.array, upper_value, lower_value,max_search_distance = MAX_SEARCH_DISTANCE):
    dst_data_array = np.copy(dst_data_array)
    all_searched_vessel_point = []
    vcGrowPt = []
    self_index_record = []
    parent_index_record = []
    distance_record = []

    vcGrowPt.append(root_point)
    all_searched_vessel_point.append(root_point)
    self_index_record.append(0)
    parent_index_record.append(-1)
    distance_record.append(0.0)

    REGION = get_region_3d(1)
    searched_point_count = 0
    grow_time_start = time.time()


    mask_data_array = np.zeros_like(src_data_array)
    searched_point_distance = 0
    while len(vcGrowPt)!=0 and distance_record[-1] < max_search_distance:
        anchor_point = vcGrowPt[0]
        anchor_index = self_index_record[0]
        anchor_distance = distance_record[0]
        vcGrowPt.pop(0)
        parent_index_record.pop(0)
        distance_record.pop(0)
        self_index_record.pop(0)

        anchor_x,anchor_y,anchor_z = anchor_point
        anchor_point_src_value = src_data_array[anchor_x,anchor_y,anchor_z]

        grow_point_position_ijk_array = REGION + np.array([anchor_x,anchor_y,anchor_z])
        grow_point_position_ijk_array = np.minimum(grow_point_position_ijk_array,np.array(src_data_array.shape) - 1)
        grow_point_position_ijk_array = np.maximum(grow_point_position_ijk_array,np.array([0,0,0]))
        grow_point_position_ijk_array = np.array(list(set([tuple(point) for point in grow_point_position_ijk_array])))
        grow_point_mask_value_array = mask_data_array[
            grow_point_position_ijk_array[:, 0], grow_point_position_ijk_array[:, 1], grow_point_position_ijk_array[:, 2]]
        unchecked_mask_point_ijk = grow_point_position_ijk_array[np.where(grow_point_mask_value_array == 0)]
        mask_data_array[
            unchecked_mask_point_ijk[:, 0], unchecked_mask_point_ijk[:, 1], unchecked_mask_point_ijk[:, 2]] = VESSEL_LABEL
        unchecked_grow_point_src_value_array =src_data_array[
            unchecked_mask_point_ijk[:, 0], unchecked_mask_point_ijk[:, 1], unchecked_mask_point_ijk[:,2]]
        labeling_index = np.intersect1d(np.where(unchecked_grow_point_src_value_array < upper_value),
                                        np.where(unchecked_grow_point_src_value_array > lower_value),
                                        np.where(unchecked_grow_point_src_value_array > 0.6 * anchor_point_src_value))
        labeling_src_point_ijk = unchecked_mask_point_ijk[labeling_index]
        dst_data_array[
            labeling_src_point_ijk[:, 0], labeling_src_point_ijk[:, 1], labeling_src_point_ijk[:, 2]] = VESSEL_LABEL

        distance_array = np.linalg.norm(labeling_src_point_ijk - anchor_point,axis=-1) + anchor_distance

        vcGrowPt.extend(list(labeling_src_point_ijk))
        valid_vessel_points_count = len(all_searched_vessel_point)
        self_index_record.extend([i + valid_vessel_points_count for i in range(len(labeling_src_point_ijk))])
        parent_index_record.extend([anchor_index for i in range(len(labeling_src_point_ijk))])
        distance_record.extend(list(distance_array))
        all_searched_vessel_point.extend(list(labeling_src_point_ijk))
        searched_point_count += len(unchecked_mask_point_ijk)

    print(f"region grow time cost: +{(time.time() - grow_time_start):.4f}")
    return all_searched_vessel_point,vcGrowPt,self_index_record,parent_index_record,dst_data_array




def laplacian_filter(src_array:np.array):
    dst_array = copy.deepcopy(src_array)

def distance_filter(src_array:np.array):


    dst_array = copy.deepcopy(src_array)
    n_x,n_y,n_z = np.nonzero(src_array)

    #get surface points
    surface_points = []
    REGION =get_region_3d(1)
    for index in range(len(n_x)):
        x = n_x[index]
        y = n_y[index]
        z = n_z[index]
        for region in REGION:
            grow_x,grow_y,grow_z = np.array([x,y,z]) + region
            if src_array[int(grow_x),int(grow_y),int(grow_z)] == 0:
                surface_points.append(np.array([int(grow_x),int(grow_y),int(grow_z)]))
                break
    print("surface time:" + str(len(surface_points)))
    surface_points = np.array(surface_points)
    #cacualte min distance from surface points
    min_distance_array = np.zeros_like(n_x).astype(float)

    calculate_distance_start_time = time.time()
    for index in range(len(n_x)):
        x = n_x[index]
        y = n_y[index]
        z = n_z[index]
        i_min_distance = 1000
        every_point_start_time = time.time()
        distance = np.sum(np.square(surface_points - np.array([x, y, z])), axis=-1)
        min_distance_array[index] = np.sqrt(distance.min())

        # for surface_point in surface_points:
        #     every_surface_start_time = time.time()
        #     sx,sy,sz = surface_point
        #     vector = np.array([sx,sy,sz]) - np.array([x,y,z])
        #     i_distance = np.sqrt(np.sum(np.power(vector,2)))
        #     if i_distance < i_min_distance:
        #         i_min_distance = i_distance
        #         min_distance_array[index] = i_distance
            # print(f"every surface  time cost: +{(time.time() - every_surface_start_time):.4f}")
        # print(f"every point time cost: +{(time.time() - every_point_start_time):.4f}")
    print(f"calculate distance time cost: +{(time.time() - calculate_distance_start_time):.4f}")
    for index,min_distance in enumerate(min_distance_array):
        x = n_x[index]
        y = n_y[index]
        z = n_z[index]
        dst_array[x,y,z] = round(min_distance*100)
    return  dst_array

def LOG_filter(src_array:np.array):
    dst_array = copy.deepcopy(src_array)
    n_x,n_y,n_z = np.nonzero(src_array)

    return  dst_array

def gaussian_filter(src_array:np.array, K_size=3, sigma=1.3):
    img = np.asarray(src_array)
    H, W, D = img.shape

    ## Zero padding
    pad = K_size // 2
    out = np.zeros((H + pad * 2, W + pad * 2, D + pad * 2), dtype=np.float)
    out[pad: pad + H, pad: pad + W,pad: pad + D] = img.copy().astype(np.float)

    ## prepare Kernel
    K = np.zeros((K_size, K_size,K_size), dtype=np.float)
    for x in range(-pad, -pad + K_size):
        for y in range(-pad, -pad + K_size):
            for z in range(-pad, -pad + K_size):
                K[y + pad, x + pad] = np.exp(-(x ** 2 + y ** 2) / (2 * (sigma ** 2)))
    K /= (2 * np.pi * sigma * sigma)
    K /= K.sum()
    tmp = out.copy()

    # filtering
    for y in range(H):
        for x in range(W):
            for c in range(D):
                out[pad + y, pad + x, c] = np.sum(K * tmp[y: y + K_size, x: x + K_size, c])
    out = np.clip(out, 0, 255)
    out = out[pad: pad + H, pad: pad + W].astype(np.uint8)
    return out

def erosion_filter(src_array:np.array):
    dst_array = copy.deepcopy(src_array)
    n_x,n_y,n_z = np.nonzero(src_array)
    REGION =get_region_3d(1)
    for index in range(len(n_x)):
        x = n_x[index]
        y = n_y[index]
        z = n_z[index]
        for region in REGION:
            grow_x,grow_y,grow_z = np.array([x,y,z]) + region
            if src_array[int(grow_x),int(grow_y),int(grow_z)] == 0:
                dst_array[int(x),int(y),int(z)] = 20
                break
    return dst_array

def test_filter(src_volume:VolumeImage):
    data_array = src_volume.image_data_array
    erode_data_array = scipy.ndimage.binary_erosion(data_array,structure=np.ones((3,3,3)))
    data_array[erode_data_array] = 0
    src_volume.set_image_data_array(data_array)

def resample_centerline_between_two_points(distance_volume:VolumeImage, mask_volume:VolumeImage, point_pair:list, divide = 10):
    p1 = np.array(point_pair[0])
    p2 = np.array(point_pair[1])

    mask_data_array = mask_volume.image_data_array
    refine_centerline_points = [p1]
    direction_euler = p2 - p1
    direction_polar = rectangular2polar(direction_euler)
    L = np.linalg.norm(direction_euler)
    L_each_step = L / divide
    phi_stride = 1
    roi_radius = 5 / np.average(np.array(distance_volume.spacing))/2

    roi_middle_point_cutsurface_array = get_roi_cylinder_along_specific_line(point_pair, L_each_step, divide, 1, roi_radius,5,int(360/5))[0]
    #TODO: this int operation make result looks better but maybe not precise
    roi_middle_point_cutsurface_array = roi_middle_point_cutsurface_array.astype("int")
    L_SIZE,THETA_SIZE,R_SIZE = roi_middle_point_cutsurface_array.shape[:-1]
    distance_value_roi_points = distance_volume.get_value_in_ijk_array(roi_middle_point_cutsurface_array.reshape((-1,3))).reshape(L_SIZE,THETA_SIZE,R_SIZE)
    max_distance_index_every_L = np.argmax(distance_value_roi_points.reshape(L_SIZE,-1),axis=-1)
    max_distance_index_every_L = np.unravel_index(max_distance_index_every_L,(THETA_SIZE,R_SIZE))
    refine_centerline_points = roi_middle_point_cutsurface_array[range(L_SIZE),max_distance_index_every_L[0],max_distance_index_every_L[1]]

    return refine_centerline_points

    for L_step in range(1,divide):
        direction_polar[0] = L_each_step * L_step
        i_point = GetNewPosition_PolarDirection(p1,direction_polar)
        roi_middle_point_cutsurface = []
        for phi_step in range(int(360 / phi_stride)):
            vertical_direction_polar, _ = GetVerticalVectorOfSpecificVector_Polor2Polar(direction_polar,
                                                                                        phi_step * phi_stride)
            for r in range(0, roi_radius):
                vertical_direction_polar[0] = r
                vertical_point = GetNewPosition_PolarDirection(i_point, vertical_direction_polar)
                vertical_point = np.round(np.array(vertical_point)).astype(int)
                vertical_point_value = mask_data_array[vertical_point[0], vertical_point[1], vertical_point[2]]
                if vertical_point_value > 0:
                    roi_middle_point_cutsurface.append(vertical_point)
        roi_middle_point_cutsurface = np.array(roi_middle_point_cutsurface).astype(int)
        pass
        try:
            distance_array = distance_volume.image_data_array[
            roi_middle_point_cutsurface[:, 0], roi_middle_point_cutsurface[:, 1], roi_middle_point_cutsurface[:, 2]]
        except:
            print("error")
        max_distance_index = np.argmax(distance_array)
        max_distance = distance_array[max_distance_index]
        if sum(i_point.astype(int) == np.array([-1, 0, 0])) == 3 :
            # show_3d_points(roi_middle_point_cutsurface[:,0],roi_middle_point_cutsurface[:,1],roi_middle_point_cutsurface[:,2])
            for temp_point in roi_middle_point_cutsurface:
                x,y,z = temp_point
                mask_volume.set_value_in_ijk([x,y,z],88)
            print(roi_middle_point_cutsurface[max_distance_index])
            print(max_distance)
        center_point = roi_middle_point_cutsurface[max_distance_index]
        refine_centerline_points.append(np.array(center_point))
    # if len(temp_x)>0:
    #     temp_x.append(vertical_point[0])
    #     temp_y.append(vertical_point[1])
    #     temp_z.append(vertical_point[2])
    #     print("center point:")
    #     print(vertical_point)
    #     show_3d_points(temp_x,temp_y,temp_z)
    # for temp_point in temp_interp_point:
    #     x, y, z = np.round(temp_point).astype("int")
    #     mask_volume.set_value_in_ijk([x, y, z], 66)
    # if sum(p2 == np.array([356, 166, 192])) == 3 :
    #     test1 = rectangular2polar(direction_euler)
    #     test2 = polar2rectangular(test1)
        # for temp_point in temp_interp_point:
        #     x,y,z = np.round(temp_point).astype("int")
        #     mask_volume.set_value_in_ijk([x,y,z],66)

    # refine_centerline_points.append(p2)
    refine_centerline_points = np.array(refine_centerline_points)

    return refine_centerline_points

def resample_centerline_between_two_points_geometry(distance_volume:VolumeImage, mask_volume:VolumeImage, point_pair_geometry:list, divide = 10):
    p1 = np.array(point_pair_geometry[0])
    p2 = np.array(point_pair_geometry[1])

    refine_centerline_points = [p1]
    direction_euler = p2 - p1
    direction_polar = rectangular2polar(direction_euler)
    L = np.linalg.norm(direction_euler)
    stride = L / divide
    phi_stride = 5

    roi_radius = int(round(5 / np.average(np.array(distance_volume.spacing)))) * 2
    temp_interp_point = []
    for i in range(1,divide):
        direction_polar[0] = stride * i
        i_point = GetNewPosition_PolarDirection(p1,direction_polar)
        temp_interp_point.append(i_point)
        max_distance = 0
        max_distance_point = np.array([0,0,0])
        max_ditance_position = []
        for phi_step in range(int(360 / phi_stride)):
            vertical_direction_polar, _ = GetVerticalVectorOfSpecificVector_Polor2Polar(direction_polar,
                                                                                        phi_step * phi_stride)
            for r_step in range(0, roi_radius):
                vertical_direction_polar[0] = r_step * np.average(np.array(distance_volume.spacing)) / 2
                vertical_point = GetNewPosition_PolarDirection(i_point, vertical_direction_polar)
                vertical_point_value = mask_volume.get_value_in_geometry(vertical_point)
                if vertical_point_value > 0:
                    vertical_point_distance = distance_volume.get_value_in_geometry(vertical_point)
                    if vertical_point_distance > max_distance:
                        max_distance = vertical_point_distance
                        max_distance_point = vertical_point
                        max_ditance_position = [phi_step * phi_stride,r_step]
                else:
                    break
        # print(max_ditance_position)
        refine_centerline_points.append(np.copy(np.array(max_distance_point)))
    # if len(temp_x)>0:
    #     temp_x.append(vertical_point[0])
    #     temp_y.append(vertical_point[1])
    #     temp_z.append(vertical_point[2])
    #     print("center point:")
    #     print(vertical_point)
    #     show_3d_points(temp_x,temp_y,temp_z)
    for temp_point in temp_interp_point:
        x, y, z = np.round(temp_point).astype("int")
        # mask_volume.set_value_in_ijk([x, y, z], 66)
    if sum(p2 == np.array([356, 166, 192])) == 3 :
        test1 = rectangular2polar(direction_euler)
        test2 = polar2rectangular(test1)
        # for temp_point in temp_interp_point:
        #     x,y,z = np.round(temp_point).astype("int")
        #     mask_volume.set_value_in_ijk([x,y,z],66)

    # refine_centerline_points.append(p2)
    refine_centerline_points = np.array(refine_centerline_points)
    return refine_centerline_points

def find_best_centerline_of_vessel_section_on_distance_map(distance_volume:VolumeImage, rough_centerline_points:list, stride = 10):

    # find max distance around every 10 rough centerline piont
    low_index = 0
    num_points = len(rough_centerline_points)
    up_index = min(num_points-1,stride)
    refine_centerline_points = []
    roi_radius = int(round(5 / np.average(np.array(distance_volume.spacing))))
    while up_index > low_index and up_index < num_points:
        section_rough_centerline_points = rough_centerline_points[low_index:up_index + 1]
        roi_middle_point_cutsurface = get_intersection_surface_of_vessel(section_rough_centerline_points,roi_radius)
        distance_array = distance_volume.image_data_array[roi_middle_point_cutsurface[:,0],roi_middle_point_cutsurface[:,1],roi_middle_point_cutsurface[:,2]]
        max_distance_index = np.argmax(distance_array)
        max_distance = distance_array[max_distance_index]
        center_point = roi_middle_point_cutsurface[max_distance_index]
        refine_centerline_points.append(np.array(center_point))
        low_index  = up_index
        up_index += stride
        up_index = min(num_points - 1,up_index)
    return refine_centerline_points

def get_intersection_surface_of_vessel(anchor_point:np.array, vessel_direction_Euler:np.array, roi_radius:int, mask_volume:VolumeImage):
    # get roi
    ROI = []
    phi_stride = 5
    direction_polar = rectangular2polar(vessel_direction_Euler)
    for phi_step in range(int(360 / phi_stride)):
        vertical_direction_polar, _ = GetVerticalVectorOfSpecificVector_Polor2Polar(direction_polar,
                                                                                    phi_step * phi_stride)
        for r in range(0, roi_radius):
            vertical_direction_polar[0] = r
            vertical_point = GetNewPosition_PolarDirection(anchor_point, vertical_direction_polar)
            ROI.append(np.array(vertical_point))

    ROI = np.round(np.array(ROI)).astype(int)
    ROI = np.array(list(set([tuple(t) for t in np.round(ROI)])))
    return ROI

def get_connected_intersection_surface_of_vessel(anchor_point_ijk:np.array, vessel_direction_Euler:np.array, roi_radius:int, mask_volume:VolumeImage):
    # get roi
    ROI = []
    phi_stride = 5
    direction_polar = rectangular2polar(vessel_direction_Euler)
    mask_data_array = mask_volume.image_data_array

    anchor_point_ijk = find_nearst_valid_point(anchor_point_ijk,mask_volume)
    for phi_step in range(int(360 / phi_stride)):
        vertical_direction_polar, _ = GetVerticalVectorOfSpecificVector_Polor2Polar(direction_polar,
                                                                                phi_step * phi_stride)
        for r in range(0, roi_radius):
            vertical_direction_polar[0] = r
            vertical_point = GetNewPosition_PolarDirection(anchor_point_ijk, vertical_direction_polar)
            vertical_point = np.round(np.array(vertical_point)).astype(int)
            vertical_point_value = mask_data_array[vertical_point[0],vertical_point[1],vertical_point[2]]
            if vertical_point_value > 0:
                ROI.append(tuple(vertical_point))
            else:
                break
    ROI = np.array(list(set(ROI)))
    try:
        x = ROI[:,0]
        y = ROI[:,1]
        z = ROI[:,2]
    except:
        print("wrong")
    return ROI

def refine_rough_centerline_points_to_OnePoint(rough_centerline_points_ijk:np.array, vessel_volume:VolumeImage, distance_volume:VolumeImage, roi_radius):
    N = len(rough_centerline_points_ijk)
    assert N > 0
    assert roi_radius > 0
    # get vessel direction
    mean_point = np.average(rough_centerline_points_ijk, axis=0)
    delta = rough_centerline_points_ijk - mean_point
    _, _, vv = np.linalg.svd(delta)
    vessel_direction_Euler = vv[0]/np.linalg.norm(vv[0])

    # get anchor point
    round_mean_point = np.round(mean_point).astype(int)
    value_round_mean_point = vessel_volume.image_data_array[round_mean_point[0],round_mean_point[1],round_mean_point[2]]
    anchor_point_ijk = np.array([0,0,0])
    if value_round_mean_point > 0:
        anchor_point_ijk = mean_point
    else:
        anchor_point_ijk = rough_centerline_points_ijk[int(N / 2)]

    intersection_surface_points_ijk = get_connected_intersection_surface_of_vessel(anchor_point_ijk,vessel_direction_Euler,roi_radius,vessel_volume)

    distance_array_of_intersection_surface = distance_volume.image_data_array[
        intersection_surface_points_ijk[:, 0], intersection_surface_points_ijk[:, 1], intersection_surface_points_ijk[:, 2]]
    max_distance_index = np.argmax(distance_array_of_intersection_surface)
    max_distance = distance_array_of_intersection_surface[max_distance_index]
    refine_centerline_point = intersection_surface_points_ijk[max_distance_index]

    return refine_centerline_point


def find_nearst_valid_point(src_point_ijk,vessel_volume:VolumeImage):
    round_point = np.round(src_point_ijk).astype("int")
    src_value = vessel_volume.get_value_in_ijk(round_point)
    if src_value==0:
        for offset in range(5):
            REGION = get_region_3d(5)
            for region in REGION:
                new_point = src_point_ijk + region
                new_point_value = vessel_volume.get_value_in_ijk(new_point)
                if new_point_value == 1:
                    return new_point
    return round_point


def refine_rough_centerline_points_to_OnePoint_on_anchor_point(rough_centerline_points_ijk:np.array, vessel_volume:VolumeImage, distance_volume:VolumeImage, roi_radius):
    N = len(rough_centerline_points_ijk)
    assert N > 0
    assert roi_radius > 0
    # get vessel direction
    mean_point = np.average(rough_centerline_points_ijk, axis=0)
    delta = rough_centerline_points_ijk - mean_point
    _, _, vv = np.linalg.svd(delta)
    vessel_direction_Euler = vv[0]/np.linalg.norm(vv[0])

    # get anchor point
    round_mean_point = np.round(mean_point).astype(int)
    value_round_mean_point = vessel_volume.image_data_array[round_mean_point[0],round_mean_point[1],round_mean_point[2]]
    anchor_point_ijk = rough_centerline_points_ijk[int(N / 2)]

    #what if anchor_point_ijk is out of vessel because of error





    intersection_surface_points_ijk = get_connected_intersection_surface_of_vessel(anchor_point_ijk,vessel_direction_Euler,roi_radius,vessel_volume)

    distance_array_of_intersection_surface = distance_volume.image_data_array[
        intersection_surface_points_ijk[:, 0], intersection_surface_points_ijk[:, 1], intersection_surface_points_ijk[:, 2]]
    max_distance_index = np.argmax(distance_array_of_intersection_surface)
    max_distance = distance_array_of_intersection_surface[max_distance_index]
    refine_centerline_point = intersection_surface_points_ijk[max_distance_index]

    return refine_centerline_point
def get_refine_sparse_centerline_points_of_single_vessel_section(vessel_volume:VolumeImage, distance_volume:VolumeImage, rough_centerline_points_ijk:list, stride = 10):

    # find max distance around every 10 rough centerline piont
    low_index = 0
    num_points = len(rough_centerline_points_ijk)
    up_index = min(num_points-1,stride)
    refine_centerline_points = []
    roi_radius = int(round(5 / np.average(np.array(vessel_volume.spacing))/2))
    while up_index > low_index and up_index < num_points:
        i_rough_centerline_points_ijk = rough_centerline_points_ijk[low_index:up_index + 1]
        center_point = refine_rough_centerline_points_to_OnePoint(i_rough_centerline_points_ijk,vessel_volume,distance_volume,roi_radius)
        refine_centerline_points.append(np.array(center_point))
        low_index  = up_index
        up_index += stride
        up_index = min(num_points - 1,up_index)
    return refine_centerline_points

def get_refine_mi_centerline_points_of_single_vessel_section(vessel_volume:VolumeImage, distance_volume:VolumeImage, rough_centerline_points_ijk:list, stride = 10, roi_radius = 10):

    # find max distance around every 10 rough centerline piont
    low_index = 0
    num_points = len(rough_centerline_points_ijk)
    up_index = min(num_points-1,stride)
    refine_centerline_points = []
    roi_radius_ijk = int(round(roi_radius / np.average(np.array(vessel_volume.spacing)) / 2))
    for index in range(len(rough_centerline_points_ijk)):
        low_index = int(index - stride/2)
        up_index = int(index + stride/2)
        if low_index >=0 and up_index < num_points:
            i_rough_centerline_points_ijk = rough_centerline_points_ijk[low_index:up_index]
            center_point = refine_rough_centerline_points_to_OnePoint_on_anchor_point(i_rough_centerline_points_ijk, vessel_volume, distance_volume, roi_radius_ijk)
            refine_centerline_points.append(np.array(center_point))
    return np.array(refine_centerline_points)



def get_initial_parameters_of_angeioma(src_volume:VolumeImage):
    image_array = src_volume.image_data_array
    pixel_volume = src_volume.spacing[0] * src_volume.spacing[1] * src_volume.spacing[2]
    angeioma_array_index = np.where(image_array == ANGEIOMA_LABEL)
    angeioma_array = np.array(angeioma_array_index).swapaxes(0,1)
    center_point_position_ijk = np.average(angeioma_array,axis=0)
    angeioma_volume = angeioma_array.shape[0] * pixel_volume


    return center_point_position_ijk,angeioma_volume

def get_roi_at_middle_cutsurface(points,radius = 5):
    mean_point = np.average(points,axis=0)
    data = points - mean_point
    _, _, vv = np.linalg.svd(data)
    direction = vv[0]/np.linalg.norm(vv[0])
    # get roi
    ROI = []
    phi_stride = 5
    direction_polar = rectangular2polar(direction)
    for phi_step in range(int(360 / phi_stride)):
        vertical_direction_polar, _ = GetVerticalVectorOfSpecificVector_Polor2Polar(direction_polar,
                                                                                    phi_step * phi_stride)
        for r in range(0, radius):
            vertical_direction_polar[0] = r
            vertical_point = GetNewPosition_PolarDirection(mean_point, vertical_direction_polar)
            ROI.append(np.array(vertical_point))
    ROI = np.array(ROI).astype(int)
    ROI = np.array(list(set([tuple(t) for t in np.round(ROI)])))
    return ROI

def get_roi_cylinder(points,radius = 5):
    # points: [N,3]
    # get nomial_vector
    mean_point = np.average(points,axis=0)
    data = points - mean_point
    _, _, vv = np.linalg.svd(data)
    direction = vv[0]/np.linalg.norm(vv[0])
    nominal_length = int((np.linalg.norm(points[0] - points[-1]))/2)
    # get roi
    ROI = []
    phi_stride = 5
    for l in range(-nominal_length,nominal_length):
        anchor_point = mean_point + l * direction
        direction_polar = rectangular2polar(direction)
        for phi_step in range(int(360 /phi_stride)):
            vertical_direction_polar,_ = GetVerticalVectorOfSpecificVector_Polor2Polar(direction_polar,phi_step * phi_stride)
            for r in range(0,radius):
                vertical_direction_polar[0] = r
                vertical_point = GetNewPosition_PolarDirection(anchor_point,vertical_direction_polar)
                ROI.append(np.array(vertical_point))
    ROI = np.array(ROI).astype(int)
    ROI = np.array(list(set([tuple(t) for t in np.round(ROI)])))
    return ROI

def get_roi_cylinder_along_specific_line(line, length_each_step, length_max_step, r_each_step, r_max_step,phi_each_step = 1,phi_max_step = 360):
    root_point = line[0]
    end_point = line[1]
    direction_euler = line[1] - line[0]
    direction_polar = rectangular2polar(direction_euler)
    #enumerate L
    # array of vector root->anchor:[N_L,3],N_L = round(direction_polar[0] / length_gap)
    L_num = round(direction_polar[0] / length_each_step)
    root_2_anchor_polar_array = np.array([[index * length_each_step, direction_polar[1], direction_polar[2]] for index in range(L_num)])
    try:
        root_2_anchor_euler_array = polar2rectangular_array(root_2_anchor_polar_array)
    except:
        print("error")
    anchor_points = root_point + root_2_anchor_euler_array

    #enumerate phi and R
    # array of vector anchor->(phi,r):[360,R_count,3],R_count = round(r_step)
    #phi_unit_polar_array:[360,3]
    phi_unit_polar_array = np.array([GetVerticalVectorOfSpecificVector_Polor2Polar(direction_polar,phi_step * phi_each_step / 180 * math.pi)[0] for phi_step in range(phi_max_step)])
    # phi_r_eular_array:[360 * R_count,3]
    R_num = round(r_max_step)
    phi_r_polar_array =np.repeat(phi_unit_polar_array,R_num,axis = 0)
    phi_r_polar_array = np.array([[(index % R_num) * r_each_step, phi_r_polar_array[index, 1], phi_r_polar_array[index, 2]] for index in range(phi_r_polar_array.shape[0])])
    phi_r_eular_array = polar2rectangular_array(phi_r_polar_array)


    #root_2_phi_r_euler_array:[N_L,360 * R_count,3]
    root_2_phi_r_euler_array = np.repeat(root_2_anchor_euler_array[:,np.newaxis,:],phi_max_step * R_num,axis=1)
    root_2_phi_r_euler_array = root_2_phi_r_euler_array + phi_r_eular_array


    roi_points = root_2_phi_r_euler_array + root_point
    roi_points = roi_points.reshape((L_num,phi_max_step,R_num,3))
    return roi_points,np.repeat(anchor_points[:,np.newaxis,:],phi_max_step * R_num ,axis=1).reshape((L_num,phi_max_step,R_num,3))

# def if_erosion_valid(src_array:np.array,dst_array:np.array, anchor_point:np.array):
#     REGION =get_region_3d(1)
#     count_src_nonzero = 0
#     stack_region = []
#     has_zero_region = False
#     for region in REGION:
#         x,y,z = anchor_point
#         grow_x, grow_y, grow_z = np.array([x, y, z]) + region
#         if src_array[int(grow_x), int(grow_y), int(grow_z)] == 0:
#             has_zero_region = True
#         else:
#             stack_region.append(np.array(region))
#             count_src_nonzero += 1
#         # if dst_array[int(grow_x), int(grow_y), int(grow_z)] != 0:
#         #     stack_region.append(np.array(region))
#     if count_src_nonzero < 1:
#         return False
#     elif not has_zero_region:
#         return False
#     else:
#         result = check_region_connected(stack_region)
#         return result

def check_remain_points_connected(remain_points):
    if len(remain_points) ==0:
        return False
    points = np.array(remain_points.copy())
    REGION =get_region_3d(1)
    vcGrowPt = [points[0]]
    connected_points = []
    mask_points = []
    while len(vcGrowPt)!=0:
        anchor_point = vcGrowPt[0]
        vcGrowPt.pop(0)
        mask_points.append(anchor_point)  
        connected_points.append(anchor_point)
        for neighbor in REGION:
            neighbor_point = anchor_point + np.array(neighbor)
            if np.any(np.all(remain_points==neighbor_point,axis=1)) and not np.any(np.all(mask_points==neighbor_point,axis=1)):
                mask_points.append(neighbor_point)
                vcGrowPt.append(neighbor_point)

    if len(remain_points) == len(connected_points):
        return True
    return False




def check_region_connected(src_stack_region):
    stack_region = list(src_stack_region.copy())
    if len(stack_region) ==0:
        return False
    REGION =get_region_3d(1)
    vcGrowPt = []
    vcGrowPt.append(stack_region[0])
    stack_region.pop(0)
    while len(vcGrowPt)!=0:
        anchor_point = vcGrowPt[0]
        vcGrowPt.pop(0)
        for neighbor in REGION:
            grow_point_position_ijk = anchor_point + np.array(neighbor)
            for index,t_region in enumerate(stack_region):
                if grow_point_position_ijk[0] == t_region[0] and \
                        grow_point_position_ijk[1] == t_region[1] and \
                        grow_point_position_ijk[2] == t_region[2] :
                        vcGrowPt.append(stack_region[index])
                        stack_region.pop(index)
    if len(stack_region)>0:
        return False
    return True

def if_erosion_valid(src_zero_points,dst_nonzero_points):
    if len(src_zero_points) == 0:
        return False
    tag1 = check_remain_points_connected(dst_nonzero_points)
    tag2 = check_region_connected(dst_nonzero_points)
    return check_region_connected(dst_nonzero_points)



def split_vessel_volume_to_connected_vessel_parts(src_nonzero_points_array,max_part_size):
    src_non_zero_points = copy.deepcopy(src_nonzero_points_array)
    part_index_of_all_non_zero_points = np.zeros((src_non_zero_points.shape[0],1))
    parts_list = []
    
    SMALL_REGION = get_region_3d(1)
    
    while src_non_zero_points.shape[0] != 0:
        i_part_points = []
        vcGrowPt = [src_non_zero_points[0]]
        while len(i_part_points) < max_part_size and len(vcGrowPt) != 0:
            anchor_point = vcGrowPt[0]
            vcGrowPt.pop(0)
            for region_point in SMALL_REGION:
                grow_point = anchor_point + region_point
                find_index = list(np.where(np.all(src_non_zero_points==grow_point,axis = 1) == True)[0])
                if len(find_index) != 0:
                    src_non_zero_points = np.delete(src_non_zero_points,find_index,axis= 0)
                    vcGrowPt.append(grow_point)
                    i_part_points.append(anchor_point)
        parts_list.append(i_part_points)    
    return parts_list    
                
        
    
    
    

def StayConnectedErodeFilter_scimage(src_volume:VolumeImage):
    dst_array = copy.deepcopy(src_volume.image_data_array)
    dst_array = morphology.skeletonize_3d(dst_array)
    return dst_array


def StayConnectedErodeFilterOnce(src_volume:VolumeImage,epoch = 1):
    dst_array = copy.deepcopy(src_volume.image_data_array)
    # dst_array = np.zeros((100,100,100))
    # dst_array[10:90,50,50] = 1
    
    
    # stick_pionts = np.array([[10,50,50],
                        #    [89,50,50]])                           
    
    
    eroded_count = 0
    # vcGrowPt = np.array([])#[np.array([n_x[index],n_y[index],n_z[index]])]
    for step in range(epoch):
        region_size = 1 #+ 2 * (epoch - step)
        SMALL_REGION = get_region_3d(region_size)
        LARGE_REGION = get_region_3d(3)
        LARGE_REGION_SIZE = 1 + 2 * (3)
        step_eroded_count = 0
        src_array = copy.deepcopy(dst_array)
        src_nonzero_points = np.array(np.nonzero(src_array)).swapaxes(1,0) # [N,3]
        # parts_list = split_vessel_volume_to_connected_vessel_parts(src_nonzero_points,1000)
        src_nonzero_points = np.repeat(src_nonzero_points[:,np.newaxis,:],len(SMALL_REGION),axis=1)# [N,21,3]
        delta_points = SMALL_REGION.copy() #[21,3]
        delta_points = np.repeat(delta_points[np.newaxis,:,:],src_nonzero_points.shape[0],axis=0)
        src_nonzero_region_points =  src_nonzero_points + delta_points
        src_nonzero_region_points = src_nonzero_region_points.reshape(-1,3)#[N*21,3]
        src_nonzero_region_points_value = src_array[src_nonzero_region_points[:,0],src_nonzero_region_points[:,1],src_nonzero_region_points[:,2]]#[N*21]
        src_nonzero_region_points_value = src_nonzero_region_points_value.reshape(src_nonzero_points.shape[0],-1)#[N,21]
        src_nonzero_region_points_valid_index = np.sum(src_nonzero_region_points_value,axis=1)
        erode_points = src_nonzero_points[np.where(src_nonzero_region_points_valid_index<26)[0],0,:]#[N,3]
        vcGrowPt = list(
            src_nonzero_points[np.where(src_nonzero_region_points_valid_index<26)[0],0,:]
        )
        
        
        ########### check if connected ############
        erode_points = np.repeat(erode_points[:,np.newaxis,:],len(LARGE_REGION),axis=1)
        delta_points = LARGE_REGION.copy() #[21,3]
        delta_points = np.repeat(delta_points[np.newaxis,:,:],erode_points.shape[0],axis=0)
        erode_region_points =  erode_points + delta_points#[N,21,3]
        erode_region_points = erode_region_points.reshape(-1,3)#[N*21,3]
        erode_dst_region_points_value = dst_array[erode_region_points[:,0],erode_region_points[:,1],erode_region_points[:,2]]#[N*21]
        erode_nonzero_region_tag = (erode_dst_region_points_value>0).reshape(-1,LARGE_REGION.shape[0])
        erode_region_points = erode_region_points.reshape(-1,LARGE_REGION.shape[0],3)#[N*21,3]
        # erode_nonzero_region_points = erode_nonzero_region_points.reshape(-1,erode_points.shape[1],3)
        tag_record = []
        
        
        start_time = time.time()
        for index,points_cluster in enumerate(list(erode_region_points)):
            tag = 0
            if erode_nonzero_region_tag.shape[0] > 0 :
                if True:
                    #not np.any(np.all(stick_pionts==erode_points[index][0],axis=1)):       
                    dst_nonzero_points = points_cluster[erode_nonzero_region_tag[index]]
                    if dst_nonzero_points.shape[0] > 1:
                        map_index = LARGE_REGION[erode_nonzero_region_tag[index]] + np.array([1,1,1])
                        erode_map = np.zeros((LARGE_REGION_SIZE,LARGE_REGION_SIZE,LARGE_REGION_SIZE))
                        erode_map[map_index[:,0],map_index[:,1],map_index[:,2]] = 1
                        dst_array[erode_points[index][0][0], erode_points[index][0][1], erode_points[index][0][2]] = 0
                        tag = scipy.ndimage.label(erode_map)[1]
                        tag_record.append(tag)  
            if tag == 1 :
                step_eroded_count += 1
            else:
                dst_array[erode_points[index][0][0], erode_points[index][0][1], erode_points[index][0][2]] = 1
                print("break connection")
            if len(tag_record) %10 == 0:          
                # print(f"each region grow time cost: +{(time.time() - start_time):.4f}")  
                start_time = time.time()
        # erode_region_points = erode_region_points.reshape(-1,3)#[N*21,3]
        # src_nonzero_region_points_value = src_array[src_nonzero_region_points[:,0],src_nonzero_region_points[:,1],src_nonzero_region_points[:,2]]#[N*21]
        # src_nonzero_region_points_value = src_nonzero_region_points_value.reshape(src_nonzero_points.shape[0],-1)#[N,21]
        
        
        # # vcGrowPt = list(np.array(np.nonzero(src_array)).swapaxes(1,0))
        # # n_x, n_y, n_z = np.nonzero(src_array)
        # # vcGrowPt = [np.array([n_x[index], n_y[index], n_z[index]]) for index in range(len(n_x))]
        # while len(vcGrowPt) != 0:
        #     remain_points = []
        #     anchor_point = vcGrowPt[0]
        #     vcGrowPt.pop(0)
        #     src_zero_points = []
        #     dst_nonzero_points = []
        #     for neighbor in SMALL_REGION:
        #         neighbor_point = anchor_point + neighbor
        #         src_value = src_array[neighbor_point[0], neighbor_point[1], neighbor_point[2]]
        #         dst_value = dst_array[neighbor_point[0], neighbor_point[1], neighbor_point[2]]
        #         if src_value == 0:
        #             src_zero_points.append(neighbor_point)
        #         if dst_value != 0:
        #             dst_nonzero_points.append(neighbor_point)
        #     if len(src_zero_points) != 0 :
        #         print("vcGrowPt has points:",len(vcGrowPt))
        #         LARGE_REGION_POINTS = LARGE_REGION + anchor_point
        #         dst_large_region_array = dst_array[LARGE_REGION_POINTS[:,0],LARGE_REGION_POINTS[:,1],LARGE_REGION_POINTS[:,2]]
        #         dst_large_region_nonzero_points = LARGE_REGION_POINTS[np.where(dst_large_region_array!=0)[0]]    
        #         test = check_remain_points_connected(dst_large_region_nonzero_points)
        #         if if_erosion_valid(src_zero_points,dst_large_region_nonzero_points):
        #             dst_array[anchor_point[0], anchor_point[1], anchor_point[2]] = 0
        #             step_eroded_count += 1
        # eroded_count += step_eroded_count
        print("erode step :",step,",regeion size:",region_size,",erode count",step_eroded_count)
        eroded_count += step_eroded_count

    return dst_array,eroded_count

def erosion_with_connectivity(img):
    # 创建一个与输入图像大小相同的临时数组
    temp_img = np.zeros_like(img)
    
    # 执行腐蚀操作
    eroded_img = morphology.erosion(img)
    
    # 找到腐蚀后图像中标签为1的区域
    labeled_img, num_labels = morphology.label(eroded_img, return_num=True)
    
    # 遍历所有标签，确保腐蚀后的区域仍然连通
    for label in range(1, num_labels + 1):
        label_mask = labeled_img == label
        
        # 检查当前标签区域是否连通
        if not morphology.is_local_maximum(label_mask):
            # 如果不连通，则将该区域的像素置为0
            eroded_img[label_mask] = 0
        
        # 找到当前标签区域的中心线
        centerline = morphology.medial_axis(label_mask)
        temp_img[centerline] = 1
    
    return temp_img


def GetCenterlineOfVesselVolumeonErosion(src_volume:VolumeImage):
    vessel_data = src_volume.image_data_array
    for i in range(2):
        vessel_data = erosion_filter(vessel_data)
    src_volume.image_data_array = vessel_data
    src_volume.fresh_itk_image_from_image_data_array()
    # src_volume.saveImageAsNIFTI(dst_volume)



def projecting_points(points_cloud,plane_equation):

    # 定义平面方程
    A, B, C, D = plane_equation


    # 计算平面法向量的模长
    L = np.sqrt(A ** 2 + B ** 2 + C ** 2)

    # 计算平面位置向量
    p = np.array([-D * A / L ** 2, -D * B / L ** 2, -D * C / L ** 2])

    vector = np.array([A, B, C])

    # 计算每个点在平面上的投影坐标

    distance = (np.dot(points_cloud, vector) + D) / L
    project_points = points_cloud - distance[:,np.newaxis] * vector

    return  project_points
    # 输出投影后的点云坐标xs

def plot_plane_and_points(points, plane):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    # 绘制点云
    ax.scatter(points[:, 0], points[:, 1], points[:, 2], s=5,c="red")
    ax.scatter(points[-1, 0], points[-1, 1], points[-1, 2], s=25,c="blue")

    # 生成网格点坐标
    x, y = np.meshgrid(np.linspace(np.min(points[:, 0]), np.max(points[:, 0]), 10),
                       np.linspace(np.min(points[:, 1]), np.max(points[:, 1]), 10))
    z = (-plane[0] * x - plane[1] * y - plane[3]) / plane[2]

    # 绘制平面
    ax.plot_surface(x, y, z, alpha=0.5)

    # 设置坐标轴标签
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')

    # 显示图形
    plt.show()
    pass

def inter_line(p1,p2,inter = 10):
    points = []
    vector_euler =  p2 - p1
    vector_polar = rectangular2polar(vector_euler)
    for i in range(inter):
        polar = [vector_polar[0] / inter * i,vector_polar[1],vector_polar[2]]
        point = GetNewPosition_PolarDirection(p1,polar)
        points.append(point)
    return points

def plot_diameter_records(max_diameter_data,min_diameter_data):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    max_diameter_data = np.array(max_diameter_data)
    min_diameter_data = np.array(min_diameter_data)
    ax.scatter(max_diameter_data[:, 0], max_diameter_data[:, 1], c='red', label='max diameter')
    ax.scatter(min_diameter_data[:, 0], min_diameter_data[:, 1], c='blue', label='min diameter')
    ax.legend()
    plt.show()

def plot_line_points(line_points):
    point_num = line_points.shape[0]
    tck, u = splprep(np.unique(line_points,axis=0).T, k=2, s=0.001)
    u_new = np.linspace(u.min(), u.max(), 10 * point_num)
    x_new, y_new, z_new = splev(u_new, tck)

    # 可视化结果
    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    ax.plot(x_new, y_new, z_new, 'b-', label='smooth curve')
    ax.scatter(line_points[:, 0], line_points[:, 1], line_points[:, 2], c='r', label='original points')
    ax.legend()
    plt.show()

def plot_2_points_3d(points_1,points_2):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    points_1 = np.array(points_1)
    points_2 = np.array(points_2)
    # 绘制点云
    ax.scatter(points_1[:, 0], points_1[:, 1], points_1[:, 2], s=5, c="red")
    ax.scatter(points_1[-1, 0], points_1[-1, 1], points_1[-1, 2], s=25, c="blue")
    ax.scatter(points_2[:, 0], points_2[:, 1], points_2[:, 2], s=5, c="blue")
    ax.scatter(points_2[-1, 0], points_2[-1, 1], points_2[-1, 2], s=25, c="red")

    # 生成网格点坐标
    x, y = np.meshgrid(np.linspace(np.min(points_1[:, 0]), np.max(points_1[:, 0]), 10),
                       np.linspace(np.min(points_1[:, 1]), np.max(points_1[:, 1]), 10))


    # 设置坐标轴标签
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')

    # 显示图形
    plt.show()
    pass

def save_line_points_to_slicer_landmarks_file(lm_file,points):
    temp_file = os.path.join(os.getcwd(),"L-Template.mrk.json")
    if not DEBUG_MODE:
        return
    with open(temp_file,"rb") as f:
        lm_data = json.load(f)
        contorl_points_data = []
        measurements_data = []
        distance = np.linalg.norm(points[0] - points[1])
        for index,point in enumerate(points):
            id = str(index)
            label = "L-" + str(index)
            item = {
            "id": id,
            "label": label,
            "description": "",
            "associatedNodeID": "vtkMRMLScalarVolumeNode4",
            "position": list(point),
            "orientation": [-1.0, -0.0, -0.0, -0.0, -1.0, -0.0, 0.0, 0.0, 1.0],
            "selected": True,
            "locked": False,
            "visibility": True,
            "positionStatus": "defined"
        }
            contorl_points_data.append(item)

        measurements_data.append({
            "name": "length",
            "enabled": True,
            "value": distance,
            "units": "mm",
            "printFormat": "%-#4.4gmm"
        })
    with open(lm_file,"w") as f:
        lm_data["markups"][0]["controlPoints"] = contorl_points_data
        lm_data["markups"][0]["measurements"] = measurements_data
        json.dump(lm_data,f)

def save_points_to_slicer_landmarks_file(lm_file,points):
    temp_file = os.path.join(os.getcwd(),"F-Template.mrk.json")
    if not DEBUG_MODE:
        return
    print(DEBUG_MODE)
    with open(temp_file,"rb") as f:
        lm_data = json.load(f)
        contorl_points_data = []
        for index,point in enumerate(points):
            id = str(index)
            label = "F-" + str(index)
            item = {
            "id": id,
            "label": label,
            "description": "",
            "associatedNodeID": "vtkMRMLScalarVolumeNode4",
            "position": list(point),
            "orientation": [-1.0, -0.0, -0.0, -0.0, -1.0, -0.0, 0.0, 0.0, 1.0],
            "selected": True,
            "locked": False,
            "visibility": True,
            "positionStatus": "defined"
        }
            contorl_points_data.append(item)

    with open(lm_file,"w") as f:
        lm_data["markups"][0]["controlPoints"] = contorl_points_data
        json.dump(lm_data,f)


def save_diameter_records_to_csv(data,csv_file):
    # if not DEBUG_MODE:
    #     return
    df = pd.DataFrame(data, columns=['distance', 'max_d', 'min_d','x','y','z'])
    df.to_csv(csv_file, index=True)

def save_5mm_diameter_records_csv(data,csv_file):
    # if not DEBUG_MODE:
    #     return
    df = pd.DataFrame(data, columns=['distance', 'diameter','x', 'y',"z",'x', 'y',"z",'x', 'y',"z"])
    df.to_csv(csv_file, index=True)

def save_neck_records_to_csv(data,csv_file):
    df = pd.DataFrame(data, columns=['case', 'max_d', 'min_d'])
    df.to_csv(csv_file, index=False)

def line_fitting_3d(pionts):
    mean_point = np.average(pionts, axis=0)
    delta = pionts - mean_point
    _, _, vv = np.linalg.svd(delta)
    direction_euler = vv[0]/np.linalg.norm(vv[0])
    return direction_euler,mean_point

def save_geometry_data_to_csv(data_dict,csv_file):
    KEYS = [
        "SIZE",
        "WIDTH",
        "WH",
        "HEIGHT",
        "SURFACE_AREA",
        "VOLUME",
        "NECK_MIN_DIAMETER",
        "NECK_MAX_DIAMETER",
        "VESSEL_MIN_DIAMETER",
        "VESSEL_MAX_DIAMETER",
        "VESSEL_MAX_DIAMETER_CENTERLINE_POINT",
        "VESSEL_MAX_DIAMETER_POINT_A",
        "VESSEL_MAX_DIAMETER_POINT_B",
        "VESSEL_MIN_DIAMETER_CENTERLINE_POINT",
        "VESSEL_MIN_DIAMETER_POINT_A",
        "VESSEL_MIN_DIAMETER_POINT_B"
    ]
    # if not DEBUG_MODE:
    #     return
    data = []
    for key in KEYS:
        value = data_dict[key]
        data.append([key,value])
    df = pd.DataFrame(data, columns=['key', 'value'])
    df.to_csv(csv_file, index=True)


if __name__ == '__main__':
    src_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa.nii.gz"
    mask_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_mask_pipeline.nii.gz"
    dst_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_dst_fast.nii.gz"
    distance_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_distance_pipline.nii.gz"
    temp_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_distance_temp.nii.gz"

    src_volume = VolumeImage()
    src_volume.get_image_from_nifti(src_file)
    dst_volume = ConstantVolumeImage(src_volume,0)
    distance_volume = ConstantVolumeImage(src_volume,0)
    mask_volume = ConstantVolumeImage(src_volume,0)
    temp_volume = ConstantVolumeImage(src_volume,0)

    root_vessel_point = Vessel_Point()
    root_vessel_point.position_ijk = np.array([357,160,186],dtype=float)
    root_vessel_point.position_geometry = src_volume.convert_ijk_to_geometry(root_vessel_point.position_ijk)
    # end -> root
    candidate_vessel_points = []
    #dynamic volume
    MAX_SEARCH_POINT = math.pi * (3**2) * 50 / (src_volume.spacing[0] *src_volume.spacing[1] *src_volume.spacing[2])
    FAST_FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_DISTANCE(src_volume, mask_volume, dst_volume, root_vessel_point, 10000, 1000)
    dst_volume.saveImageAsNIFTI(dst_file)


    # for i in range(len(candidate_vessel_points)):
    #     for j in range(len(candidate_vessel_points[i])):
    #         x,y,z = candidate_vessel_points[i][j].position_ijk
    #         temp_volume.set_value_in_ijk([x,y,z], VESSEL_CENTERLINE_LABEL_TEMP + i)
    #
    # temp_volume.saveImageAsNIFTI(temp_file)
    # exit(0)


    # distance_volume.get_image_from_nifti(distance_file)
    # for vessel_index, rough_centerline_points in enumerate(candidate_vessel_points):
    #     rough_centerline_points_array  = [np.array(point.position_ijk) for point in rough_centerline_points]
    #     sparse_points =  get_refine_sparse_centerline_points_of_single_vessel_section(dst_volume, distance_volume, rough_centerline_points_array, 10)
    #     for point_index in range(len(sparse_points) ):
    #         x, y, z = sparse_points[point_index]
    #         dst_volume.set_value_in_ijk([x,y,z], VESSEL_CENTERLINE_LABEL + vessel_index)
    # dst_volume.saveImageAsNIFTI(dst_file)
    # exit(0)



    distance_volume.get_image_from_nifti(distance_file)
    for vessel_index, rough_centerline_points in enumerate(candidate_vessel_points):
        rough_centerline_points_array  = [np.array(point.position_ijk) for point in rough_centerline_points]
        sparse_points =  get_refine_sparse_centerline_points_of_single_vessel_section(dst_volume, distance_volume, rough_centerline_points_array, 10)
        # sparse_points.insert(0,rough_centerline_points[0].position_ijk)
        # sparse_points.insert(-1, rough_centerline_points[-1].position_ijk)
        for point_index in range(len(sparse_points) - 1):
            point_pair = [sparse_points[point_index], sparse_points[point_index + 1]]
            resample_points = resample_centerline_between_two_points(distance_volume,dst_volume,point_pair)
            for point in resample_points:
                # print(point)
                x, y, z = point
                dst_volume.set_value_in_ijk([x,y,z], VESSEL_CENTERLINE_LABEL + vessel_index)
    dst_volume.saveImageAsNIFTI(dst_file)
    exit(0)


    src_volume = VolumeImage()
    dst_volume = VolumeImage()
    distance_volume = VolumeImage()
    src_volume.get_image_from_nifti(src_file)

    dst_volume.get_image_from_nifti(dst_file)
    distance_volume.get_image_from_nifti(distance_file)
    i_points = np.load("D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\\section_points_array.npy")
    sparse_points =  find_best_centerline_of_vessel_section_on_distance_map(distance_volume, i_points)
    for point_index in range(len(sparse_points) - 2):
        point_pair = [sparse_points[point_index], sparse_points[point_index + 1]]
        resample_points = resample_centerline_between_two_points(distance_volume, point_pair)
        for point in resample_points:
            # print(point)
            x, y, z = point
            dst_volume.set_value_in_ijk(np.array([x, y, z]), 60)
    dst_volume.saveImageAsNIFTI(dst_file)

    exit(0)
    #

    src_volume = VolumeImage()
    src_volume.get_image_from_nifti(src_file)
    dst_volume = ConstantVolumeImage(src_volume,0)
    distance_volume = ConstantVolumeImage(src_volume,0)
    mask_volume = ConstantVolumeImage(src_volume,0)
    root_vessel_point = Vessel_Point()
    root_vessel_point.position_ijk = np.array([369,255,67],dtype=float)
    root_vessel_point.position_geometry = src_volume.convert_ijk_to_geometry(root_vessel_point.position_ijk)
    candidate_vessel_points = []
    FAST_FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_POINT(src_volume,mask_volume,dst_volume,root_vessel_point,10000,1000,candidate_vessel_points)
    dst_volume.saveImageAsNIFTI(dst_file)
    FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_POINT(src_volume,mask_volume,dst_volume,root_vessel_point,10000,1000,candidate_vessel_points)
    dst_volume.saveImageAsNIFTI(dst_file)


    distance_data_array = distance_filter(dst_volume.image_data_array)
    distance_data_array = scipy.ndimage.gaussian_filter(distance_data_array,sigma=1.3,radius=3)
    distance_volume.set_image_data_array(distance_data_array)
    distance_volume.saveImageAsNIFTI(distance_file)
    file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\\section_points_array.csv"
    np.save("D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\\section_points_array.csv",i_points_array)
    for point_index, vessel_section in enumerate(candidate_vessel_points):
        i_points_array = [np.array(point.position_ijk) for point in vessel_section]
        i_centerline_points = find_best_centerline_of_vessel_section_on_distance_map(src_volume, distance_volume, i_points_array)
        for point in i_centerline_points:
            print(point)
            x,y,z = point
            dst_volume.set_value_in_ijk(np.array([x,y,z]), 60 + point_index)
    dst_volume.saveImageAsNIFTI(dst_file)
    exit(0)


    src_volume.get_image_from_nifti("D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_dst_50k.nii.gz")
    distance_volume = VolumeImage()
    distance_volume.get_image_from_nifti("D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_dst_gaussian.nii.gz")

    mask_volume = ConstantVolumeImage(src_volume,0)

    # n_x,n_y,n_z = np.where(src_volume.image_data_array == 32)
    # points = []
    # for index in range(len(n_x)):
    #     points.append(np.array([n_x[index],n_y[index],n_z[index]]))

    rough_centerline_points_array = np.array([
        [368,256,66],
        [353,264,46],

    ])
    dst_data_array = find_best_centerline_of_vessel_section_on_distance_map(src_volume, distance_volume, distance_volume, rough_centerline_points_array)
    # dst_data_array = scipy.ndimage.gaussian_laplace(dst_data_array,sigma=1.3,radius=3) * (-1)
    # dst_data_array[dst_data_array < 30] = 0
    # dst_data_array[dst_data_array >= 30] = 1
    src_volume.saveImageAsNIFTI("D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_dst_distance.nii.gz")
    exit(0)


    src_volume = VolumeImage()
    src_volume.get_image_from_nifti("D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_dst_50k.nii.gz")
    dst_volume = ConstantVolumeImage(src_volume,0)
    for i in range(0,1):
        dst_array,_ = StayConnectedErodeFilter(src_volume)
    dst_volume.set_image_data_array(dst_array)
    dst_volume.saveImageAsNIFTI("D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_dst_50k_connected_erode.nii.gz")

    exit(0)

    src_volume = VolumeImage()
    src_volume.get_image_from_nifti("D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_dst_50k.nii.gz")
    dst_data_array = distance_filter(src_volume.image_data_array)
    dst_data_array = scipy.ndimage.gaussian_filter(dst_data_array,sigma=1.3,radius=3)
    dst_data_array = scipy.ndimage.gaussian_laplace(dst_data_array,sigma=1.3,radius=3) * (-1)
    dst_data_array[dst_data_array < 30] = 0
    dst_data_array[dst_data_array >= 30] = 1
    src_volume.set_image_data_array(dst_data_array )
    src_volume.saveImageAsNIFTI("D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_dst_log.nii.gz")


    exit(0)

    src_volume = VolumeImage()
    src_volume.get_image_from_nifti(src_file)
    dst_volume = ConstantVolumeImage(src_volume,0)
    mask_volume = ConstantVolumeImage(src_volume,0)
    root_vessel_point = Vessel_Point()
    root_vessel_point.position_ijk = np.array([369,255,67],dtype=float)
    root_vessel_point.position_geometry = src_volume.convert_ijk_to_geometry(root_vessel_point.position_ijk)
    candidate_vessel_points = []
    FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_POINT(src_volume,mask_volume,dst_volume,root_vessel_point,10000,1000,candidate_vessel_points)

    exit(0)
    # chkpt
    src_volume = VolumeImage()
    src_volume.get_image_from_nifti(dst_file)
    dst_volume = ConstantVolumeImage(src_volume,0)


    exit(0)

