import math

import numpy as np
from CommonParameters import *
import scipy
from utils import *
import time
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.spatial import ConvexHull
from coordinates_system import *

def Get_Angeioma_Neck_Array_From_VesselImageArray_And_AngeiomaImageArray(angeioma_image_array:np.array,rough_vessel_image_array:np.array):
    time_start= time.time()
    # dialate angeioma
    dialate_angeioma_array = np.copy(angeioma_image_array)
    dialate_angeioma_array[np.where(angeioma_image_array != ANGEIOMA_LABEL)] = 0
    dialate_angeioma_array[scipy.ndimage.binary_dilation(dialate_angeioma_array,structure=np.ones((3,3,3)))] = 1


    #cutoff dialte angeioma from rough_vessel_image_array
    neat_vessel_array = np.copy(rough_vessel_image_array)
    neat_vessel_array[np.where(dialate_angeioma_array== ANGEIOMA_LABEL)] = 0
    # neat_vessel_array = neat_vessel_array - dialate_angeioma_array

    #get neck array
    dialate_vessel_array = np.copy(neat_vessel_array)
    dialate_vessel_array[np.where(dialate_vessel_array != ANGEIOMA_LABEL)] = 0
    dialate_vessel_array[scipy.ndimage.binary_dilation(dialate_vessel_array,structure=np.ones((3,3,3)))] = 1
    neck_array = np.copy(dialate_angeioma_array)
    neck_array[np.where(dialate_vessel_array==0)] = 0



    print(f"neck time cost: +{(time.time() - time_start):.4f}")

    return neck_array
def  Get_Angeioma_Neck_Array_From_VesselImageVolume_And_AngeiomaImageVolue_V2(angeioma_image:VolumeImage,rough_vessel_image:VolumeImage):
    time_start = time.time()
    angeioma_image_array = angeioma_image.image_data_array
    neck_array = np.zeros_like(angeioma_image_array)
    neck_array_in_vessel = np.zeros_like(angeioma_image_array)
    neck_array_in_angeioma = np.zeros_like(angeioma_image_array)
    angeioma_points = np.nonzero(angeioma_image_array)
    angeioma_points = np.array([angeioma_points[0],angeioma_points[1],angeioma_points[2]]).swapaxes(1,0)
    REGION = get_region_3d(1)
    for point in angeioma_points:
        region_points = point + REGION
        for region_point in region_points:
            vessel_value = rough_vessel_image.get_value_in_ijk(region_point)
                # rough_vessel_image_array[region_point[0],region_point[1],region_point[2]]
            if vessel_value == 1:
                neck_array_in_vessel[region_point[0],region_point[1],region_point[2]] = 1
                neck_array_in_angeioma[point[0],point[1],point[2]] = 1
                neck_array[region_point[0],region_point[1],region_point[2]] = 1
                neck_array[point[0],point[1],point[2]] = 1
    test_volume1 = ConstantVolumeImage(angeioma_image,0)
    test_volume2 = ConstantVolumeImage(angeioma_image,0)
    test_volume1.set_image_data_array(neck_array_in_vessel)
    test_volume2.set_image_data_array(neck_array_in_angeioma)
    test_volume1.saveImageAsNIFTI("D:\Coding\\0_data\\benchmark_cad\\1\\test1.nii.gz")
    test_volume2.saveImageAsNIFTI("D:\Coding\\0_data\\benchmark_cad\\1\\test2.nii.gz")
    return neck_array

def get_angeoma_neck_nomial_points_geometry(neck_image:VolumeImage):
    index = np.where(neck_image.image_data_array!=0)
    points_ijk_array = np.array([index[0],index[1],index[2]]).swapaxes(0,1)
    points_geometry_array = np.array([neck_image.convert_ijk_to_geometry(point) for point in points_ijk_array])
    plane_equation, normal, S2_center_point = surface_fitting(points_geometry_array)
    # print("center point of S2:")
    # print(S2_center_point)
    #project
    S3_points = projecting_points(points_geometry_array,plane_equation)
    S3_center_point = np.mean(S3_points,axis=0)
    # print("center point of S3:")
    # print(S3_center_point)

    temp_S3_vessel_points = []
    temp_S3_vessel_points_ijk_records = []
    for point in list(S3_points):
        i_point = Vessel_Point()
        i_point.position_ijk = neck_image.convert_geometry_to_ijk(point).astype("int")
        i_point.position_geometry = neck_image.convert_ijk_to_geometry(i_point.position_ijk)
        if list(i_point.position_ijk) not in temp_S3_vessel_points_ijk_records:
            temp_S3_vessel_points.append(i_point)
            temp_S3_vessel_points_ijk_records.append(list(i_point.position_ijk))
    surfaces = separate_vessel_surface_points(temp_S3_vessel_points)
    surface_size = [len(surface) for surface in surfaces]
    S4_points = surfaces[surface_size.index(max(surface_size))]
    S4_points = np.array([np.array(point.position_geometry) for point in S4_points])
    S4_center_point = np.mean(S4_points,axis=0)
    # plot_plane_and_points(S4_points,plane_equation)
    save_points_to_slicer_landmarks_file("D:\Coding\\0_data\\benchmark_cad\\1\output\\s4.mrk.json",S4_points)
    save_points_to_slicer_landmarks_file("D:\Coding\\0_data\\benchmark_cad\\1\output\\s3.mrk.json",S3_points)
    return S3_points,S3_center_point,plane_equation,normal
    return S4_points,S4_center_point,plane_equation,normal


def Get_Angeioma_Neck_Geometry(neck_image:VolumeImage):
    S3_points, S3_center_point, plane_equation, normal = get_angeoma_neck_nomial_points_geometry(neck_image)
    max_diameter_points,max_diameter,min_diameter_points,min_diameter = get_diamter_of_points_contour_new(S3_points,S3_center_point,normal,plane_equation)

    return max_diameter_points,max_diameter,min_diameter_points,min_diameter,[S3_points,S3_center_point,plane_equation,normal]


def get_diamter_of_points_contour(points_cloud,center_point,normal,plane_equation = None):
    normal_polar = rectangular2polar(normal)
    rotation_matrix = GetRotationMatrix(normal_polar)
    points_2d = np.array([np.dot(rotation_matrix,point - center_point) for point in points_cloud])[:,:-1]
    hull = ConvexHull(points_2d)
    contour_3d = np.array([points_cloud[index] for index in hull.vertices])
    contour_3d = np.append(contour_3d, contour_3d[0,:][np.newaxis, :], axis=0)

    # plot_plane_and_points(np.append(contour_3d, center_point[np.newaxis, :], axis=0), plane_equation)
    lines = []
    seeds = []
    cross_ponits = []
    plot_points = []
    for index in range(contour_3d.shape[0] - 1):
        p1 = contour_3d[index]
        p2 = contour_3d[index+1]
        if index == 7:
            lines.append(np.array([p1,p2]))
        # plot_points.append(p1)
    for i in range(1):
        i_distance_records = []
        i_cross_point_records = []
        diameter_vector_polar_relative = [20,math.pi/2,i/180*math.pi]
        diameter_vector_euler_relative = polar2rectangular(diameter_vector_polar_relative)
        diameter_vector_euler = np.dot(np.linalg.inv(rotation_matrix), diameter_vector_euler_relative)
        diameter_vector_polar = rectangular2polar(diameter_vector_euler)
        for point in [GetNewPosition_PolarDirection(center_point,[i,diameter_vector_polar[1],diameter_vector_polar[2]]) for i in range(20)]:
            cross_ponits.append(np.array(point))
            plot_points.append(np.array(point))
        diameter_line = [center_point,GetNewPosition_PolarDirection(center_point,diameter_vector_polar)]
        for index,line in enumerate(lines):
            i_distance = get_distance_of_line_to_line(line,diameter_line)
            i_distance_records.append(i_distance)
            line_points = inter_line(line[0],line[1])
            for point in line_points:
                plot_points.append(point)
        # cross_index = np.argmin(i_distance_records)
        # cross_contour_line = lines[cross_index]
        # line_points = np.array(inter_line(cross_contour_line[0],cross_contour_line[1]))
        # # for point in line_points:
        # #     plot_points.append(point)
        plot_points = np.array(plot_points)
        # plot_points = np.append(plot_points,line_points,axis=0)
        # cross_point,_ = closest_point_line_to_line(cross_contour_line,diameter_line)
        # cross_point = i_cross_point_records[cross_index]
        # cross_vector_euler = cross_point - center_point
        # cross_vector_polar = rectangular2polar(cross_vector_euler)
        # for point in [GetNewPosition_PolarDirection(center_point,[i,cross_vector_polar[1],cross_vector_polar[2]]) for i in range(10)]:
        #     cross_ponits.append(np.array(point))

        # cross_ponits.append(cross_point)
        # i_radius = np.linalg.norm(cross_point - center_point)
        # print(i_radius,cross_point)
        plot_plane_and_points(np.append(plot_points,center_point[np.newaxis,:],axis=0),plane_equation)
    # cross_points = np.array(cross_ponits)
    pass

    # angle_records = []
    # for point in contour_3d:
    #     vector_euler = point - center_point
    #     vector_euler_relative = np.dot(rotation_matrix, vector_euler)
    #     vector_polar_relative = rectangular2polar(vector_euler_relative)
    #     angle = (vector_polar_relative[-1]) / math.pi * 180
    #     i_distance = np.linalg.norm(vector_euler)
    #     angle_records.append(angle)
    # # find roi angles
    # angle_records = np.array(angle_records)
    # for angle in range(1,180):
    #     error = angle_records - angle
    #     nearest_pos_angle = np.min(angle_records[np.where(error >= 0)])
    #     nearest_pos_angle_index = np.where(angle_records == nearest_pos_angle)
    #     nearest_neg_angle = np.max(angle_records[np.where(error <= 0)])
    #     nearest_neg_angle_index = np.where(angle_records == nearest_neg_angle)



def get_diamter_of_points_contour_new(points_cloud,center_point,normal,plane_equation = None):
    normal_polar = rectangular2polar(normal)
    rotation_matrix = GetRotationMatrix(normal_polar)
    points_2d = np.array([np.dot(rotation_matrix,point - center_point) for point in points_cloud])[:,:-1]
    hull = ConvexHull(points_2d)
    contour_3d = np.array([points_cloud[index] for index in hull.vertices])

    angle_records = []
    cross_points = []
    radius_records = []
    diameter_records = []
    plot_points = []
    for point in contour_3d:
        vector_euler = point - center_point
        vector_euler_relative = np.dot(rotation_matrix, vector_euler)
        vector_polar_relative = rectangular2polar(vector_euler_relative)
        angle = (vector_polar_relative[-1])/math.pi * 180
        angle_records.append(angle)
        plot_points.append(point)
        # angle = get_angle_between_two_vecotr_euler(benchmark_vector_euler,vector_euler)
    angle_records = np.array(angle_records)
    for diameter_angle in range(360):
        error = angle_records - diameter_angle
        # nearst positive anlge
        if np.array(np.where(error >= 0)).shape[-1] >0:
            nearest_pos_angle = np.min(angle_records[np.where(error >= 0)])
            nearest_pos_angle_index = np.where(angle_records == nearest_pos_angle)[0][0]
        else:
            nearest_pos_angle = np.min(angle_records)
            nearest_pos_angle_index = np.argmin(angle_records)
        if np.array(np.where(error <= 0)).shape[-1] >0:
            nearest_neg_angle = np.max(angle_records[np.where(error <= 0)])
            nearest_neg_angle_index = np.where(angle_records == nearest_neg_angle)[0][0]
        else:
            nearest_neg_angle = np.max(angle_records)
            nearest_neg_angle_index = np.argmax(angle_records)
        diamter_contour_line = [contour_3d[nearest_neg_angle_index],contour_3d[nearest_pos_angle_index]]
        diameter_vector_polar_relative = [5,math.pi/2,diameter_angle/180*math.pi]
        diameter_vector_euler_relative = polar2rectangular(diameter_vector_polar_relative)
        diameter_vector_euler = np.dot(np.linalg.inv(rotation_matrix), diameter_vector_euler_relative)
        diameter_vector_polar = rectangular2polar(diameter_vector_euler)
        diameter_P2 = GetNewPosition_PolarDirection(center_point,diameter_vector_polar)
        diameter_line = [center_point,diameter_P2]
        cross_point = find_cross_point_of_two_lines_2d(diamter_contour_line,diameter_line,center_point, rotation_matrix)
        i_radius = np.linalg.norm(cross_point - center_point)
        cross_points.append(cross_point)
        radius_records.append(i_radius)
        # print(diameter_angle,i_radius,cross_point)
        # plot_points.append(cross_point)
    # plot_plane_and_points(np.append(plot_points,center_point[np.newaxis,:],axis=0),plane_equation)
    #convert radius to diameters
    save_points_to_slicer_landmarks_file("D:\Coding\\0_data\\benchmark_cad\\1\output\\s5.mrk.json",plot_points)
    for i in range(180):
        i_diameter = radius_records[i] + radius_records[i + 180]
        diameter_records.append(i_diameter)
    max_diameter = np.max(np.array(diameter_records))
    max_diameter_index = np.argmax(np.array(diameter_records))
    max_diameter_points = [cross_points[max_diameter_index],cross_points[max_diameter_index + 180]]
    min_diameter = np.min(np.array(diameter_records))
    min_diameter_index = np.argmin(np.array(diameter_records))
    min_diameter_points = [cross_points[min_diameter_index],cross_points[min_diameter_index + 180]]

    max_line_points = np.array(inter_line(max_diameter_points[0],max_diameter_points[1]))
    for point in max_line_points:
        plot_points.append(point)
    min_line_points = np.array(inter_line(min_diameter_points[0],min_diameter_points[1]))
    for point in min_line_points:
        plot_points.append(point)
    return max_diameter_points,max_diameter,min_diameter_points,min_diameter


def shrink_neck_diamter(points,offset):
    p1 = points[0]
    p2 = points[1]
    L = np.linalg.norm(p1 - p2)

    direction_euler,mean_point = line_fitting_3d(points)
    p1_n = GetNewPosition_EulerDirection(mean_point,direction_euler * (L/2 + offset)  )
    p2_n = GetNewPosition_EulerDirection(mean_point, - (direction_euler * (L/2 + offset)  ))
    return [p1_n,p2_n]





def if_line_contain_point(line,P3):
    P1,P2 = line
    distance_p3p1 = np.linalg.norm(P3 - P1)
    distance_p2p1 = np.linalg.norm(P2 - P1)
    if distance_p3p1 > distance_p2p1:
        return False
    else:
        return True


def get_distance_of_line_to_line(line1,line2):
    # line1: contour
    # line2: diameter
    P1,P2 = line1
    P3,P4 = line2
    L1_euler = P2 - P1
    L1_polar = rectangular2polar(L1_euler)
    L1  = L1_polar[0]
    distance_records = []
    for i in range(100):
        r = i * L1 /100
        point = GetNewPosition_PolarDirection(P1,[r,L1_polar[1],L1_polar[2]])
        i_distance = get_distance_of_point_to_line(line2,point)
        distance_records.append(i_distance)
    return min(distance_records)

def get_distance_of_point_to_line(line,P3):
    P1,P2 = line

    direction_vector = P2 - P1
    # 计算点到直线的向量
    point_vector = P3 - P1

    # 计算向量在直线方向上的投影
    projection_length = np.dot(point_vector, direction_vector) / np.linalg.norm(direction_vector)

    # 计算点到直线的距离
    distance = np.linalg.norm(point_vector - projection_length * direction_vector / np.linalg.norm(direction_vector))
    Q = P1 + np.dot(point_vector, direction_vector) / np.dot(direction_vector, direction_vector) * direction_vector
    return distance,Q

def find_cross_point_of_two_lines_2d(line1,line2,center_point,rotation_matrix):
    def get_line_euqation(p1,p2):
        x1, y1 = p1
        x2, y2 = p2

        # 求解斜率
        if x2 == x1:
            # 处理 x2 = x1 的情况
            return -1,0,x1
        else:
            k = (y2 - y1) / (x2 - x1)

        # 将点斜式公式转化为截距式
        b = y1 - k * x1

        # 将截距式转化为 Ax + By + C = 0 的形式
        A = k
        B = -1
        C = b
        return A,B,C
    # convert 3d -> 2d
    P1_3d,P2_3d = line1
    P3_3d,P4_3d = line2

    line1_2d = np.array([np.dot(rotation_matrix,point - center_point) for point in line1])[:,:-1]
    line2_2d = np.array([np.dot(rotation_matrix,point - center_point) for point in line2])[:,:-1]





    P1,P2 = line1_2d
    P3,P4 = line2_2d

    A1,B1,C1 = get_line_euqation(P1,P2)
    A2,B2,C2 = get_line_euqation(P3,P4)
    x = (B1 * C2 - B2 * C1) / (A1 * B2 - A2 * B1)
    y = (C1 * A2 - C2 * A1) / (A1 * B2 - A2 * B1)

    a1= (P2 - P1)[-1]
    b1 = (P2 - P1)[0]
    c1 =  P1[0]*P2[1] - P1[1]*P2[0]

    a2= (P4 - P3)[-1]
    b2 = (P4 - P3)[0]
    c2 =  P3[0]*P4[1] - P3[1]*P4[0]


    cross_point = [
        (b1*c2 - b2*c1) / (a1 *b2 -a2*b1),
        (c1*a2 - c2*a1) / (a1 *b2 -a2*b1)
    ]
    cross_point = [x,y]
    v1 = P2 - P1
    v2 = P4 - P3

    n = np.cross(v1, v2)
    D = np.dot(n, P3 - P1) / np.linalg.norm(n)

    # # 计算焦点
    # if np.linalg.norm(n) == 0:
    #     # 两条直线平行，没有交点
    #     Q1 = None
    #     Q2 = None
    # else:
    #     Q1 = P1 + (np.dot((P3 - P1), n) / np.dot(v1, n)) * v1
    #     # print(Q1)
    # Q1_3d = np.dot(np.linalg.inv(rotation_matrix), np.array([Q1[0],Q1[1],0])) + center_point
    Q1_3d = np.dot(np.linalg.inv(rotation_matrix), np.array([cross_point[0],cross_point[1],0])) + center_point
    return Q1_3d

def point_in_line(line,p3):
    p1,p2 = line
    v1 = p3 - p1
    v2 = p2 - p1
    if np.cross(v1,v2) == 0:
        return True
    else:
        return False
def closest_point_line_to_line(line1, line2):
    """
    计算两条直线之间最近的点
    """
    p1, p2 = line1
    p3, p4 = line2
    v1 = p2 - p1
    v2 = p4 - p3
    v3 = p3 - p1
    normal = np.cross(v1, v2)
    if np.linalg.norm(normal) != 0:
        t1 = np.cross(v2, v3) / np.dot(normal, normal)
        t2 = np.cross(v1, v3) / np.dot(normal, normal)
        closest_point1 = p1 + t1*v1
        closest_point2 = p3 + t2*v2
        return closest_point1, closest_point2
    else:
        return None, None


def distance_between_lines(p1, d1, p2, d2):
    # 计算两条直线的方向向量
    u = d1 / np.linalg.norm(d1)
    v = d2 / np.linalg.norm(d2)

    # 计算两条直线上任意一点的坐标差向量
    w = p1 - p2

    # 计算两条直线之间的距离
    a = np.dot(u, v)
    b = np.dot(u, w)
    c = np.dot(v, w)
    d = np.linalg.norm(np.cross(u, v))

    t1 = (b - c * a) / (1 - a ** 2)
    t2 = (c - a * b) / (1 - a ** 2)

    q1 = p1 + t1 * u
    q2 = p2 + t2 * v

    # 计算两点之间的距离
    distance = np.linalg.norm(q1 - q2)

    return distance, q1, q2


def get_diameter_of_points(points_cloud,center_point,normal,plane_equation = None):
    def get_index_in_angle_range(angles,angle_records):
        try:
            index = np.intersect1d(np.where(angle_records>=angles[0]),np.where(angle_records<angles[1]))
            mid_angle = np.mean(angles)
            error = angle_records - mid_angle
            nearest_pos_angle = np.min(angle_records[np.where(error >= 0)])
            nearest_pos_angle_index =np.where(angle_records == nearest_pos_angle)
            nearest_neg_angle = np.max(angle_records[np.where(error <= 0)])
            nearest_neg_angle_index =np.where(angle_records == nearest_neg_angle)
            index = np.append(index,[nearest_pos_angle_index,nearest_neg_angle_index])
        except:
            pass
        return np.unique(index)

    diameter_points_nomial = []
    diameter_points_real = []
    diameter_records = []
    #calculate distance and angle of every point
    benchmark_vector_euler = points_cloud[0] - center_point
    normal_polar = rectangular2polar(normal)

    rotation_matrix = GetRotationMatrix(normal_polar)
    distance_records = []
    pos_radius_records = []
    neg_radius_records = []
    angle_records = []
    for point in points_cloud:
        vector_euler = point - center_point
        vector_euler_relative = np.dot(rotation_matrix, vector_euler)
        vector_polar_relative = rectangular2polar(vector_euler_relative)
        angle = (vector_polar_relative[-1])/math.pi * 180
        # angle = get_angle_between_two_vecotr_euler(benchmark_vector_euler,vector_euler)
        i_distance = np.linalg.norm(vector_euler)
        angle_records.append(angle)
        distance_records.append(i_distance)
        # print(angle,i_distance)
    angle_records = np.array(angle_records)
    distance_records = np.array(distance_records)
    angle_roi = 3
    for angle in range(angle_roi,180 - angle_roi):
        pos_angles = np.array([angle-angle_roi,angle+angle_roi])
        neg_angles = pos_angles + 180
        pos_index = get_index_in_angle_range(pos_angles,angle_records)
        neg_index = get_index_in_angle_range(neg_angles,angle_records)
        pos_radius = distance_records[pos_index]
        neg_radius = distance_records[neg_index]
        i_diamter = np.max(pos_radius) + np.max(neg_radius)
        pos_radius_records.append(np.max(pos_radius))
        neg_radius_records.append(np.max(neg_radius))
        # get real diameter points
        diameter_point_pos_real = points_cloud[np.where(distance_records == np.max(pos_radius))][0]
        diameter_point_neg_real = points_cloud[np.where(distance_records == np.max(neg_radius))][0]


        #get nominal diameter points
        nominal_angle = np.mean(pos_angles)
        i_vector_polar_pos_relative = [np.max(pos_radius),math.pi/2,nominal_angle /180 * math.pi]
        i_vector_euler_pos = np.dot(np.linalg.inv(rotation_matrix), polar2rectangular(i_vector_polar_pos_relative))
        diameter_point_pos = GetNewPosition_EulerDirection(center_point,i_vector_euler_pos)

        i_vector_polar_neg_relative = [np.max(neg_radius),math.pi/2,(nominal_angle + 180) /180 * math.pi]
        i_vector_euler_neg = np.dot(np.linalg.inv(rotation_matrix), polar2rectangular(i_vector_polar_neg_relative))
        diameter_point_neg = GetNewPosition_EulerDirection(center_point,i_vector_euler_neg)



        diameter_points_nomial.append(diameter_point_pos)
        diameter_points_nomial.append(diameter_point_neg)
        diameter_points_real.append(diameter_point_pos_real)
        diameter_points_real.append(diameter_point_neg_real)
        diameter_records.append(i_diamter)
        # if np.max(pos_radius) == 0.4857554829773235:
        #     # plot_plane_and_points(np.append(points_cloud[pos_index],np.append(points_cloud[neg_index],center_point[np.newaxis,:],axis=0),axis=0), plane_equation)
        #     plot_plane_and_points(np.append(points_cloud[pos_index],center_point[np.newaxis,:],axis=0), plane_equation)
        #     pass
    return np.array(diameter_points_nomial),np.array(diameter_records)

def surface_fitting(plane_points:np.array):
    #[N,3]
    # 计算中心点坐标
    center = np.mean(plane_points, axis=0)

    # 将点云中心化
    centered_points = plane_points - center

    # 计算协方差矩阵
    cov = np.cov(centered_points, rowvar=False)

    # 奇异值分解，得到协方差矩阵的特征向量
    eigvals, eigvecs = np.linalg.eig(cov)

    # 提取法向量
    normal = eigvecs[:, np.argmin(eigvals)]

    # 将法向量归一化
    normal /= np.linalg.norm(normal)

    # 计算平面方程中的d值
    d = -np.dot(normal, center)
    return np.append(normal, d),normal,center



def my_projected_points(point,plane_equation):

    A, B, C, D = plane_equation
    x,y,z = point
    bottom = A * A + B * B + C * C
    p_x = ((B * B + C * C) * x - A * (B * y + C * z + D)) / bottom
    p_y = ((A * A + C * C) * y - B * (A * x + C * z + D)) / bottom
    p_z = ((A * A + B * B) * z - C * (A * x + B * y + D)) / bottom
    return np.array([p_x,p_y,p_z])


if __name__ == '__main__':

    src_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa.nii.gz"
    label_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\label.nii.gz"
    mask_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_mask_pipeline.nii.gz"
    dst_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_dst_fast.nii.gz"
    distance_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_distance_pipline.nii.gz"
    temp_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_distance_temp.nii.gz"
    vessel_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_vessel_pipeline.nii.gz"
    neck_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_neck_pipeline.nii.gz"



    rough_vessel_volume = VolumeImage()
    rough_vessel_volume.get_image_from_nifti(dst_file)

    angeioma_volume = VolumeImage()
    angeioma_volume.get_image_from_nifti(label_file)

    neck_volume = VolumeImage()
    neck_volume.get_image_from_nifti(neck_file)

    Get_Angeioma_Neck_Geometry(neck_volume)


    # plot_plane_and_points(points,plane_equation)
    # Neck_volume = ConstantVolumeImage(rough_vessel_volume, 0)
    # temp_array = Get_Angeioma_Neck_Array_From_VesselImageArray_And_AngeiomaImageArray(angeioma_volume.image_data_array,rough_vessel_volume.image_data_array)
    # Neck_volume.set_image_data_array(temp_array)
    # Neck_volume.saveImageAsNIFTI(neck_file)