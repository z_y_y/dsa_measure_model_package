import os.path
import numpy as np
import scipy.ndimage

from utils import *
from AngeiomaNeckExtraction import *
from skimage import morphology
from skeletonize import skeletonize

def calculate_vessel_volume(src_volume:VolumeImage, angeioma_gt_volume:VolumeImage):
    ####################### STEP 1 ###############################
    #region grow vessel tissue
    center_point_position_ijk,angeioma_volume = get_initial_parameters_of_angeioma(angeioma_gt_volume)
    nominal_angeioma_diameter = np.sqrt(angeioma_volume / math.pi)
    root_vessel_point = Vessel_Point()
    root_vessel_point.position_ijk = np.round(center_point_position_ijk).astype("int")
    root_vessel_point.position_geometry = src_volume.convert_ijk_to_geometry(root_vessel_point.position_ijk)

    growing_points,surface_points = FAST_FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_DISTANCE(src_volume, large_scale_vesel_volume, root_vessel_point, 10000, 1000, nominal_angeioma_diameter + 20)
    # growing_points,surface_points,self_index_record,parent_index_record,dst_data_array = FAST_ARRAY_FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_DISTANCE(src_volume.image_data_array,dst_volume.image_data_array,root_vessel_point.position_ijk,10000,1000, (nominal_angeioma_diameter + 20)/np.average(src_volume.spacing))
    large_scale_vesel_volume.saveImageAsNIFTI(largescale_vessel_file)


    ####################### STEP 1.5 ###############################
    # get only vessel volume
    temp_all_vessel_array = np.copy(large_scale_vesel_volume.image_data_array)
    temp_angeioma_array = np.copy(angeioma_gt_volume.image_data_array)
    temp_all_vessel_array[np.where(temp_angeioma_array==ANGEIOMA_LABEL)] = 0
    neat_vessel_volume.set_image_data_array(temp_all_vessel_array)
    neat_vessel_volume.saveImageAsNIFTI(neat_vessel_file)


def combine_rough_centerline_points(centerlines):
    pass


def analysize_vessel_centerline_points(root_point:Vessel_Point,vessel_volume:VolumeImage):
    #step 1 calculate centerline points' parent and child points.

    REGION = get_region_3d_27(1)
    vcGrowPt = [root_point]
    centerline_points = [root_point]

    src_data_array = copy.deepcopy(vessel_volume.image_data_array)
    while len(vcGrowPt) != 0:
        anchor_point = vcGrowPt[0]
        vcGrowPt.pop(0)
        anchor_x,anchor_y,anchor_z = anchor_point.position_ijk.astype(int)
        anchor_point_index= find_same_postion_ijk_in_points_list(anchor_point.position_ijk,centerline_points)[-1]
        anchor_point_src_value = src_data_array[anchor_x,anchor_y,anchor_z]
        # print("anchor :" ,anchor_point.position_ijk,"index :",anchor_point_index)
        for region in REGION:
            neighbor_point_ijk = anchor_point.position_ijk + region
            neighbor_point_value = vessel_volume.get_value_in_ijk(neighbor_point_ijk)
            if neighbor_point_value == 1:
                # print("neighbor :" ,neighbor_point_ijk)
                temp_vessel_point = Vessel_Point()
                temp_vessel_point.position_ijk = neighbor_point_ijk
                temp_vessel_point.position_geometry = vessel_volume.convert_ijk_to_geometry(neighbor_point_ijk)
                temp_vessel_point.parent_points_index.append(anchor_point_index)
                temp_vessel_point.distance_from_root_point = anchor_point.distance_from_root_point + np.linalg.norm(temp_vessel_point.position_geometry - anchor_point.position_geometry)
                index_in_results = find_same_postion_ijk_in_points_list(neighbor_point_ijk,centerline_points)
                if len(index_in_results) == 0:
                    # print("  neighbor not in results,append results and vcGrowPt")
                    centerline_points.append(temp_vessel_point)
                    vcGrowPt.append(temp_vessel_point)
                    temp_index = len(centerline_points) - 1
                    # print("  temp_index:",temp_index)
                    if temp_index not in anchor_point.child_points_index:
                        # print("    temp_index not in anchor_point.child_points_index，append anchor_point.child_points_index")
                        anchor_point = centerline_points[anchor_point_index]
                        anchor_point.child_points_index.append(temp_index)
                        centerline_points[anchor_point_index] = anchor_point

                else:
                    temp_index = index_in_results[-1]
                    # print("  neighbor in results,append results and vcGrowPt at index:",index_in_results)
                    older_point = centerline_points[temp_index]
                    if anchor_point_index not in older_point.parent_points_index\
                            and anchor_point_index not in older_point.child_points_index:
                        # print("    anchor_point_index not in results and not in older_point.child_points_index")
                        older_point.parent_points_index.append(anchor_point_index)
                        centerline_points[index_in_results[-1]] = older_point

    # step 2 calculate node and leaf points
    node_points_index_records = []
    for index,point in enumerate(centerline_points):
        if len(point.parent_points_index) == 0:
            centerline_points[index].node_type = 2
        elif len(point.child_points_index) == 0:
            centerline_points[index].node_type = 3
        elif len(point.parent_points_index) > 1 \
                or len(point.child_points_index) > 1:
            centerline_points[index].node_type = 1
        else:
            centerline_points[index].node_type = 0

        if centerline_points[index].node_type != 0:
            node_points_index_records.append(index)

    vessel_sections = []

    for node_point_index in node_points_index_records:
        i_vessel_sections = find_end_node_from_start_node(node_point_index,centerline_points)
        vessel_sections.extend(i_vessel_sections)

    for vessel_index,vessel_section in enumerate(vessel_sections):
        i_node_index_pair, i_leaf_points_index = vessel_section
        if len(i_leaf_points_index) > 1 and vessel_index < 254:
            for point_index in i_leaf_points_index:
                i_point = centerline_points[point_index].position_ijk
                i_point_value = vessel_volume.get_value_in_ijk(i_point)
                if i_point_value!=1:
                    print("duplicate")
            # vessel_volume.set_value_in_ijk(i_point, vessel_index+1)



    return vessel_sections,centerline_points





def find_end_node_from_start_node(start_node_index,src_points):
    start_node_point = src_points[start_node_index]
    results = []
    for child_point_index in start_node_point.child_points_index:
        i_node_index_pair = [start_node_index]
        i_leaf_points_index = []
        while src_points[child_point_index].node_type == 0:
            i_leaf_points_index.append(child_point_index)
            child_point_index = src_points[child_point_index].child_points_index[0]
        i_end_node_index = child_point_index
        i_node_index_pair.append(i_end_node_index)
        results.append([i_node_index_pair,i_leaf_points_index])
    return  results




def extract_vessel_centerline(src_volume:VolumeImage, angeioma_gt_volume:VolumeImage,max_value,min_value,output_dir):
    #TODO Temprory step for user angeioma volume contain vessel label
    temp_array = angeioma_gt_volume.image_data_array
    temp_array[np.where(temp_array!=ANGEIOMA_LABEL)] = 0
    angeioma_gt_volume.set_image_data_array(temp_array)

    ########################STEP 0#####################
    # init image volume
    neat_vessel_volume = ConstantVolumeImage(src_volume, 0)
    # neck_volume = ConstantVolumeImage(src_volume,0)
    skeleton_volume = ConstantVolumeImage(src_volume,0)



    ##################
    if max_value == -1 and min_value == -1:
        min_percentage = 99.9
        min_value = np.percentile(src_volume.image_data_array,min_percentage)
        max_value = src_volume.image_data_array.max()



    ####################### STEP 1 ###############################
    center_point_position_ijk,angeioma_volume = get_initial_parameters_of_angeioma(angeioma_gt_volume)
    nominal_angeioma_diameter = np.sqrt(angeioma_volume / math.pi)
    root_vessel_point = Vessel_Point()
    root_vessel_point.position_ijk = np.round(center_point_position_ijk).astype("int")
    root_vessel_point.position_geometry = src_volume.convert_ijk_to_geometry(root_vessel_point.position_ijk)

    largescale_vessel_file = os.path.join(output_dir,"largescale_vessel_best_estimate.nii.gz")
    if os.path.exists(largescale_vessel_file):
        large_scale_vesel_volume = VolumeImage()
        large_scale_vesel_volume.get_image_from_nifti(largescale_vessel_file)
    else:
        large_scale_vesel_volume = ConstantVolumeImage(src_volume,0)
        growing_points,surface_points = FAST_FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_DISTANCE(src_volume, large_scale_vesel_volume,
                                                                                                         root_vessel_point,
                                                                                                         max_value, min_value,
                                                                                                         nominal_angeioma_diameter + 50)
        # growing_points,surface_points,self_index_record,parent_index_record,dst_data_array = FAST_ARRAY_FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_DISTANCE(src_volume.image_data_array,dst_volume.image_data_array,root_vessel_point.position_ijk,10000,1000, (nominal_angeioma_diameter + 20)/np.average(src_volume.spacing))
        large_scale_vesel_volume.saveImageAsNIFTI(largescale_vessel_file)
    # return 0
    # exit(0)

    # get neat vessel volume


    temp_all_vessel_array = np.copy(large_scale_vesel_volume.image_data_array)
    temp_angeioma_array = np.copy(angeioma_gt_volume.image_data_array)
    ## method 1, dialte overlap method
    # temp_angeioma_array[scipy.ndimage.binary_dilation(temp_angeioma_array,structure=np.ones((3,3,3)))] = 1
    #
    # temp_all_vessel_array[np.where(temp_angeioma_array==ANGEIOMA_LABEL)] = 0
    # temp_all_vessel_array = measure.label(temp_all_vessel_array,background=0)
    # temp_all_vessel_array[np.where(temp_all_vessel_array>1)] = 0


    ##method2

    temp_all_vessel_array[np.where(temp_angeioma_array==ANGEIOMA_LABEL)] = 0
    temp_all_vessel_array= scipy.ndimage.binary_erosion(temp_all_vessel_array, structure=np.ones((3, 3, 3))).astype(int)
    temp_all_vessel_array = measure.label(temp_all_vessel_array,background=0)
    temp_all_vessel_array = scipy.ndimage.binary_dilation(temp_all_vessel_array, structure=np.ones((3, 3, 3))).astype(int)
    temp_all_vessel_array[np.where(temp_all_vessel_array>1)] = 0


    neat_vessel_volume.set_image_data_array(temp_all_vessel_array)
    neat_vessel_volume.saveImageAsNIFTI(os.path.join(output_dir,"neat_vessel.nii.gz"))
    vessel_src_array = np.copy(src_volume.image_data_array)
    vessel_src_array[np.where(temp_angeioma_array==ANGEIOMA_LABEL)] = 0

    method = 0

    if method == 0:
        skeleton_array = StayConnectedErodeFilter_scimage(neat_vessel_volume)
        skeleton_array[np.where(skeleton_array>0)] = 1
    if len(np.where(skeleton_array>0)[0]) ==0:
        skeleton_array = np.zeros_like(vessel_src_array)
        filter = skeletonize(speed_power=1.2, Euler_step_size=0.5, depth_th=2, length_th=None, simple_path=True,
                             verbose=False)
        temp_result = filter.skeleton(temp_all_vessel_array)
        for index, vessel in enumerate(temp_result):
            for point in vessel:
                point = np.round(point)
                skeleton_array[int(point[0]), int(point[1]), int(point[2])] = 1

    skeleton_volume.set_image_data_array(skeleton_array)


    skeleton_volume.saveImageAsNIFTI(os.path.join(output_dir,"skeleton.nii.gz"))
    # erode_volume.saveImageAsNIFTI(os.path.join(output_dir,"eroded_vessel_" + str(eroded_count) + ".nii.gz"))


    # get vessel points from erode vessel volume
    centerline_points = np.array(np.nonzero(skeleton_array)).swapaxes(1, 0)

    # get root centerline point from angeioma center point[N,3]
    root_centerline_point = Vessel_Point()
    nomial_root_point = np.repeat(np.array(root_vessel_point.position_ijk)[np.newaxis,:],len(centerline_points),axis=0)
    distance_array = np.sum(np.square(centerline_points - nomial_root_point), axis=-1)
    root_centerline_point.position_ijk = centerline_points[np.argmin(distance_array)]
    root_centerline_point.position_geometry = src_volume.convert_ijk_to_geometry(root_centerline_point.position_ijk)
    root_centerline_point.parent_point_index = -1
    vessel_centerline_points_sections,all_centerline_points = analysize_vessel_centerline_points(root_centerline_point,skeleton_volume)



    max_5mm_diameter  = 0
    max_5mm_diameter_centerline_point_geometry = [0,0,0]
    max_5mm_diameter_contour_points = [[0,0,0],[0,0,0]]
    min_5mm_diameter  = 1000
    min_5mm_diameter_centerline_point_geometry = [0,0,0]
    min_5mm_diameter_contour_points = [[0,0,0],[0,0,0]]
    distance_threshold = 10
    dead_zone = round(5 / src_volume.spacing[0])

    for vessel_index, vessel_section in enumerate(vessel_centerline_points_sections):
        i_node_index_pair, i_leaf_points_index = vessel_section
        if len(i_leaf_points_index) > 5:
            i_leaf_points_ijk = np.array([all_centerline_points[index].position_ijk for index in i_leaf_points_index])
            i_leaf_points_geometry = src_volume.convert_ijk_to_geometry_array(i_leaf_points_ijk)
            mean_points, max_diameter_results, min_diameter_results, avg_diameter_results = get_diameter_of_centerline_points(
                neat_vessel_volume, i_leaf_points_geometry)
            star_node_point = all_centerline_points[i_node_index_pair[0]]
            end_node_point = all_centerline_points[i_node_index_pair[1]]
            csv_data = [[star_node_point.distance_from_root_point,-1,-1] + list(star_node_point.position_geometry)]
            last_distance = star_node_point.distance_from_root_point
            last_point_geometry = star_node_point.position_geometry
            distance_form_star_node = 0
            for mean_point_index, point in enumerate(mean_points):
                last_distance += np.linalg.norm(point - last_point_geometry)
                distance_form_star_node += np.linalg.norm(point - last_point_geometry)
                max_diameter = max_diameter_results[mean_point_index][0]
                min_diameter = min_diameter_results[mean_point_index][0]
                csv_data.append([last_distance, max_diameter, min_diameter] + list(point))
                last_point_geometry = point
                if (mean_point_index >= dead_zone and mean_point_index <= len(mean_points) - dead_zone) and last_distance < distance_threshold:
                    if max_diameter > max_5mm_diameter:
                        max_5mm_diameter = max_diameter
                        max_5mm_diameter_centerline_point_geometry = mean_points[mean_point_index]
                        max_5mm_diameter_contour_points = max_diameter_results[mean_point_index][1]
                        max_5mm_diameter_contour_points.append(max_5mm_diameter_centerline_point_geometry)
                if (mean_point_index >= dead_zone and mean_point_index <= len(mean_points) - dead_zone) and last_distance < distance_threshold:
                    if min_diameter < min_5mm_diameter:
                        min_5mm_diameter = min_diameter
                        min_5mm_diameter_centerline_point_geometry = mean_points[mean_point_index]
                        min_5mm_diameter_contour_points = min_diameter_results[mean_point_index][1]
                        min_5mm_diameter_contour_points.append(min_5mm_diameter_centerline_point_geometry)
            csv_data.append([end_node_point.distance_from_root_point,-1,-1] + list(end_node_point.position_geometry))

            save_diameter_records_to_csv(csv_data,os.path.join(output_dir,"vessel_diameter_record_"+str(vessel_index) + ".csv"))
            save_points_to_slicer_landmarks_file(os.path.join(output_dir,"max_diameter_points.mrk.json"),max_5mm_diameter_contour_points)
    VESSEL_GEOMETRY_DICT = {
        "VESSEL_MAX_DIAMETER":max_5mm_diameter,
        "VESSEL_MAX_DIAMETER_CENTERLINE_POINT":max_5mm_diameter_centerline_point_geometry,
        "VESSEL_MAX_DIAMETER_POINT_A":max_5mm_diameter_contour_points[0],
        "VESSEL_MAX_DIAMETER_POINT_B":max_5mm_diameter_contour_points[1],
        "VESSEL_MIN_DIAMETER":min_5mm_diameter,
        "VESSEL_MIN_DIAMETER_CENTERLINE_POINT":min_5mm_diameter_centerline_point_geometry,
        "VESSEL_MIN_DIAMETER_POINT_A":min_5mm_diameter_contour_points[0],
        "VESSEL_MIN_DIAMETER_POINT_B":min_5mm_diameter_contour_points[1],
    }

    print(VESSEL_GEOMETRY_DICT)
    return VESSEL_GEOMETRY_DICT




def get_nominal_diameter_points(orgin_points,center_point,nominal_diameter):
    p1 = orgin_points[0]
    p2 = orgin_points[1]
    radius = nominal_diameter / 2
    direction = p1 - p2
    p3 = GetNewPosition_EulerDirection(center_point,direction/np.linalg.norm(direction)*radius)
    p4 = GetNewPosition_EulerDirection(center_point,-direction/np.linalg.norm(direction)*radius)
    return [p3,p4]




def refine_vessel_diameter(vessel_points,diameter_records):
    # find bad points
    bad_index = []
    max_diameter = 10
    refine_diameter_records = []
    for index in range(1,vessel_points.shape[0] - 1):
        if diameter_records[index] >= max_diameter or \
            abs(diameter_records[index - 1] - diameter_records[index]) > 0.5 * diameter_records[index - 1]:
            # \
            # diameter_records[index - 1] > 2 * diameter_records[index] or diameter_records[index - 1] < 0.5 * diameter_records[index] or \
            #     diameter_records[index + 1] > 2 * diameter_records[index] or diameter_records[index +- 1] < 0.5 * diameter_records[index] :
            bad_index.append(index)


    for index,point in enumerate(vessel_points):
        # if diameter_records[index] >= 7 or
        pass


def refine_vessel_diameter_left_side(vessel_points,diameter_records):

    refine_diameter_records = [diameter_records[0]]

    for index in range(1,vessel_points.shape[0]):
        vessel_point = vessel_points[index]
        diameter = diameter_records[index]
        last_vessel_point = vessel_points[index - 1]
        last_refine_diameter = refine_diameter_records[index - 1]
        distance = np.linalg.norm(vessel_point - last_vessel_point)
        max_delta_diameter = 1.5 * distance
        if abs(diameter - last_refine_diameter) > max_delta_diameter:
            refine_diameter_records.append(last_refine_diameter + max_delta_diameter)
        else:
            refine_diameter_records.append(diameter)
    return refine_diameter_records


def refine_vessel_diameter_both_side(vessel_points_geometry, diameter_records, vessel_volume:VolumeImage):
    diameter_records = [max(1.5,x) for x in diameter_records]

    num_points = vessel_points_geometry.shape[0]
    fisrt_refine_diameter_records = [diameter_records[0]]
    second_refine_diameter_records = [diameter_records[-1]]

    for index in range(1,num_points):
        vessel_point = vessel_points_geometry[index]
        diameter = diameter_records[index]
        last_vessel_point =  vessel_points_geometry[index - 1]
        last_refine_diameter = fisrt_refine_diameter_records[index - 1]
        distance = np.linalg.norm(vessel_point - last_vessel_point)
        max_delta_diameter = 1.5 * distance
        if diameter - last_refine_diameter > max_delta_diameter:
            fisrt_refine_diameter_records.append(last_refine_diameter + max_delta_diameter)
        else:
            fisrt_refine_diameter_records.append(diameter)


    for index in range(1,num_points):
        real_index = num_points - index - 1
        vessel_point = vessel_points_geometry[real_index]
        diameter = fisrt_refine_diameter_records[real_index]
        last_vessel_point = vessel_points_geometry[real_index + 1]
        last_refine_diameter = second_refine_diameter_records[index - 1]
        distance = np.linalg.norm(vessel_point - last_vessel_point)
        max_delta_diameter = 1.5 * distance
        if diameter - last_refine_diameter > max_delta_diameter:
            second_refine_diameter_records.append(last_refine_diameter + max_delta_diameter)
        else:
            second_refine_diameter_records.append(diameter)
    second_refine_diameter_records.reverse()
    return second_refine_diameter_records


def easy_refine_points(centerline_points_geometry,roi = 2):
    point_num = centerline_points_geometry.shape[0]
    # [roi : point_nun - roi]
    easy_refine_pionts = []
    for i in range(0,roi):
        easy_refine_pionts.append(centerline_points_geometry[i])
    for i in range(roi,point_num - roi):
        roi_points = centerline_points_geometry[i - roi:i + roi + 1]
        direction_euler, mean_point = line_fitting_3d(roi_points)
        easy_refine_pionts.append(mean_point)
    for i in range(roi):
        easy_refine_pionts.append(centerline_points_geometry[point_num - roi + i])

    return np.array(easy_refine_pionts)

def get_diameter_of_centerline_points(angeioma_volume:VolumeImage,centerline_points_geometry, roi = 2):
    point_num = centerline_points_geometry.shape[0]
    # [roi : point_nun - roi]
    max_diameter_results = []
    min_diameter_results = []
    avg_diameter_results = []
    mean_points_geometry = []
    r_each_step = 0.1
    max_r = 8
    for i in range(roi,point_num - roi):
        roi_points = centerline_points_geometry[i - roi:i + roi + 1]
        roi_L = np.linalg.norm(roi_points[-1] - roi_points[0])
        direction_euler, mean_point = line_fitting_3d(roi_points)
        mean_point = centerline_points_geometry[i]
        # temp_points = [list(GetNewPosition_EulerDirection(mean_point,roi_L/roi/2 * direction_euler * i)) for i in range(-5,5)]
        # save_points_to_slicer_landmarks_file(
        #     os.path.join("D:\Coding\\0_data\\benchmark_05\case_01\output", "temp_points.mrk.json"), temp_points)
        cylinder_roi_p1 = GetNewPosition_EulerDirection(mean_point,roi_L/roi/2 * direction_euler)
        cylinder_roi_p2 = GetNewPosition_EulerDirection(mean_point,-roi_L/roi/2 * direction_euler)
        cylinder_roi_points,_= get_roi_cylinder_along_specific_line([cylinder_roi_p1,cylinder_roi_p2],0.1,1,r_each_step,int(max_r/r_each_step))
        L_SIZE,THETA_SIZE,R_SIZE = cylinder_roi_points.shape[:-1]
        # save_points_to_slicer_landmarks_file(
        #     os.path.join("D:\Coding\\0_data\\benchmark_05\case_01\output", "temp_points_cylinder.mrk.json"), [list(point) for point in cylinder_roi_points[:,range(1,360,10),-1,:].reshape((-1,3))])
        angeioma_value_roi_points = angeioma_volume.get_value_in_geometry_array(
            cylinder_roi_points.reshape((-1, 3))).reshape(L_SIZE, THETA_SIZE, R_SIZE)
        angeioma_value_roi_points[:,:,R_SIZE - 1] = 0
        angeioma_value_roi_points[:,:,:2] = 1
        boundry_R_index = np.argmin(angeioma_value_roi_points == 1,axis=-1)
        # boundry_R_index = np.argmin(angeioma_value_roi_points != 0,axis=-1)
        boundry_D_index = boundry_R_index[:,:int(THETA_SIZE/2)] + boundry_R_index[:,int(THETA_SIZE/2):]
        
        # max_diameter
        max_diameter_index = np.unravel_index(np.argmax(boundry_D_index),(L_SIZE,int(THETA_SIZE/2)))
        max_diameter = boundry_D_index[max_diameter_index[0],max_diameter_index[1]]* r_each_step
        max_diameter = max_diameter - np.average(angeioma_volume.spacing)
        max_radius_index_1 = boundry_R_index[max_diameter_index[0],max_diameter_index[1]]
        max_radius_index_2 = boundry_R_index[max_diameter_index[0], max_diameter_index[1] + int(THETA_SIZE/2)]
        max_dimaeter_point_1 = cylinder_roi_points[max_diameter_index[0],max_diameter_index[1],max_radius_index_1]
        max_dimaeter_point_2 = cylinder_roi_points[max_diameter_index[0],max_diameter_index[1] + int(THETA_SIZE/2),max_radius_index_2]
        
        # min_diameter
        min_diameter_index = np.unravel_index(np.argmin(boundry_D_index),(L_SIZE,int(THETA_SIZE/2)))
        min_diameter = boundry_D_index[min_diameter_index[0],min_diameter_index[1]]* r_each_step
        min_radius_index_1 = boundry_R_index[min_diameter_index[0],min_diameter_index[1]]
        min_radius_index_2 = boundry_R_index[min_diameter_index[0], min_diameter_index[1] + int(THETA_SIZE/2)]
        min_dimaeter_point_1 = cylinder_roi_points[min_diameter_index[0],min_diameter_index[1],min_radius_index_1]
        min_dimaeter_point_2 = cylinder_roi_points[min_diameter_index[0],min_diameter_index[1] + int(THETA_SIZE/2),min_radius_index_2]



        if min_diameter == 0:
            print("bingo")

        #avg_diameter
        avg_diameter = np.mean(boundry_D_index) * r_each_step

        mean_points_geometry.append(mean_point)
        max_diameter_results.append([max_diameter,[max_dimaeter_point_1,max_dimaeter_point_2]])
        min_diameter_results.append([min_diameter,[min_dimaeter_point_1,min_dimaeter_point_2]])
        avg_diameter_results.append(avg_diameter)
    return mean_points_geometry,max_diameter_results,min_diameter_results,avg_diameter_results








if __name__ == '__main__':
    src_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa.nii.gz"
    label_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\label.nii.gz"
    mask_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_mask_pipeline.nii.gz"
    largescale_vessel_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_largescale_vessel_fast.nii.gz"
    neat_vessel_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_neat_vessel_fast.nii.gz"
    distance_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_distance_pipline.nii.gz"
    temp_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_distance_temp.nii.gz"

    src_volume = VolumeImage()
    src_volume.get_image_from_nifti(src_file)
    label_volume = VolumeImage()
    label_volume.get_image_from_nifti(label_file)
    dst_volume = ConstantVolumeImage(src_volume, 0)


    extract_vessel_centerline(src_volume,label_volume,"D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\erode_output")