import numpy as np


class Vessel_Point():
    def __init__(self):
        self.position_geometry = np.array([0.0,0.0,0.0],dtype=float)
        self.position_ijk = np.array([0.0,0.0,0.0],dtype=float)
        self.parent_points_index = []
        self.self_point_index = 0
        self.branch_label = 0
        self.distance_from_root_point = 0
        self.child_points_index = []
        self.node_type = 0 # 1: cross node 2:start_node 3:end node
        self.node_index_pair = []
        self.near_node_index = -1
        self.far_node_index = -1




