import os
import re
import SimpleITK as sitk
import numpy as np
import math
import time
import copy

def ijk_within_size(index,box):
    i,j,k = index
    h,w,d = box
    if i < 0 or i > h - 1 or \
            j < 0 or j > w - 1 or \
            k < 0 or k > d - 1:
                return False
    return True

class Vessel_Point():
    def __init__(self):
        self.position_geometry = np.array([0.0,0.0,0.0],dtype=float)
        self.position_ijk = np.array([0.0,0.0,0.0],dtype=float)
        self.parent_point_index = 0
        self.self_point_index = 0
        self.branch_label = 0
        self.distance_from_root_point = 0

class VolumeImage():
    def __init__(self,):
        self.src_itk_image:sitk.Image = None
        self.affine_matrix = None
        self.origin = None
        self.spacing = None
        self.size = None
        self.direction = None
        self.image_data_array = None
        self.IndexToGeometryMatrix = np.identity(3)
        self.GeometryToIndexMatrix = np.identity(3)

    def get_image_from_nifti(self,src_file):
        self.src_itk_image = sitk.ReadImage(src_file)
        self.origin = self.src_itk_image.GetOrigin()
        self.spacing = self.src_itk_image.GetSpacing()
        self.size = self.src_itk_image.GetSize()
        self.direction = self.src_itk_image.GetDirection()
        self.image_data_array = sitk.GetArrayFromImage(self.src_itk_image).swapaxes(0,2) # [D,W,H] / [k,j,i]
        # self.image_data_array = self.image_data_array.swapaxes(0,2)
        self.IndexToGeometryMatrix = self.calculate_affine_matrix(np.array(self.spacing),np.array(self.direction).reshape((3,3)))
        self.GeometryToIndexMatrix = np.linalg.inv(self.IndexToGeometryMatrix)
    def calculate_affine_matrix(self,spacing,direction):
        affine_matrix = (spacing * np.identity(3)).dot(direction)
        return affine_matrix

    def get_value_in_ijk(self,position_ijk:np.array):
        def Interpolotion2D(g_value,s_value,ratio):
            return (1 - ratio) * s_value + ratio * g_value

        i,j,k = position_ijk
        if not ijk_within_size(np.round([i,j,k]),self.size):
            return -1024

        g_i,g_j,g_k = [int(pos) for pos in np.ceil(np.array([i,j,k]))]
        l_i,l_j,l_k =  [int(pos) for pos in np.floor(np.array([i,j,k]))]
        ratio_i,ratio_j,ratio_k = np.array([i,j,k]) - np.array([l_i,l_j,l_k])
        try:
            d_1 = Interpolotion2D(self.src_itk_image.GetPixel([g_i,l_j,l_k]),self.src_itk_image.GetPixel([l_i,l_j,l_k]),ratio_i)
            d_2 = Interpolotion2D(self.src_itk_image.GetPixel([g_i,g_j,l_k]),self.src_itk_image.GetPixel([l_i,g_j,l_k]),ratio_i)
            d_12 = Interpolotion2D(d_2,d_1,ratio_j)

            u_1 = Interpolotion2D(self.src_itk_image.GetPixel([g_i,l_j,g_k]),self.src_itk_image.GetPixel([l_i,l_j,g_k]),ratio_i)
            u_2 = Interpolotion2D(self.src_itk_image.GetPixel([g_i,g_j,g_k]),self.src_itk_image.GetPixel([l_i,g_j,g_k]),ratio_i)
            u_12 = Interpolotion2D(u_2,u_1,ratio_j)

            ud = Interpolotion2D(u_12,d_12,ratio_k)
        except:
            print("sth wrong")

        return ud

    def get_value_in_ijk_array(self,points_array:np.array):
        def Interpolotion2D_array(ceil_value, floor_value, ratio):
            return (1 - ratio) * floor_value + ratio * ceil_value
        #points_array:[N,3]
        points_array[:,0] = np.maximum(points_array[:,0],0)
        points_array[:,0] = np.minimum(points_array[:,0],self.size[0] - 1)

        points_array[:,1] = np.maximum(points_array[:,1],0)
        points_array[:,1] = np.minimum(points_array[:,1],self.size[1] - 1)

        points_array[:,2] = np.maximum(points_array[:,2],0)
        points_array[:,2] = np.minimum(points_array[:,2],self.size[2] - 1)


        #ceil_array:[N,3]
        ceil_array = np.ceil(points_array).astype("int")
        #floor_array:[N,3]
        floor_array = np.floor(points_array).astype("int")
        #ratio_array:[N,3]
        ratio_array = points_array - floor_array
        try:
            # box_points = np.zeros((ceil_array.shape[0],8,3))
            # box_points[:,0,:] = np.array([ceil_array[:,0],floor_array[:,1],floor_array[:,2]]).T.astype("int")
            # box_points[:,1,:] = np.array([floor_array[:,0],floor_array[:,1],floor_array[:,2]]).T.astype("int")
            # box_points[:,2,:] = np.array([ceil_array[:,0],ceil_array[:,1],floor_array[:,2]]).T.astype("int")
            # box_points[:,3,:] = np.array([floor_array[:,0],ceil_array[:,1],floor_array[:,2]]).T.astype("int")
            # box_points[:,4,:] = np.array([ceil_array[:,0],floor_array[:,1],ceil_array[:,2]]).T.astype("int")
            # box_points[:,5,:] = np.array([floor_array[:,0],floor_array[:,1],ceil_array[:,2]]).T.astype("int")
            # box_points[:,6,:] = np.array([ceil_array[:,0],ceil_array[:,1],ceil_array[:,2]]).T.astype("int")
            # box_points[:,7,:] = np.array([floor_array[:,0],ceil_array[:,1],ceil_array[:,2]]).T.astype("int")
            box_points_value = np.zeros((ceil_array.shape[0],8))
            box_points_value[:,0] = self.image_data_array[ceil_array[:,0],floor_array[:,1],floor_array[:,2]]
            box_points_value[:,1] = self.image_data_array[floor_array[:,0],floor_array[:,1],floor_array[:,2]]
            box_points_value[:,2] = self.image_data_array[ceil_array[:,0],ceil_array[:,1],floor_array[:,2]]
            box_points_value[:,3] = self.image_data_array[floor_array[:,0],ceil_array[:,1],floor_array[:,2]]
            box_points_value[:,4] = self.image_data_array[ceil_array[:,0],floor_array[:,1],ceil_array[:,2]]
            box_points_value[:,5] = self.image_data_array[floor_array[:,0],floor_array[:,1],ceil_array[:,2]]
            box_points_value[:,6] = self.image_data_array[ceil_array[:,0],ceil_array[:,1],ceil_array[:,2]]
            box_points_value[:,7] = self.image_data_array[floor_array[:,0],ceil_array[:,1],ceil_array[:,2]]

            d1 = Interpolotion2D_array(box_points_value[:,0],box_points_value[:,1],ratio_array[:,0])
            d2 = Interpolotion2D_array(box_points_value[:,2],box_points_value[:,3],ratio_array[:,0])
            d12 = Interpolotion2D_array(d1,d2,ratio_array[:,1])

            u1 = Interpolotion2D_array(box_points_value[:,4],box_points_value[:,5],ratio_array[:,0])
            u2 = Interpolotion2D_array(box_points_value[:,6],box_points_value[:,7],ratio_array[:,0])
            u12 = Interpolotion2D_array(u1,u2,ratio_array[:,1])

            ud = Interpolotion2D_array(u12,d12,ratio_array[:,2])
            return ud
        except:
            print("sth wrong")

    def get_value_in_geometry(self,position_geometry:np.array):
        position_ijk = self.convert_geometry_to_ijk(position_geometry)
        value = self.get_value_in_ijk(position_ijk)
        return value


    def get_value_in_geometry_array(self,points_array:np.array):
        position_ijk_array = self.convert_geometry_to_ijk_array(points_array)
        value = self.get_value_in_ijk_array(position_ijk_array)
        return value

    def set_value_in_ijk(self,position_ijk:np.array,label:int,flash_image_data_array = False):
        if not ijk_within_size(np.round(position_ijk), self.size):
            return
        i,j,k = np.round(position_ijk).astype(int)
        self.src_itk_image.SetPixel(int(i),int(j),int(k),label)
        if flash_image_data_array:
            self.fresh_image_data_array_from_itk_image()

    def set_image_data_array(self,data_array):
        self.image_data_array = np.copy(data_array)
        self.fresh_itk_image_from_image_data_array()

    def fresh_image_data_array_from_itk_image(self):
        self.image_data_array = sitk.GetArrayFromImage(self.src_itk_image).swapaxes(0,2)

    def fresh_itk_image_from_image_data_array(self):
        self.src_itk_image = sitk.GetImageFromArray(self.image_data_array.swapaxes(0,2))
        self.src_itk_image.SetSpacing(self.spacing)
        self.src_itk_image.SetOrigin(self.origin)
        self.src_itk_image.SetDirection(self.direction)


    def set_value_in_geometry(self,position_geometry:np.array,label:int):
        position_ijk = self.convert_geometry_to_ijk(position_geometry)
        if not ijk_within_size(np.round(position_ijk), self.size):
            return
        self.set_value_in_ijk(position_ijk,label)

    def convert_geometry_to_ijk(self,position_geometry):
        return np.array(self.src_itk_image.TransformPhysicalPointToContinuousIndex(position_geometry.astype(float)))

    def convert_geometry_to_ijk_array(self,position_geometry):
        value = np.matmul(self.GeometryToIndexMatrix,(position_geometry - np.array(self.origin)).T).T
        return value

    def convert_ijk_to_geometry(self,position_index):
        return np.array(self.src_itk_image.TransformContinuousIndexToPhysicalPoint(position_index.astype(float)))

    def convert_ijk_to_geometry_array(self,position_index):
        value = np.matmul(self.IndexToGeometryMatrix, position_index.T).T + np.array(self.origin)
        return value

    def saveImageArrayAsNIFTI(self,nii_file):
        img = sitk.GetImageFromArray(self.image_data_array.swapaxes(0,2))
        img.CopyInformation(self.src_itk_image)
        sitk.WriteImage(img, nii_file)

    def saveImageAsNIFTI(self,nii_file):
        sitk.WriteImage(self.src_itk_image, nii_file)

class ConstantVolumeImage(VolumeImage):
    def __init__(self, template_volume_image:VolumeImage,constant_label:int):
        super().__init__()
        self.image_data_array = np.ones_like(template_volume_image.image_data_array, dtype=float)
        self.image_data_array = self.image_data_array * constant_label
        self.origin = template_volume_image.origin
        self.spacing = template_volume_image.spacing
        self.size = template_volume_image.size
        self.direction = template_volume_image.direction
        self.src_itk_image = sitk.GetImageFromArray(self.image_data_array.swapaxes(0,2))
        self.src_itk_image.CopyInformation(template_volume_image.src_itk_image)
        self.GeometryToIndexMatrix = template_volume_image.GeometryToIndexMatrix
        self.IndexToGeometryMatrix = template_volume_image.IndexToGeometryMatrix

def get_initial_parameters_of_angeioma(src_volume:VolumeImage):
    image_array = src_volume.image_data_array
    pixel_volume = src_volume.spacing[0] * src_volume.spacing[1] * src_volume.spacing[2]
    angeioma_array_index = np.where(image_array == 1)
    angeioma_array = np.array(angeioma_array_index).swapaxes(0,1)
    center_point_position_ijk = np.average(angeioma_array,axis=0)
    angeioma_volume = angeioma_array.shape[0] * pixel_volume
    return center_point_position_ijk,angeioma_volume

def get_region_3d(distance:int):
    region = []
    for i in range(-distance,distance + 1):
        for j in range(-distance,distance + 1):
            for k in range(-distance, distance + 1):
                if [i,j,k] != [0,0,0]:
                    region.append(np.array([i,j,k]))
    return np.array(region)

def FAST_FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_DISTANCE(src_volume:VolumeImage, dst_volume:VolumeImage, root_point:Vessel_Point, upper_value, lower_value,max_search_distance = 50):
    all_searched_vessel_point = []
    vcGrowPt = []

    root_point.self_point_index = 0
    root_point.parent_point_index = -1
    vcGrowPt.append(root_point)
    all_searched_vessel_point.append(root_point)

    REGION = get_region_3d(1)
    searched_point_count = 0
    grow_time_start = time.time()

    src_data_array = copy.deepcopy(src_volume.image_data_array)
    dst_data_array = copy.deepcopy(dst_volume.image_data_array)
    mask_volume = ConstantVolumeImage(src_volume,0)
    mask_data_array = copy.deepcopy(mask_volume.image_data_array)
    searched_point_distance = 0
    while len(vcGrowPt)!=0 and vcGrowPt[0].distance_from_root_point < max_search_distance:
        i_grow_time_start = time.time()
        anchor_point = vcGrowPt[0]
        vcGrowPt.pop(0)
        anchor_x,anchor_y,anchor_z = anchor_point.position_ijk.astype(int)
        anchor_point_src_value = src_data_array[anchor_x,anchor_y,anchor_z]

        grow_point_position_ijk_array = REGION + np.array([anchor_x,anchor_y,anchor_z])
        grow_point_position_ijk_array = np.minimum(grow_point_position_ijk_array,np.array(src_volume.size) - 1)
        grow_point_position_ijk_array = np.maximum(grow_point_position_ijk_array,np.array([0,0,0]))
        grow_point_position_ijk_array = np.array(list(set([tuple(point) for point in grow_point_position_ijk_array])))
        grow_point_mask_value_array = mask_data_array[
            grow_point_position_ijk_array[:, 0], grow_point_position_ijk_array[:, 1], grow_point_position_ijk_array[:, 2]]
        unchecked_mask_point_ijk = grow_point_position_ijk_array[np.where(grow_point_mask_value_array == 0)]
        mask_data_array[
            unchecked_mask_point_ijk[:, 0], unchecked_mask_point_ijk[:, 1], unchecked_mask_point_ijk[:, 2]] = 1
        unchecked_grow_point_src_value_array =src_data_array[
            unchecked_mask_point_ijk[:, 0], unchecked_mask_point_ijk[:, 1], unchecked_mask_point_ijk[:,2]]
        labeling_index = np.intersect1d(np.where(unchecked_grow_point_src_value_array < upper_value),
                                        np.where(unchecked_grow_point_src_value_array > lower_value),
                                        np.where(unchecked_grow_point_src_value_array > 0.6 * anchor_point_src_value))
        labeling_src_point_ijk = unchecked_mask_point_ijk[labeling_index]
        dst_data_array[
            labeling_src_point_ijk[:, 0], labeling_src_point_ijk[:, 1], labeling_src_point_ijk[:, 2]] = 1
        for grow_point_position_ijk in labeling_src_point_ijk:
            grow_vessel_point = Vessel_Point()
            grow_vessel_point.position_ijk = grow_point_position_ijk
            grow_vessel_point.position_geometry = src_volume.convert_ijk_to_geometry(
                grow_point_position_ijk)
            grow_vessel_point.parent_point_index = anchor_point.self_point_index
            grow_vessel_point.self_point_index = len(all_searched_vessel_point)
            grow_vessel_point.distance_from_root_point = np.linalg.norm(grow_vessel_point.position_geometry - anchor_point.position_geometry) + anchor_point.distance_from_root_point
            vcGrowPt.append(grow_vessel_point)
            all_searched_vessel_point.append(grow_vessel_point)
        searched_point_count+= len(unchecked_mask_point_ijk)

    print(f"vessel segmentation time cost: +{(time.time() - grow_time_start):.4f}")

    dst_volume.set_image_data_array(dst_data_array)
    # print("dest volume none zero points:" + str(((np.nonzero(dst_volume.image_data_array))[0]).size))
    mask_volume.set_image_data_array(mask_data_array)
    # print("mask volume none zero points:" + str(((np.nonzero(mask_volume.image_data_array))[0]).size))
    return all_searched_vessel_point,vcGrowPt


def run(src_volume:VolumeImage, angeioma_gt_volume:VolumeImage):
    ########################STEP 0#####################
    # init image volume
    large_scale_vesel_volume = ConstantVolumeImage(src_volume, 0)

    ####################### STEP 1 ###############################
    # region grow vessel tissue
    center_point_position_ijk, angeioma_volume = get_initial_parameters_of_angeioma(angeioma_gt_volume)
    nominal_angeioma_diameter = np.sqrt(angeioma_volume / math.pi)
    root_vessel_point = Vessel_Point()
    root_vessel_point.position_ijk = np.round(center_point_position_ijk).astype("int")
    root_vessel_point.position_geometry = src_volume.convert_ijk_to_geometry(root_vessel_point.position_ijk)

    growing_points, surface_points = FAST_FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_DISTANCE(src_volume,
                                                                                                      large_scale_vesel_volume,
                                                                                                      root_vessel_point,
                                                                                                      10000, 1000,
                                                                                                      nominal_angeioma_diameter + 50)
    return large_scale_vesel_volume


def run_single_case(case_dir,output_base_dir):
    files = os.listdir(case_dir)
    case_name = os.path.basename(case_dir)
    output_dir = os.path.join(output_base_dir,case_name)
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    src_volume_file = os.path.join(case_dir,"dsa.nii.gz")
    seg_file = []
    for file in files:
        if os.path.isfile(os.path.join(case_dir,file)):
            if re.match("^seg_\d+.nii.gz$", file):
                seg_file.append(os.path.join(case_dir,file))
    assert os.path.exists(src_volume_file)

    for isolate_index, isolate_file in enumerate(seg_file):
        print("Processing angeioma seg file : " + isolate_file)
        src_volume = VolumeImage()
        src_volume.get_image_from_nifti(src_volume_file)
        label_volume = VolumeImage()
        label_volume.get_image_from_nifti(isolate_file)
        large_scale_vesel_volume = run(src_volume, label_volume)
        large_scale_vesel_volume.saveImageAsNIFTI(
            os.path.join(output_dir, "seg_" + str(isolate_index) + "_vessel.nii.gz"))



if __name__ == '__main__':
    print(os.getcwd())
    data_path = os.path.join(os.path.dirname(os.getcwd()),"data")
    # data_path = os.path.join(os.getcwd(),"data")
    root_path = os.getcwd()
    input_dir = os.path.join(data_path, "input")
    output_dir = os.path.join(data_path, "output")



    cases = []

    for dir in os.listdir(input_dir):
        if os.path.isdir(os.path.join(input_dir,dir)):
            cases.append(dir)


    print("There are " + str(len(cases)) + " cases")
    for case in cases:
        case_dir = os.path.join(input_dir,case)
        print("processing " + case_dir)
        run_single_case(case_dir,output_dir)

    print("----------------------Vessel Segematation Completed-----------------------")




