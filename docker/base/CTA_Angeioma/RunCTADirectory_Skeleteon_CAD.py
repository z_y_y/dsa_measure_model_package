import os
import shutil

from VesselCenterlineExtraction_ErodeMethod import *
from AngeiomaNeckExtraction import *
from AngeiomaGeomery import *
from skimage import morphology


def run(src_dir,output_base_dir):
    for case in os.listdir(src_dir):
        case_dir = os.path.join(src_dir,case)
        if os.path.isdir(case_dir) :
            print("processsing case : ",case)
            output_dir = os.path.join(output_base_dir,case,"output")
            if not os.path.exists(output_dir):
                os.makedirs(output_dir)
            src_nii_file = os.path.join(case_dir,"dsa.nii.gz")
            label_nii_file = os.path.join(case_dir,"label.nii.gz")
            # preprocessed_file = os.path.join(output_dir,"preprocessed.nii.gz")
            preprocessed_file = src_nii_file
            # if not os.path.exists(os.path.join(output_dir,"all_geometry.csv")):#True:#
            if True:
                if os.path.exists(src_nii_file) and os.path.exists(label_nii_file) :
                    results_dict = {}
                    #step 0 preprocessing

                    # preprocessing_image(src_nii_file,preprocessed_file)

                    #step 1 vessel extration
                    src_volume = VolumeImage()
                    src_volume.get_image_from_nifti(preprocessed_file)
                    angeioma_volume = VolumeImage()
                    angeioma_volume.get_image_from_nifti(label_nii_file)
                    #prefrocessed
                    vessel_geometry_dict = extract_vessel_centerline(src_volume, angeioma_volume,1.5,0.5,output_dir)

                    #step 2 vessel neck extration
                    #method 1
                    neck_volume = ConstantVolumeImage(src_volume, 0)
                    neat_vessel_volume = VolumeImage()
                    neat_vessel_volume.get_image_from_nifti(os.path.join(output_dir,"neat_vessel.nii.gz"))
                    neck_array = Get_Angeioma_Neck_Array_From_VesselImageVolume_And_AngeiomaImageVolue_V2(angeioma_volume,neat_vessel_volume)
                    neck_array = measure.label(neck_array, background=0)
                    neck_array[np.where(neck_array > 1)] = 0
                    neck_volume.set_image_data_array(neck_array)
                    neck_volume.saveImageAsNIFTI(os.path.join(output_dir,"neck.nii.gz"))

                    #method 2
                    # largescale_vessel_volume = VolumeImage()
                    # largescale_vessel_volume.get_image_from_nifti(os.path.join(output_dir,"largescale_vessel_best_estimate.nii.gz"))
                    # neck_volume = ConstantVolumeImage(src_volume, 0)
                    # neck_array = Get_Angeioma_Neck_Array_From_VesselImageArray_And_AngeiomaImageArray(angeioma_volume.image_data_array,largescale_vessel_volume.image_data_array)
                    # neck_volume.set_image_data_array(neck_array)
                    # neck_volume.saveImageAsNIFTI(os.path.join(output_dir,"neck.nii.gz"))

                    max_diameter_points,neck_max_diameter,min_diameter_points,neck_min_diameter,angeioma_neck_info= Get_Angeioma_Neck_Geometry(neck_volume)
                    save_line_points_to_slicer_landmarks_file(os.path.join(output_dir,"L_neck_max_d.mrk.json"),
                                                              max_diameter_points)
                    save_line_points_to_slicer_landmarks_file(os.path.join(output_dir,"L_neck_min_d.mrk.json"),
                                                              min_diameter_points)
                    # refine_max_diameter_points = shrink_neck_diamter(max_diameter_points, - src_volume.spacing[0])
                    # refine_min_diameter_points = shrink_neck_diamter(min_diameter_points, - src_volume.spacing[0])
                    # neck_max_diameter = np.linalg.norm(refine_max_diameter_points[0] - refine_max_diameter_points[1])
                    # neck_min_diameter = np.linalg.norm(refine_min_diameter_points[0] - refine_min_diameter_points[1])

                    # save_line_points_to_slicer_landmarks_file(os.path.join(output_dir,"L_neck_max_d.mrk.json"),
                    #                                           refine_max_diameter_points)
                    # save_line_points_to_slicer_landmarks_file(os.path.join(output_dir,"L_neck_min_d.mrk.json"),
                    #                                           refine_min_diameter_points)

                    neck_geometry_dict = {
                        "NECK_MAX_DIAMETER":neck_max_diameter ,
                        "NECK_MIN_DIAMETER":neck_min_diameter,
                    }
                    #step 3 angeioma geometry
                    angeioma_geometry_dict = get_angeioma_geometry(angeioma_volume, angeioma_neck_info,output_dir)
                    save_geometry_data_to_csv(dict(**neck_geometry_dict,**vessel_geometry_dict,**angeioma_geometry_dict),os.path.join(output_dir,"all_geometry.csv"))

            else:
                shutil.copy(os.path.join(output_dir,"all_geometry.csv"),os.path.join("D:\Coding\\0_data\\benchmark_cad_output","all_geometry_" + str(case) + ".csv"))
if __name__ == '__main__':
    src_dir = "/ai/input_3d_cad"
    output_base_dir = "/ai/output_3d_cad"
    # src_dir = "D:\Coding\\0_data\\benchmark_cad"
    run(src_dir,output_base_dir)