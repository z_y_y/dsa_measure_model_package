import os
from VesselCenterlineExtraction import *
from AngeiomaNeckExtraction import *
from AngeiomaGeomery import *
def run(src_dir):
    for case in os.listdir(src_dir):
        case_dir = os.path.join(src_dir,case)
        if os.path.isdir(case_dir):
            print("processsing case : ",case)
            output_dir = case_dir
            src_nii_file = os.path.join(case_dir,"dsa.nii.gz")
            label_nii_file = os.path.join(case_dir,"label.nii.gz")
            if os.path.exists(src_nii_file) and os.path.exists(label_nii_file) and not os.path.exists(os.path.join(output_dir,"all_geometry.csv")):
                results_dict = {}
                #step 1 vessel extration
                src_volume = VolumeImage()
                src_volume.get_image_from_nifti(src_nii_file)
                angeioma_volume = VolumeImage()
                angeioma_volume.get_image_from_nifti(label_nii_file)
                vessel_geometry_dict = extract_vessel_centerline(src_volume, angeioma_volume,output_dir)

                #step 2 vessel neck extration
                largescale_vessel_volume = VolumeImage()
                largescale_vessel_volume.get_image_from_nifti(os.path.join(output_dir,"largescale_vessel.nii.gz"))

                neck_volume = ConstantVolumeImage(src_volume, 0)
                neck_array = Get_Angeioma_Neck_Array_From_VesselImageArray_And_AngeiomaImageArray(angeioma_volume.image_data_array,largescale_vessel_volume.image_data_array)
                neck_volume.set_image_data_array(neck_array)
                neck_volume.saveImageAsNIFTI(os.path.join(output_dir,"neck.nii.gz"))

                max_diameter_points,max_diameter,min_diameter_points,min_diameter,angeioma_neck_info= Get_Angeioma_Neck_Geometry(neck_volume)
                refine_max_diameter_points = shrink_neck_diamter(max_diameter_points, - src_volume.spacing[0])
                refine_min_diameter_points = shrink_neck_diamter(min_diameter_points, - src_volume.spacing[0])
                neck_max_diameter = np.linalg.norm(refine_max_diameter_points[0] - refine_max_diameter_points[1])
                neck_min_diameter = np.linalg.norm(refine_min_diameter_points[0] - refine_min_diameter_points[1])

                save_line_points_to_slicer_landmarks_file(os.path.join(output_dir,"L_neck_max_d.mrk.json"),
                                                          refine_max_diameter_points)
                save_line_points_to_slicer_landmarks_file(os.path.join(output_dir,"L_neck_min_d.mrk.json"),
                                                          refine_min_diameter_points)

                neck_geometry_dict = {
                    "NECK_MAX_DIAMETER":neck_max_diameter ,
                    "NECK_MIN_DIAMETER":neck_min_diameter,
                }
                #step 3 angeioma geometry
                angeioma_geometry_dict = get_angeioma_geometry(angeioma_volume, angeioma_neck_info,output_dir)
                save_geometry_data_to_csv(dict(**neck_geometry_dict,**vessel_geometry_dict,**angeioma_geometry_dict),os.path.join(output_dir,"all_geometry.csv"))

if __name__ == '__main__':
    # src_dir = "G:\\0.data\\benchmark\\"
    src_dir = "D:\Coding\\0_data\DAS_Benchmark"
    run(src_dir)