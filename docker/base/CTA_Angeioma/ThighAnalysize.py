import os
import shutil

import numpy as np

from VesselCenterlineExtraction_ErodeMethod import *
from AngeiomaNeckExtraction import *
from AngeiomaGeomery import *
from skimage import morphology


def extract_thigh_centerline(src_volume:VolumeImage,max_value,min_value,output_dir):
    #TODO Temprory step for user angeioma volume contain vessel label

    src_array = src_volume.image_data_array
    ########################STEP 0#####################
    # neck_volume = ConstantVolumeImage(src_volume,0)
    skeleton_volume = ConstantVolumeImage(src_volume,0)
    temp_mask_volume = ConstantVolumeImage(src_volume,0)
    end_cap_volume = ConstantVolumeImage(src_volume,0)


    ##################
    if max_value == -1 and min_value == -1:
        min_percentage = 99.7
        min_value = np.percentile(src_volume.image_data_array,min_percentage)
        max_value = src_volume.image_data_array.max()


    method = 0
    temp_mask_array = np.copy(src_volume.image_data_array)
    temp_mask_array = measure.label(temp_mask_array,background=0)
    temp_mask_array[np.where(temp_mask_array>1)] = 0

    temp_mask_volume.set_image_data_array(temp_mask_array)
    temp_mask_volume.saveImageAsNIFTI(os.path.join(output_dir,"temp_mask_volume.nii.gz"))

    if method == 0:
        skeleton_array = StayConnectedErodeFilter_scimage(temp_mask_volume)
        skeleton_array[np.where(skeleton_array>0)] = 1

    elif method ==1:
        skeleton_array = np.zeros_like(src_array)
        filter = skeletonize(speed_power=1.2, Euler_step_size=0.5, depth_th=2, length_th=None, simple_path=True,
                             verbose=False)
        temp_result = filter.skeleton(temp_mask_array)
        for index, vessel in enumerate(temp_result):
            for point in vessel:
                point = np.round(point)
                skeleton_array[int(point[0]), int(point[1]), int(point[2])] = 1

    skeleton_array = measure.label(skeleton_array,background=0)
    skeleton_array[np.where(skeleton_array>1)] = 0
    skeleton_volume.set_image_data_array(skeleton_array)


    skeleton_volume.saveImageAsNIFTI(os.path.join(output_dir,"skeleton.nii.gz"))
    # erode_volume.saveImageAsNIFTI(os.path.join(output_dir,"eroded_vessel_" + str(eroded_count) + ".nii.gz"))


    seed = [60,104,3]
    end_points = get_end_points_from_centerline_volume(skeleton_volume,seed,output_dir)
    end_points_ijk = [point.position_ijk for point in end_points]
    direction_euler,mean_point = line_fitting_3d(end_points_ijk)
    nomial_direction = (end_points_ijk[0] - end_points_ijk[-1])/np.linalg.norm(end_points_ijk[0] - end_points_ijk[-1])
    if np.dot(direction_euler,nomial_direction) < 0:
        direction_euler = - direction_euler

    max_length = 20

    end_cap_line_points = [end_points_ijk[0]]
    for i in range(1,max_length):
        new_point = end_points_ijk[0] + direction_euler * i
        src_value = src_volume.get_value_in_ijk(new_point)
        end_cap_line_points.append(new_point)
        if src_value !=1:
            break

    end_cap_line = [end_cap_line_points[0],end_cap_line_points[-1]]


    roi_points,anchor_points = get_roi_cylinder_along_specific_line(end_cap_line, 0.5, 100, 0.1, 130)
    for roi_point in roi_points.reshape(-1,3):
        end_cap_volume.set_value_in_ijk(roi_point,1)
    end_cap_volume.saveImageAsNIFTI(os.path.join(output_dir,"end_cap.nii.gz"))




def get_end_points_from_centerline_volume(src_volume:VolumeImage,seed_point,output_dir):
    src_array = src_volume.image_data_array
    centerline_points = np.array(np.where(src_array == 1)).swapaxes(0, 1)
    n = centerline_points.shape[0]
    min_distance = 999999
    end_point = centerline_points[0]
    for index in range(n):
        i_point = centerline_points[index,:]
        distance = np.linalg.norm(i_point - seed_point)
        if distance < min_distance:
            end_point = i_point
            min_distance = distance

    root_point = Vessel_Point()
    root_point.position_ijk = end_point
    root_point.position_geometry = src_volume.convert_ijk_to_geometry(root_point.position_ijk)
    root_point.parent_point_index = -1

    temp_volume = ConstantVolumeImage(src_volume,0)
    growing_points,surface_points = FAST_FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_DISTANCE(src_volume,temp_volume,root_point,1.5,0.5,10)
    temp_volume.saveImageAsNIFTI(os.path.join(output_dir,"region_grow.nii.gz"))

    return growing_points

def FAST_FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_DISTANCE(src_volume:VolumeImage, dst_volume:VolumeImage, root_point:Vessel_Point, upper_value, lower_value,max_search_distance = MAX_SEARCH_DISTANCE):
    all_searched_vessel_point = []
    vcGrowPt = []

    root_point.self_point_index = 0
    root_point.parent_point_index = -1
    vcGrowPt.append(root_point)
    all_searched_vessel_point.append(root_point)

    REGION = get_region_3d(1)
    searched_point_count = 0
    grow_time_start = time.time()

    src_data_array = copy.deepcopy(src_volume.image_data_array)
    non_zero_count = np.nonzero(src_data_array)[0].shape[0]
    dst_data_array = copy.deepcopy(dst_volume.image_data_array)
    mask_volume = ConstantVolumeImage(src_volume,0)
    mask_data_array = copy.deepcopy(mask_volume.image_data_array)
    searched_point_distance = 0
    while len(vcGrowPt)!=0 and vcGrowPt[0].distance_from_root_point < max_search_distance and len(all_searched_vessel_point) < non_zero_count * 0.6:
        i_grow_time_start = time.time()
        anchor_point = vcGrowPt[0]
        vcGrowPt.pop(0)
        anchor_x,anchor_y,anchor_z = anchor_point.position_ijk.astype(int)
        anchor_point_src_value = src_data_array[anchor_x,anchor_y,anchor_z]

        grow_point_position_ijk_array = REGION + np.array([anchor_x,anchor_y,anchor_z])
        grow_point_position_ijk_array = np.minimum(grow_point_position_ijk_array,np.array(src_volume.size) - 1)
        grow_point_position_ijk_array = np.maximum(grow_point_position_ijk_array,np.array([0,0,0]))
        grow_point_position_ijk_array = np.array(list(set([tuple(point) for point in grow_point_position_ijk_array])))
        grow_point_mask_value_array = mask_data_array[
            grow_point_position_ijk_array[:, 0], grow_point_position_ijk_array[:, 1], grow_point_position_ijk_array[:, 2]]
        unchecked_mask_point_ijk = grow_point_position_ijk_array[np.where(grow_point_mask_value_array == 0)]
        mask_data_array[
            unchecked_mask_point_ijk[:, 0], unchecked_mask_point_ijk[:, 1], unchecked_mask_point_ijk[:, 2]] = VESSEL_LABEL
        unchecked_grow_point_src_value_array =src_data_array[
            unchecked_mask_point_ijk[:, 0], unchecked_mask_point_ijk[:, 1], unchecked_mask_point_ijk[:,2]]
        labeling_index = np.intersect1d(np.where(unchecked_grow_point_src_value_array < upper_value),
                                        np.where(unchecked_grow_point_src_value_array > lower_value))
        if len(list(labeling_index)) > 0:
            labeling_src_point_ijk = unchecked_mask_point_ijk[labeling_index]
            dst_data_array[
                labeling_src_point_ijk[:, 0], labeling_src_point_ijk[:, 1], labeling_src_point_ijk[:, 2]] = VESSEL_LABEL
            for grow_point_position_ijk in labeling_src_point_ijk:
                grow_vessel_point = Vessel_Point()
                grow_vessel_point.position_ijk = grow_point_position_ijk
                grow_vessel_point.position_geometry = src_volume.convert_ijk_to_geometry(
                    grow_point_position_ijk)
                grow_vessel_point.parent_point_index = anchor_point.self_point_index
                grow_vessel_point.self_point_index = len(all_searched_vessel_point)
                grow_vessel_point.distance_from_root_point = np.linalg.norm(grow_vessel_point.position_geometry - anchor_point.position_geometry) + anchor_point.distance_from_root_point
                vcGrowPt.append(grow_vessel_point)
                all_searched_vessel_point.append(grow_vessel_point)
            searched_point_count+= len(unchecked_mask_point_ijk)

    print(f"region grow time cost: +{(time.time() - grow_time_start):.4f}")

    dst_volume.set_image_data_array(dst_data_array)
    # print("dest volume none zero points:" + str(((np.nonzero(dst_volume.image_data_array))[0]).size))
    mask_volume.set_image_data_array(mask_data_array)
    # print("mask volume none zero points:" + str(((np.nonzero(mask_volume.image_data_array))[0]).size))
    return all_searched_vessel_point,vcGrowPt

def run(src_dir):
    for case in os.listdir(src_dir):
        case_dir = os.path.join(src_dir,case)
        if os.path.isdir(case_dir):
            print("processsing case : ",case)
            output_dir = os.path.join(case_dir,"output")
            if not os.path.exists(output_dir):
                os.mkdir(output_dir)
            src_nii_file = os.path.join(case_dir,"src.nii.gz")
            label_nii_file = os.path.join(case_dir,"label.nii.gz")
            preprocessed_file = os.path.join(output_dir,"preprocessed.nii.gz")
            preprocessed_file = src_nii_file
            if True:
                if os.path.exists(src_nii_file) and os.path.exists(label_nii_file) :
                    src_volume = VolumeImage()
                    src_volume.get_image_from_nifti(preprocessed_file)
                    label_volume = VolumeImage()
                    label_volume.get_image_from_nifti(label_nii_file)
                    # angeioma_volume = VolumeImage()
                    # angeioma_volume.get_image_from_nifti(label_nii_file)
                    extract_thigh_centerline(label_volume, 1.5, 0.5, output_dir)


if __name__ == '__main__':
    src_dir = "D:\Coding\\0_data\leg"
    run(src_dir)