import SimpleITK as sitk
import numpy as np
import json
from skimage.morphology import skeletonize
from skimage import img_as_bool
import networkx as nx
from collections import deque
import cv2
import os

def load_nii_gz_file(file_path):
    return sitk.ReadImage(file_path)

def load_json_points(file_path):
    # 读取JSON文件
    with open(file_path, 'r', encoding='utf-8') as file:
        data = json.load(file)

    # 提取markups.controlPoints下的所有点的二维坐标
    positions = [[point['position'][0],point['position'][1],0] for point in data['markups'][0]['controlPoints']]

    return points['A'], points['B']



def load_txt_points(file_path):
    # 读取JSON文件
    points = []
    with open(file_path, 'r', encoding='utf-8') as file:
        for line in file:
            # 假设每行的格式是"(x, y)"
            # 移除括号并按逗号分割
            x, y = line.strip('\n').split(',')
            # 将字符串转换为整数或浮点数，并添加到列表中
            points.append((round(float(x)), round(float(y))))

    return points[0], points[1]


def extract_centerline(label_image):
    centerline = skeletonize(label_image)
    return centerline

def find_minimum_path(centerline, start_point, end_point):
    G = nx.Graph()
    # 假设centerline是一个二维numpy数组，其中1表示路径，0表示非路径
    rows, cols = centerline.shape
    for row in range(rows):
        for col in range(cols):
            if centerline[row, col]:
                # 对于每个路径点，将其添加到图中，并连接其8个邻居
                for i in range(-1, 2):
                    for j in range(-1, 2):
                        if 0 <= row + i < rows and 0 <= col + j < cols and centerline[row + i, col + j]:
                            G.add_edge((row, col), (row + i, col + j), weight=1)
    # 使用Dijkstra算法找到最短路径
    try:
        path = nx.shortest_path(G, source=tuple(start_point), target=tuple(end_point), weight='weight')
    except:
        return False,None
    return True,path

def calculate_diameter_along_path(path,label_array):
    diameters = []
    for i, point in enumerate(path):
        # 确保有足够的点进行操作



        # 获取line_points
        line_points = path[max(0,i - 5):min(i + 6,len(path))]
        line_points = np.array(line_points, dtype=np.float32)

        # 使用opencv进行直线拟合，获得向量A的参数
        [vx, vy, x, y] = cv2.fitLine(line_points, cv2.DIST_L2, 0, 0.01, 0.01)
        # 向量A
        vector_A = np.array([vx[0], vy[0]])
        # 计算垂直向量B
        vector_B = np.array([-vy[0], vx[0]])

        diameter,center_point, edge_points = calculate_diameter_at_point(point, vector_B, label_array)

        diameters.append((diameter, center_point, edge_points))

    return diameters


def calculate_diameter_at_point(point, direction, label_array):
    step = 0.1
    diameter = 0
    edge_points = [None, None]
    diameter_max_limit = 100
    delta_ratio = 1.2

    H,W = label_array.shape
    # 沿着正方向和负方向搜索
    sign_stop = [False,False]
    sign_distance = [0,0]
    while diameter <= diameter_max_limit:
        for sign_index,sign in enumerate([-1, 1]):
            if not sign_stop[sign_index]:
                P_grow = point + direction * sign_distance[sign_index] * sign
                value = interpolate_label_value(P_grow, label_array)
                diameter += step
                edge_points[sign_index] = P_grow
                sign_distance[sign_index] += step
                if value < 0.5:
                    sign_stop[sign_index] = True

                if sign_distance[sign_index] >= sign_distance[abs(sign_index -1)] * delta_ratio + 0.2 or \
                    sign_distance[sign_index -1] >= sign_distance[abs(sign_index)] * delta_ratio + 0.2:
                    sign_stop[sign_index] = True
            # print(sign,sign_distance[sign_index])
        if  sign_stop[0] == True and  sign_stop[1] == True:
            break
    
    return diameter, point, edge_points


def interpolate_label_value(point, label_array):
    """在给定点进行线性插值"""
    y, x = point
    if x < 0 or y < 0 or x >= label_array.shape[1] - 1 or y >= label_array.shape[0] - 1:
        return 0  # 超出图像边界，返回0
    x0, y0 = int(x), int(y)
    x1, y1 = x0 + 1, y0 + 1
    dx, dy = x - x0, y - y0
    # 双线性插值
    value = (label_array[y0, x0] * (1 - dx) * (1 - dy) +
             label_array[y0, x1] * dx * (1 - dy) +
             label_array[y1, x0] * (1 - dx) * dy +
             label_array[y1, x1] * dx * dy)
    return value

def find_nearst_point(M, A):
    rows, cols = len(M), len(M[0])
    visited = [[False for _ in range(cols)] for _ in range(rows)]
    queue = deque([(A[0], A[1], 0)])  # (row, col, distance)
    visited[A[0]][A[1]] = True

    # 定义8个可能的移动方向（如果只允许上下左右移动，则只需定义这四个方向）
    directions = [(1, 0), (-1, 0), (0, 1), (0, -1), (1, 1), (-1, -1), (1, -1), (-1, 1)]

    while queue:
        row, col, dist = queue.popleft()

        # 如果找到值为1的点，则返回其坐标
        if M[row][col] == True:
            return (row, col), dist

        # 否则，继续搜索周围的点
        for dr, dc in directions:
            r, c = row + dr, col + dc
            if 0 <= r < rows and 0 <= c < cols and not visited[r][c]:
                visited[r][c] = True
                queue.append((r, c, dist + 1))

    return None, None  # 如果没有找到，则返回None


def index_to_world(index, origin, spacing):
    """
    将索引坐标转换为世界坐标。
    :param index: 索引坐标（浮点数）
    :param origin: 图像的原点
    :param spacing: 图像的像素间距
    :return: 世界坐标
    """
    return [origin[i] + index[i] * spacing[i] for i in range(len(index))]

def convert_index_diameter_2_geometry_diameter(src_image,index_a,index_b):
    
    spacing = src_image.GetSpacing()
    origin = src_image.GetOrigin()
    
    point_a_world = index_to_world([index_a[0],index_a[1],1],origin,spacing)
    point_b_world = index_to_world([index_b[0],index_b[1],1],origin,spacing)
    
    # point_a_world = src_image.TransformIndexToPhysicalPoint([round(index_a[0]),round(index_a[1]),1])
    # point_b_world = src_image.TransformIndexToPhysicalPoint([round(index_b[0]),round(index_b[1]),1])
    
    # 计算两点间的欧氏距离
    distance = np.linalg.norm(np.array(point_a_world) - np.array(point_b_world))
    
    return distance


def vessel_extract(label_file,seeds_file):
    
    points = load_txt_points(seeds_file)

    label_image = load_nii_gz_file(label_file)
    label_array = sitk.GetArrayFromImage(label_image)[0,:,:]
    label_array = np.ascontiguousarray(np.swapaxes(label_array,1,0))
    centerline = extract_centerline(label_array)

    
    cv2.imwrite(os.path.join(os.path.dirname(label_file),"centerline.png"),np.uint8(np.swapaxes(centerline,1,0) * 255))
    A_nominal,dist_A = find_nearst_point(centerline,points[0])
    B_nominal,dist_B = find_nearst_point(centerline,points[1])
    status,minimum_path = find_minimum_path(centerline, A_nominal, B_nominal)

    if not status:
        print("not connected")
        return None,[np.uint8(np.swapaxes(centerline,1,0) * 255),None,None]

    min_path_array = np.zeros_like(label_array)
    for point in minimum_path:
        min_path_array[point[0],point[1]] = 1

    
    diameters_info = calculate_diameter_along_path(minimum_path,label_array)
    for index,item in enumerate(diameters_info):
        diameter_index, center_point, [P1,P2] = item
        diameter_geometry = convert_index_diameter_2_geometry_diameter(label_image,P1,P2)
        diameters_info[index] = [diameter_geometry, center_point, [P1,P2]]
    
    diamter_array = np.zeros_like(label_array)
    min_diameter = 10000
    min_diameter_info = None
    for item in diameters_info:
        # print(item)
        if item[0] < min_diameter:
            min_diameter_info = item
            min_diameter = item[0]
        diamter_array[round(item[2][0][0]),round(item[2][0][1])] = 1
        diamter_array[round(item[2][1][0]),round(item[2][1][1])] = 1

    return min_diameter_info,diameters_info, [np.uint8(np.swapaxes(centerline,1,0) * 255),np.uint8(np.swapaxes(min_path_array,1,0) * 255),np.uint8(np.swapaxes(diamter_array,1,0) * 255)]

if __name__ == '__main__':
    # 示例使用
    case_dir = "C:\\Users\Administrator\Desktop\\vessel_seg\\roi\\000089-1"
    src_image_path = os.path.join(case_dir,"000089-1.nii.gz")
    label_image_path = os.path.join(case_dir,"label.nii.gz")
    json_points_path = os.path.join(case_dir,"test_input.mrk.json")

    src_image = load_nii_gz_file(src_image_path)
    label_image = load_nii_gz_file(label_image_path)
    # A, B = load_json_points(json_points_path)
    A = np.array([434,419])
    B = np.array([309,407])

    # A = np.array([419,434])
    # B = np.array([407,309])
    # 转换SimpleITK图像为NumPy数组以方便处理
    label_array = sitk.GetArrayFromImage(label_image)[0,:,:]
    centerline = extract_centerline(label_array)

    cv2.imwrite(os.path.join(case_dir,"centerline.png"),np.uint8(centerline * 255))

    A_nominal,dist_A = find_nearst_point(centerline,A)
    B_nominal,dist_B = find_nearst_point(centerline,B)
    path = find_minimum_path(centerline, A_nominal, B_nominal)

    min_path_array = np.zeros_like(label_array)
    for point in path:
        min_path_array[point[0],point[1]] = 1

    cv2.imwrite(os.path.join(case_dir,"min_path_array.png"),np.uint8(min_path_array * 255))

    diameters_info = calculate_diameter_along_path(path,label_array)

    
    diamter_array = np.zeros_like(label_array)
    min_diameter = 10000
    for item in diameters_info:
        print(item)
        diamter_array[round(item[2][0][0]),round(item[2][0][1])] = 1
        diamter_array[round(item[2][1][0]),round(item[2][1][1])] = 1

    cv2.imwrite(os.path.join(case_dir,"diamter_array.png"),np.uint8(diamter_array * 255))
