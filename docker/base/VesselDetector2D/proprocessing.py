import os
import SimpleITK as sitk
import numpy as np

def refine_diameter(root_dir):
    patients_name = []
    for pat_dir in os.listdir(root_dir):
        if os.path.isdir(os.path.join(root_dir,pat_dir)):
            patients_name.append(pat_dir)
    for pat in patients_name:
        pat_dir = os.path.join(root_dir,pat)
        for file in os.listdir(pat_dir):
            dsa_file = os.path.join(pat_dir,"dsa.nii.gz")
            image = sitk.ReadImage(dsa_file)
            if os.path.isfile(os.path.join(pat_dir,file)):
                if file[-4:] == ".txt":
                    print("refine case : " + pat)
                    geometry_file = os.path.join(pat_dir,file)
                    max_p1,max_p2,min_p1,min_p2 = parse_diameter(geometry_file)
                    max_p1_geometry = image.TransformContinuousIndexToPhysicalPoint(max_p1)
                    max_p2_geometry = image.TransformContinuousIndexToPhysicalPoint(max_p2)
                    min_p1_geometry = image.TransformContinuousIndexToPhysicalPoint(min_p1)
                    min_p2_geometry = image.TransformContinuousIndexToPhysicalPoint(min_p2)
                    max_diemater = np.linalg.norm(np.array(max_p1_geometry) - np.array(max_p2_geometry))
                    min_diemater = np.linalg.norm(np.array(min_p1_geometry) - np.array(min_p2_geometry))
                    print(max_p1_geometry,max_p2_geometry)
                    print(min_p1_geometry,min_p2_geometry)
                    print(max_diemater,min_diemater)


def parse_diameter(txt_file):
    max_p1 = []
    max_p2 = []
    min_p1 = []
    min_p2 = []
    with open(txt_file) as f:
        for line in f.readlines():
            key,value = line.split(":")
            if key == "Index of Max Diameter of Vessel P1  ":
                max_p1 = [float(p) for p in value.split(",")]
            elif key == "Index of Max Diameter of Vessel P2  ":
                max_p2 = [float(p) for p in value.split(",")]
            elif key == "Index of Min Diameter of Vessel P1  ":
                min_p1 = [float(p) for p in value.split(",")]
            elif key == "Index of Min Diameter of Vessel P2  ":
                min_p2 = [float(p) for p in value.split(",")]
    return max_p1,max_p2,min_p1,min_p2

if __name__ == '__main__':
    root_dir = "D:\Coding\\0_data\DAS_Benchmark"
    refine_diameter(root_dir)

