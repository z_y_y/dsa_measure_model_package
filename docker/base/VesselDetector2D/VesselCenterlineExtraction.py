import numpy as np

from utils import *



def extract_vessel_centerline(src_volume:VolumeImage,angeioma_gt_volume:VolumeImage,dst_volume:VolumeImage):

    ####################### STEP 1 ###############################
    #region grow vessel tissue
    center_point_position_ijk,angeioma_volume = get_initial_parameters_of_angeioma(angeioma_gt_volume)
    nominal_angeioma_diameter = np.sqrt(angeioma_volume / math.pi)
    root_vessel_point = Vessel_Point()
    root_vessel_point.position_ijk = np.round(center_point_position_ijk).astype("int")
    root_vessel_point.position_geometry = src_volume.convert_ijk_to_geometry(root_vessel_point.position_ijk)

    growing_points,surface_points = FAST_FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_DISTANCE(src_volume,dst_volume,root_vessel_point,10000,1000, nominal_angeioma_diameter + 20)
    # growing_points,surface_points,self_index_record,parent_index_record,dst_data_array = FAST_ARRAY_FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_DISTANCE(src_volume.image_data_array,dst_volume.image_data_array,root_vessel_point.position_ijk,10000,1000, (nominal_angeioma_diameter + 20)/np.average(src_volume.spacing))
    # dst_volume.set_image_data_array(dst_data_array)
    # dst_volume.saveImageAsNIFTI(dst_file)
    ####################### STEP 2 ###############################
    # Identify Separate Surface and their center point
    surfaces = FAST_separate_vessel_surface_points([np.array(point.position_ijk) for point in surface_points])
    print("seperate vessel surface:" + str(len(surfaces)))
    vessel_section_seed_points = []
    candidates_vessel_point = []
    vessel_section_seed_points = []
    for surface_index,surface in enumerate(surfaces):
        surface = [surface_points[find_same_postion_ijk_in_vessel_surface_points(point,surface_points)] for point in surface]
        for surface_vessel_point in surface:
            point_position_ijk = surface_vessel_point.position_ijk
            # point_position_ijk = surface_vessel_point
            dst_volume.set_value_in_ijk(point_position_ijk, VESSEL_SURFACE_LABEL + surface_index)
        i_surface_center_point_index = find_center_point_in_vessel_surface_points(surface)
        vessel_section_seed_points.append(surface[i_surface_center_point_index])

    ####################### STEP 3 ###############################
    # Back trace parent points of each surface center point
    # points of every vessel section
    continuous_vessel_points_ijk = []
    for surface_index,seed_point in enumerate(vessel_section_seed_points):
        isinstance(seed_point,Vessel_Point)
        i_section_points = []
        parent_point_index = seed_point.parent_point_index
        while parent_point_index != 0:
            i_point = growing_points[parent_point_index]
            # dst_volume.set_value_in_ijk(i_point.position_ijk, VESSEL_CENTERLINE_LABEL + surface_index)
            parent_point_index = i_point.parent_point_index
            i_section_points.append(i_point)
        continuous_vessel_points_ijk.append(i_section_points)


    ####################### STEP 3.5 ###############################
    # select 2 farest vessel section if more than 2 section\
    if len(continuous_vessel_points_ijk ) > 2:
        end_point = np.array([np.array(points[0].position_ijk) for points in continuous_vessel_points_ijk])
        end_point_matrix = end_point[:,np.newaxis,:]
        end_point_matrix = np.repeat(end_point_matrix,end_point_matrix.shape[0],axis=1)
        end_point_matrix_T = end_point_matrix.swapaxes(0,1)
        distance_matrix = np.sum(np.abs(end_point_matrix - end_point_matrix_T),axis=-1)
        max_index = np.where(distance_matrix == np.max(distance_matrix))[0]
        continuous_vessel_points_ijk = [continuous_vessel_points_ijk[i] for i in max_index]





    ####################### STEP 4 ###############################
    # Calculate Distance Volume for Next Centerline Extration
    # distance_volume = ConstantVolumeImage(src_volume,0)
    # distance_data_array = distance_filter(dst_volume.image_data_array)
    # distance_data_array = scipy.ndimage.gaussian_filter(distance_data_array, sigma=1.3, radius=3)
    # distance_volume.set_image_data_array(distance_data_array)
    # distance_volume.saveImageAsNIFTI(distance_file)
    distance_volume = VolumeImage()
    distance_volume.get_image_from_nifti(distance_file)

    ####################### STEP 5 ###############################
    # Refine rough centerline points (contain sparse and resample)
    refine_vessel_points_ijk = []
    refine_vessel_points_geometry = []
    for vessel_index, rough_centerline_points in enumerate(continuous_vessel_points_ijk):
        section_refine_vessel_points_ijk = np.empty((0,3))
        section_refine_vessel_points_geometry = np.empty((0,3))
        rough_centerline_points_array  = [np.array(i_point.position_ijk) for i_point in rough_centerline_points]
        # stride_1_for_1mm = max(10 * 1 / np.linalg.norm(src_volume.spacing))
        sparse_points_ijk = get_refine_sparse_centerline_points_of_single_vessel_section(dst_volume, distance_volume, rough_centerline_points_array, 10)
        # sparse_points_ijk.insert(0,rough_centerline_points[0].position_ijk)
        sparse_points_ijk.insert(-1, rough_centerline_points[-1].position_ijk)
        for point_index in range(len(sparse_points_ijk) - 1):
            point_pair = [sparse_points_ijk[point_index], sparse_points_ijk[point_index + 1]]
            point_pair_geomtry = [src_volume.convert_ijk_to_geometry(point) for point in point_pair]
            resample_points_ijk = resample_centerline_between_two_points(distance_volume,dst_volume,point_pair)
            section_refine_vessel_points_ijk = np.concatenate((section_refine_vessel_points_ijk,resample_points_ijk),axis=0)
            # resample_points_geometry = resample_centerline_between_two_points_geometry(distance_volume,dst_volume,point_pair_geomtry)
            # section_refine_vessel_points_geometry = np.concatenate((section_refine_vessel_points_geometry,resample_points_geometry),axis=0)
        refine_vessel_points_ijk.append(section_refine_vessel_points_ijk)
        refine_vessel_points_geometry.append(section_refine_vessel_points_geometry)


    for vessel_index, refine_centerline_points in enumerate(refine_vessel_points_ijk):
        for point_index,point in enumerate(refine_centerline_points):
            dst_volume.set_value_in_ijk(point,(vessel_index + 2) * 10,True)

    # show_3d_points(refine_vessel_points_ijk[0][:, 0], refine_vessel_points_ijk[0][:, 1],
    #                refine_vessel_points_ijk[0][:, 2])
    # show_3d_points(refine_vessel_points_geometry[0][:, 0], refine_vessel_points_geometry[0][:, 1],
    #                refine_vessel_points_geometry[0][:, 2])

    ####################### STEP 6 ###############################
    # cut off angeioma area from region grow vessel tissue
    # cut off
    # dst_data_array = dst_volume.image_data_array
    # dst_data_array[np.where(label_volume.image_data_array == ANGEIOMA_LABEL)] = 0
    # dst_volume.set_image_data_array(dst_data_array)


    dst_volume.saveImageAsNIFTI(dst_file)





    return


if __name__ == '__main__':

    src_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa.nii.gz"
    label_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\label.nii.gz"
    mask_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_mask_pipeline.nii.gz"
    dst_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_dst_fast.nii.gz"
    distance_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_distance_pipline.nii.gz"
    temp_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_distance_temp.nii.gz"

    src_volume = VolumeImage()
    src_volume.get_image_from_nifti(src_file)
    label_volume = VolumeImage()
    label_volume.get_image_from_nifti(label_file)
    dst_volume = ConstantVolumeImage(src_volume, 0)


    root_vessel_point = Vessel_Point()
    root_vessel_point.position_ijk = np.array([357, 160, 186], dtype=float)
    root_vessel_point.position_geometry = src_volume.convert_ijk_to_geometry(root_vessel_point.position_ijk)
    extract_vessel_centerline(src_volume,label_volume,dst_volume)