import os
from VesselDiameterFilter_eazy_mode import *
from inference_single_nii import *


def write_points_to_file(points, file_path):
    with open(file_path, 'w') as file:
        for point in points:
            # 将每个点格式化为"(x, y)"的形式
            line = f"({point[0]}, {point[1]})\n"
            file.write(line)


def run(data_folder,model_path):

    for case in os.listdir(data_folder):
        print("process case :",case)
        case_dir = os.path.join(data_folder,case)
        src_file = os.path.join(case_dir,"src.nii.gz")
        points_file = os.path.join(case_dir,"seeds.txt")
        label_file = os.path.join(case_dir,"label.nii.gz")
        pred_file = os.path.join(case_dir,"pred.nii.gz")
        output_file = os.path.join(case_dir,"output.txt")



        #step 1 seg vessel using ai model
        model = load_model(model_path)
        predict(model, src_file, pred_file)


        #step2 vessel extraction
        min_diameter_info,diameters_info,[centerline_array, min_path_array, diamter_array] = vessel_extract(pred_file,points_file)
        # print(min_diameter_info)

        cv2.imwrite(os.path.join(case_dir,"centerline.png"),centerline_array)

        with open(output_file, 'w') as file:
            title = "min_diameter,center_point_x,center_point_x,edge_point_A_x,edge_point_A_y,edge_point_B_x,edge_point_B_y\n"

            file.write(title)
            if len(diameters_info) == 0:
                min_diameter,center_point,edge_point_A,edge_point_B = 0,[0,0],[0,0],[0,0]          
                line = f"{min_diameter,center_point[0],center_point[1],edge_point_A[0],edge_point_A[1],edge_point_B[0],edge_point_B[1]}"
                file.write(line)
            else:
                for item in diameters_info:
                    diameter,center_point,[edge_point_A,edge_point_B] =  item 
                    line = f"{diameter,center_point[0],center_point[1],edge_point_A[0],edge_point_A[1],edge_point_B[0],edge_point_B[1]}\n"
                    file.write(line)
                
                cv2.imwrite(os.path.join(case_dir,"min_path_array.png"),min_path_array)
                cv2.imwrite(os.path.join(case_dir,"diamter_array.png"),diamter_array)

if __name__ == '__main__':
    data_folder = '/vessel/input_2d'
    model_path = '/VesselDetector2D/best_model.pth'
    run(data_folder,model_path)