import numpy as np

from utils import *
import matplotlib.pyplot as plt
GLOBAL_MAX_DIAMETER = 10
GLOBAL_IMAGE_SIZE = [512,512]
CONSTANT_Z = 0
def extract_vessel_centerline_2d(src_volume:VolumeImage,tri_points_1,tri_points_2,dst_volume:VolumeImage):
    GLOBAL_IMAGE_SIZE = src_volume.image_data_array.shape
    plot_points_1 = []
    plot_points_2 = []
    # step 1 : get global parameters
    root_points_geometry = np.array([np.array(tri_points_1[0]),
                                     np.array(tri_points_1[1]),
                                     np.array(tri_points_1[2]),
                                     np.array(tri_points_2[0]),
                                     np.array(tri_points_2[1]),
                                     np.array(tri_points_2[2])])
    root_points_ijk = src_volume.convert_geometry_to_ijk_array(root_points_geometry)
    # round z axis
    root_points_ijk[:,-1] = np.round(root_points_ijk[:,-1])
    CONSTANT_Z = int(root_points_ijk[0,-1])
    root_points_ijk_2d = root_points_ijk[:,:-1]


    #get points:[3]
    p1,p2,d1,p3,p4,d2 = root_points_ijk_2d[0,:],root_points_ijk_2d[1,:],root_points_ijk_2d[2,:],root_points_ijk_2d[3,:],root_points_ijk_2d[4,:],root_points_ijk_2d[5,:]
    root_centerline_1 = (p2 + p1) / 2

    # get direction of p1 p2 section
    start_direction_p1p2_2d =  get_direction_from_tri_points(p1,p2,d1)
    start_direction_p3p4_2d =  get_direction_from_tri_points(p3,p4,d2)


    D_p1p2 = np.linalg.norm(p2 - p1)
    D_p3p4 = np.linalg.norm(p2 - p1)
    GLOBAL_MAX_DIAMETER = max(D_p1p2,D_p3p4)*1.5

    #get next point for p1 p2
    count = 0
    vessel_engine_1 = VesselPointEngine((p2 + p1) / 2,p1,p2,start_direction_p1p2_2d,GLOBAL_MAX_DIAMETER,GLOBAL_IMAGE_SIZE,CONSTANT_Z)
    vessel_engine_2 = VesselPointEngine((p3 + p4) / 2,p3,p4,start_direction_p3p4_2d,GLOBAL_MAX_DIAMETER,GLOBAL_IMAGE_SIZE,CONSTANT_Z)

    centerline_point_2d = root_centerline_1[:2]
    vessel_direction_at_centerline_point_2d = start_direction_p1p2_2d
    distance_to_next_centerline_point = D_p1p2 / 3
    roi_radius = D_p1p2 / 2 * 1.2
    success = False
    while(count < 30 and not success):
        vessel_engine_1.step()
        vessel_engine_2.step()
        plot_points_1.append(vessel_engine_1.centerline_point_2d)
        plot_points_1.append(vessel_engine_1.boundry_point_pos)
        plot_points_1.append(vessel_engine_1.boundry_point_neg)
        plot_points_2.append(vessel_engine_2.centerline_point_2d)
        plot_points_2.append(vessel_engine_2.boundry_point_pos)
        plot_points_2.append(vessel_engine_2.boundry_point_neg)
        distance  = np.linalg.norm(vessel_engine_1.centerline_point_2d - vessel_engine_2.centerline_point_2d)
        print("ditance : " + str(distance))
        print("diameter_1 : " + str(vessel_engine_1.vessel_diameter))
        print("diameter_2 : " + str(vessel_engine_2.vessel_diameter))
        if  distance < (vessel_engine_1.vessel_diameter + vessel_engine_2.vessel_diameter) / 2:
            success = True

        # if True :
        # print(centerline_point_2d,CONSTANT_Z,vessel_direction_at_centerline_point_2d,distance_to_next_centerline_point, roi_radius)
        # center_point_2d,boundry_point_2d_pos,boundry_point_2d_neg = get_next_centerline_point_2d(centerline_point_2d,CONSTANT_Z,vessel_direction_at_centerline_point_2d,distance_to_next_centerline_point, roi_radius)
        # nomial_diameter =  np.linalg.norm(boundry_point_2d_pos - boundry_point_2d_neg)
        # balance_diameter = min(GLOBAL_MAX_DIAMETER,nomial_diameter)
        # vessel_direction_at_centerline_point_2d = center_point_2d - centerline_point_2d
        # distance_to_next_centerline_point = balance_diameter/3
        # roi_radius = balance_diameter/ 2 * 1.2
        # vessel_direction_at_centerline_point_2d = vessel_direction_at_centerline_point_2d / np.linalg.norm(vessel_direction_at_centerline_point_2d)
        # centerline_point_2d = center_point_2d
        # plot_points_1.append(centerline_point_2d)
        # plot_points_1.append(boundry_point_2d_pos)
        # plot_points_1.append(boundry_point_2d_neg)

        count +=1

    plot_2d_points(plot_points_1,plot_points_2,src_volume.image_data_array[:,:,CONSTANT_Z])
    pass
class VesselPointEngine():
    def __init__(self,start_centerline_point,start_boundry_point_1,start_boundry_point_2,start_vessel_direction,global_diameter,global_image_size,contant_z):
        self.vessel_direction = start_vessel_direction
        self.vessel_diameter = np.linalg.norm(start_boundry_point_2 - start_boundry_point_1)
        self.distance_to_next_centerline_point = self.vessel_diameter/3
        self.search_radius = self.vessel_diameter / 2 * 1.2
        self.centerline_point_2d = start_centerline_point
        self.boundry_point_pos = start_boundry_point_1
        self.boundry_point_neg = start_boundry_point_2
        self.global_max_diameter = global_diameter
        self.global_image_size = global_image_size
        self.constant_z = contant_z
    def step(self):
        center_point_2d, self.boundry_point_pos, self.boundry_point_neg = get_next_centerline_point_2d(self.centerline_point_2d,
                                                                                                   self.constant_z,
                                                                                                   self.vessel_direction,
                                                                                                   self.distance_to_next_centerline_point,
                                                                                                   self.search_radius,
                                                                                                   self.global_image_size)
        nomial_diameter = np.linalg.norm(self.boundry_point_pos - self.boundry_point_neg)
        balance_diameter = min(self.global_max_diameter, nomial_diameter)
        self.vessel_diameter = balance_diameter
        self.vessel_direction = center_point_2d - self.centerline_point_2d
        self.vessel_direction = self.vessel_direction / np.linalg.norm(self.vessel_direction)
        self.distance_to_next_centerline_point = self.vessel_diameter / 3
        self.search_radius = self.vessel_diameter / 2 * 1.2
        self.centerline_point_2d = center_point_2d



def get_next_centerline_point_2d(root_point_2d,CONSTANT_Z,direction_euler_2d,L,radius,global_image_size = GLOBAL_IMAGE_SIZE):
    anchor_point_2d = GetNewPosition_EulerDirection(root_point_2d,direction_euler_2d * L)
    pos_roi_points_2d, neg_roi_points_2d,radius_direction_2d_Pos, radius_direction_2d_Neg = get_square_roi_2d(anchor_point_2d,direction_euler_2d,radius,radius / 2,100,global_image_size)
    # normalized
    # add max_point to all section
    pos_roi_points_3d = np.zeros((pos_roi_points_2d.shape[0],3))
    pos_roi_points_3d[:,:2] = pos_roi_points_2d
    pos_roi_points_3d[:,-1] = CONSTANT_Z
    neg_roi_points_3d = np.zeros((neg_roi_points_2d.shape[0],3))
    neg_roi_points_3d[:,:2] = neg_roi_points_2d
    neg_roi_points_3d[:,-1] = CONSTANT_Z
    max_value_point_pos = pos_roi_points_2d[np.argmax(src_volume.get_value_in_ijk_array(pos_roi_points_3d))]
    max_value_point_neg = neg_roi_points_2d[np.argmax(src_volume.get_value_in_ijk_array(neg_roi_points_3d))]
    pos_roi_points_2d = np.insert(pos_roi_points_2d,0,max_value_point_neg,0)
    neg_roi_points_2d = np.insert(neg_roi_points_2d,0,max_value_point_pos,0)
    boundry_point_3d_pos = get_boundry_point_in_roi_2d(pos_roi_points_2d,CONSTANT_Z,src_volume,0.5)
    nomial_point_2d_pos = GetNewPosition_EulerDirection(anchor_point_2d,radius_direction_2d_Pos * np.linalg.norm(boundry_point_3d_pos[:2] - anchor_point_2d))
    boundry_point_3d_neg = get_boundry_point_in_roi_2d(neg_roi_points_2d,CONSTANT_Z,src_volume,0.5)
    nomial_point_2d_neg = GetNewPosition_EulerDirection(anchor_point_2d,radius_direction_2d_Neg * np.linalg.norm(boundry_point_3d_neg[:2] - anchor_point_2d))
    center_point_2d = (nomial_point_2d_pos + nomial_point_2d_neg) / 2
    return center_point_2d,nomial_point_2d_pos,nomial_point_2d_neg
    pass

def get_boundry_point_in_roi_2d(roi_points_2d,CONSTANT_Z,src_volume:VolumeImage,ratio = 0.8):
    roi_points_3d = np.zeros((roi_points_2d.shape[0],3))
    roi_points_3d[:,:2] = roi_points_2d
    roi_points_3d[:,-1] = CONSTANT_Z
    value_roi_points = src_volume.get_value_in_ijk_array(roi_points_3d)
    value_roi_points = (np.max(value_roi_points) - value_roi_points)/(np.max(value_roi_points) - np.min(value_roi_points))
    max_value = np.max(value_roi_points)
    vessel_index = np.where(value_roi_points > ratio * max_value)[0]
    max_vessel_index = max(vessel_index)

    ##### gradient #########
    origin_size = value_roi_points.shape
    grad_points_1 = np.zeros((origin_size[0] - 1))
    grad_points_2 = np.zeros((origin_size[0] - 1))
    grad_points_1[:] = value_roi_points[:-1]
    grad_points_2[:] = value_roi_points[1:]
    grad_value = grad_points_2 - grad_points_1
    grad_value_norm = (np.max(grad_value) - grad_value)/(np.max(grad_value) - np.min(grad_value))
    max_grad_value = np.max(grad_value_norm)
    vessel_gard_index = np.where(grad_value_norm > 0 * max_grad_value)[0]
    max_gard_index = np.argmax(vessel_gard_index)

    # max_vessel_index = min(max_vessel_index,max_gard_index)
    boundry_point = roi_points_3d[max_vessel_index]
    return  boundry_point


def get_square_roi_2d(root_point_2d,L_direction_2d,nomial_radius,nominal_L,divide,IMAGE_SIZE = [512,100]):
    radius_step = nomial_radius / divide
    L_step = nominal_L / divide
    L_direction_2d = L_direction_2d / np.linalg.norm(L_direction_2d)
    radius_direction_2d_Pos = GetGetVerticalVectorOfSpecificVector_2d_Geometry2Geometry(L_direction_2d)
    POS_ROI_POINTS = []
    NEG_ROI_POINTS = []
    plot_points = []
    for L_index in range(divide):
        L = L_index * L_step
        L_point = GetNewPosition_EulerDirection(root_point_2d,L * L_direction_2d)
        # pos
        for R_index in range(divide):
            radius = R_index * radius_step
            R_point = GetNewPosition_EulerDirection(L_point, radius * radius_direction_2d_Pos)
            if R_point[0] < 0 or R_point[0]>IMAGE_SIZE[0] or R_point[1] < 0 or R_point[1]>IMAGE_SIZE[1]:
                break
            POS_ROI_POINTS.append(R_point)
            plot_points.append(R_point)
        for R_index in range(divide):
            radius = R_index * radius_step
            R_point = GetNewPosition_EulerDirection(L_point,  - radius * radius_direction_2d_Pos)
            if R_point[0] < 0 or R_point[0]>IMAGE_SIZE[0] or R_point[1] < 0 or R_point[1]>IMAGE_SIZE[1]:
                break
            NEG_ROI_POINTS.append(R_point)
            plot_points.append(R_point)
    # plot_2d_points(plot_points)
    return np.array(POS_ROI_POINTS),np.array(NEG_ROI_POINTS),radius_direction_2d_Pos, - radius_direction_2d_Pos

def get_direction_from_tri_points(p1,p2,p3):
    nominal_direction = GetGetVerticalVectorOfSpecificVector_2d_Geometry2Geometry(p2 - p1)
    nominal_direction = nominal_direction / np.linalg.norm(nominal_direction)
    center_point = (p2+p1)/2
    guide_direction = p3 - center_point
    if np.dot(nominal_direction,guide_direction) > 0:
        return nominal_direction
    else:
        return -nominal_direction

def extract_vessel_centerline(src_volume:VolumeImage,angeioma_gt_volume:VolumeImage,dst_volume:VolumeImage):

    ####################### STEP 1 ###############################
    #region grow vessel tissue
    center_point_position_ijk,angeioma_volume = get_initial_parameters_of_angeioma(angeioma_gt_volume)
    nominal_angeioma_diameter = np.sqrt(angeioma_volume / math.pi)
    root_vessel_point = Vessel_Point()
    root_vessel_point.position_ijk = np.round(center_point_position_ijk).astype("int")
    root_vessel_point.position_geometry = src_volume.convert_ijk_to_geometry(root_vessel_point.position_ijk)

    growing_points,surface_points = FAST_FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_DISTANCE(src_volume,dst_volume,root_vessel_point,10000,1000, nominal_angeioma_diameter + 20)
    # growing_points,surface_points,self_index_record,parent_index_record,dst_data_array = FAST_ARRAY_FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_DISTANCE(src_volume.image_data_array,dst_volume.image_data_array,root_vessel_point.position_ijk,10000,1000, (nominal_angeioma_diameter + 20)/np.average(src_volume.spacing))
    # dst_volume.set_image_data_array(dst_data_array)
    # dst_volume.saveImageAsNIFTI(dst_file)
    ####################### STEP 2 ###############################
    # Identify Separate Surface and their center point
    surfaces = FAST_separate_vessel_surface_points([np.array(point.position_ijk) for point in surface_points])
    print("seperate vessel surface:" + str(len(surfaces)))
    vessel_section_seed_points = []
    candidates_vessel_point = []
    vessel_section_seed_points = []
    for surface_index,surface in enumerate(surfaces):
        surface = [surface_points[find_same_postion_ijk_in_vessel_surface_points(point,surface_points)] for point in surface]
        for surface_vessel_point in surface:
            point_position_ijk = surface_vessel_point.position_ijk
            # point_position_ijk = surface_vessel_point
            dst_volume.set_value_in_ijk(point_position_ijk, VESSEL_SURFACE_LABEL + surface_index)
        i_surface_center_point_index = find_center_point_in_vessel_surface_points(surface)
        vessel_section_seed_points.append(surface[i_surface_center_point_index])

    ####################### STEP 3 ###############################
    # Back trace parent points of each surface center point
    # points of every vessel section
    continuous_vessel_points_ijk = []
    for surface_index,seed_point in enumerate(vessel_section_seed_points):
        isinstance(seed_point,Vessel_Point)
        i_section_points = []
        parent_point_index = seed_point.parent_point_index
        while parent_point_index != 0:
            i_point = growing_points[parent_point_index]
            # dst_volume.set_value_in_ijk(i_point.position_ijk, VESSEL_CENTERLINE_LABEL + surface_index)
            parent_point_index = i_point.parent_point_index
            i_section_points.append(i_point)
        continuous_vessel_points_ijk.append(i_section_points)


    ####################### STEP 3.5 ###############################
    # select 2 farest vessel section if more than 2 section\
    if len(continuous_vessel_points_ijk ) > 2:
        end_point = np.array([np.array(points[0].position_ijk) for points in continuous_vessel_points_ijk])
        end_point_matrix = end_point[:,np.newaxis,:]
        end_point_matrix = np.repeat(end_point_matrix,end_point_matrix.shape[0],axis=1)
        end_point_matrix_T = end_point_matrix.swapaxes(0,1)
        distance_matrix = np.sum(np.abs(end_point_matrix - end_point_matrix_T),axis=-1)
        max_index = np.where(distance_matrix == np.max(distance_matrix))[0]
        continuous_vessel_points_ijk = [continuous_vessel_points_ijk[i] for i in max_index]





    ####################### STEP 4 ###############################
    # Calculate Distance Volume for Next Centerline Extration
    # distance_volume = ConstantVolumeImage(src_volume,0)
    # distance_data_array = distance_filter(dst_volume.image_data_array)
    # distance_data_array = scipy.ndimage.gaussian_filter(distance_data_array, sigma=1.3, radius=3)
    # distance_volume.set_image_data_array(distance_data_array)
    # distance_volume.saveImageAsNIFTI(distance_file)
    distance_volume = VolumeImage()
    distance_volume.get_image_from_nifti(distance_file)

    ####################### STEP 5 ###############################
    # Refine rough centerline points (contain sparse and resample)
    refine_vessel_points_ijk = []
    refine_vessel_points_geometry = []
    for vessel_index, rough_centerline_points in enumerate(continuous_vessel_points_ijk):
        section_refine_vessel_points_ijk = np.empty((0,3))
        section_refine_vessel_points_geometry = np.empty((0,3))
        rough_centerline_points_array  = [np.array(i_point.position_ijk) for i_point in rough_centerline_points]
        # stride_1_for_1mm = max(10 * 1 / np.linalg.norm(src_volume.spacing))
        sparse_points_ijk = get_refine_sparse_centerline_points_of_single_vessel_section(dst_volume, distance_volume, rough_centerline_points_array, 10)
        # sparse_points_ijk.insert(0,rough_centerline_points[0].position_ijk)
        sparse_points_ijk.insert(-1, rough_centerline_points[-1].position_ijk)
        for point_index in range(len(sparse_points_ijk) - 1):
            point_pair = [sparse_points_ijk[point_index], sparse_points_ijk[point_index + 1]]
            point_pair_geomtry = [src_volume.convert_ijk_to_geometry(point) for point in point_pair]
            resample_points_ijk = resample_centerline_between_two_points(distance_volume,dst_volume,point_pair)
            section_refine_vessel_points_ijk = np.concatenate((section_refine_vessel_points_ijk,resample_points_ijk),axis=0)
            # resample_points_geometry = resample_centerline_between_two_points_geometry(distance_volume,dst_volume,point_pair_geomtry)
            # section_refine_vessel_points_geometry = np.concatenate((section_refine_vessel_points_geometry,resample_points_geometry),axis=0)
        refine_vessel_points_ijk.append(section_refine_vessel_points_ijk)
        refine_vessel_points_geometry.append(section_refine_vessel_points_geometry)


    for vessel_index, refine_centerline_points in enumerate(refine_vessel_points_ijk):
        for point_index,point in enumerate(refine_centerline_points):
            dst_volume.set_value_in_ijk(point,(vessel_index + 2) * 10,True)

    # show_3d_points(refine_vessel_points_ijk[0][:, 0], refine_vessel_points_ijk[0][:, 1],
    #                refine_vessel_points_ijk[0][:, 2])
    # show_3d_points(refine_vessel_points_geometry[0][:, 0], refine_vessel_points_geometry[0][:, 1],
    #                refine_vessel_points_geometry[0][:, 2])

    ####################### STEP 6 ###############################
    # cut off angeioma area from region grow vessel tissue
    # cut off
    # dst_data_array = dst_volume.image_data_array
    # dst_data_array[np.where(label_volume.image_data_array == ANGEIOMA_LABEL)] = 0
    # dst_volume.set_image_data_array(dst_data_array)


    dst_volume.saveImageAsNIFTI(dst_file)





    return


if __name__ == '__main__':

    src_file = "D:\Coding\\0_data\\2D\\volume2.nii.gz"
    dst_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\destination.nii.gz"

    src_volume = VolumeImage()
    src_volume.get_image_from_nifti(src_file)
    dst_volume = ConstantVolumeImage(src_volume, 0)


    # p1 = np.array([63.39381787092641,128.43228249138545,1600.0085449218755])
    # p2 = np.array([67.135,133.086,1600.009])
    # p3 = np.array([15.314,34.770,1600.0085449218755])
    # p4 = np.array([9.637,34.487,1600.0085449218755])
    # d1 = np.array([62.942,131.999,1600.009])
    # d2 = np.array([11.624,39.596,1600.009])
    import json
    with open("C:\\Users\superele\Documents\F.mrk.json","r") as f:
        content = json.load(f)
        p1 = np.array(content["markups"][0]["controlPoints"][0]["position"])
        p2 = np.array(content["markups"][0]["controlPoints"][1]["position"])
        d1 = np.array(content["markups"][0]["controlPoints"][2]["position"])
        p3 = np.array(content["markups"][0]["controlPoints"][3]["position"])
        p4 = np.array(content["markups"][0]["controlPoints"][4]["position"])
        d2 = np.array(content["markups"][0]["controlPoints"][5]["position"])
        extract_vessel_centerline_2d(src_volume,[p1,p2,d1],[p3,p4,d2],dst_volume)