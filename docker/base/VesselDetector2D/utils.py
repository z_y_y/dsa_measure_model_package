import SimpleITK as sitk
import numpy as np
import sys
import copy
import scipy
import time
import matplotlib.pyplot as plt

from imageDataProcessor import VolumeImage,ConstantVolumeImage,ijk_within_size
from data_struct import Vessel_Point
from coordinates_system import *

ANGEIOMA_LABEL = 1
VESSEL_LABEL = 1
VESSEL_SURFACE_LABEL = 20
VESSEL_CENTERLINE_LABEL = 30
VESSEL_CENTERLINE_LABEL_TEMP = 100
MAX_SEARCH_POINT = 50000
MAX_SEARCH_DISTANCE = 50


def show_3d_points(x,y,z,tiltle = "3D scatter plot"):
    fig = plt.figure(figsize=(10, 7))
    ax = plt.axes(projection="3d")
    ax.scatter3D(x,y,z, color="red")
    plt.title(tiltle)
    plt.show()

def get_region_3d(distance:int):
    region = []
    for i in range(-distance,distance + 1):
        for j in range(-distance,distance + 1):
            for k in range(-distance, distance + 1):
                if [i,j,k] != [0,0,0]:
                    region.append(np.array([i,j,k]))
    return np.array(region)

def find_same_postion_ijk_in_vessel_surface_points(ijk, vessel_points):
    result = []
    i, j, k = [float(x) for x in ijk]
    for index,vessel_point in enumerate(vessel_points):
        ii, jj, kk = [float(x) for x in vessel_point.position_ijk]
        if i==ii and j==jj and k==kk:
            result.append(index)
    if len(result) != 1:
        print("Test")
    assert len(result) ==1
    return result[-1]


#TO DO
def find_center_point_in_vessel_surface_points(vessel_surface_points):
    totol_postion_ijk = np.array([0,0,0]).astype(float)
    for surface_vessel_point in vessel_surface_points:
        totol_postion_ijk += surface_vessel_point.position_ijk
    center_position_ijk = totol_postion_ijk / len(vessel_surface_points)
    min_distance = 1000
    center_point_index = -1
    for index,surface_vessel_point in enumerate(vessel_surface_points):
        i_distance = np.sum(np.abs(center_position_ijk - surface_vessel_point.position_ijk))
        if i_distance < min_distance:
            center_point_index = index
            min_distance = i_distance
    return center_point_index

def separate_vessel_surface_points(src_points:list):
    surfaces = []
    all_surface_points = copy.deepcopy(src_points)


    REGION = get_region_3d(1)
    while len(all_surface_points)!=0:
        i_surface_points = []
        vcGrowPt = []

        root_vessel_point = all_surface_points[0]
        all_surface_points.pop(0)
        i_surface_points.append(root_vessel_point)
        vcGrowPt.append(root_vessel_point)

        while len(vcGrowPt)!=0:
            anchor_point = vcGrowPt[0]
            vcGrowPt.pop(0)
            for neighbor in REGION:
                grow_point_position_ijk = anchor_point.position_ijk + np.array(neighbor)
                same_postion_index = find_same_postion_ijk_in_vessel_surface_points(grow_point_position_ijk, all_surface_points)
                if same_postion_index != None:
                    vcGrowPt.append(all_surface_points[same_postion_index])
                    i_surface_points.append(all_surface_points[same_postion_index])
                    all_surface_points.pop(same_postion_index)
        if len(i_surface_points) > 0:
            surfaces.append(i_surface_points)
    return surfaces

def FAST_separate_vessel_surface_points(src_points:list):
    surfaces = []
    REGION = get_region_3d(1)
    all_surface_points_array = np.array(copy.deepcopy(src_points))[:,np.newaxis,:]
    all_surface_points_array = np.repeat(all_surface_points_array,REGION.shape[0],axis=1)

    while all_surface_points_array.shape[0] > 0:
        i_surface_points = []
        vcGrowPt = []
        root_vessel_point = all_surface_points_array[0,0,:]
        all_surface_points_array = np.delete(all_surface_points_array,[0],axis=0)
        i_surface_points.append(root_vessel_point)
        vcGrowPt.append(root_vessel_point)

        while len(vcGrowPt)!=0:
            anchor_point = vcGrowPt[0]
            vcGrowPt.pop(0)
            grow_point_position_ijk_array = REGION + anchor_point
            delta = np.sum(np.abs(all_surface_points_array - grow_point_position_ijk_array),axis=-1)
            same_postion_index = np.where(delta==0)
            for index in same_postion_index[0]:
                i_point = all_surface_points_array[index,0,:]
                vcGrowPt.append(i_point)
                i_surface_points.append(i_point)
            all_surface_points_array = np.delete(all_surface_points_array,list(same_postion_index[0]),axis=0)
        if len(i_surface_points) > 0:
            surfaces.append(i_surface_points)
    return surfaces
#357 303 19
def FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_POINT(src_volume:VolumeImage,mask_volume:VolumeImage, dst_volume:VolumeImage, root_point:Vessel_Point, upper_value, lower_value, candidates_vessel_point):
    candidates_vessel_point.clear()
    all_searched_vessel_point = []
    vcGrowPt = []

    root_point.self_point_index = 0
    root_point.parent_point_index = -1
    vcGrowPt.append(root_point)
    all_searched_vessel_point.append(root_point)

    REGION = get_region_3d(1)
    searched_point_count = 0
    grow_time_start = time.time()
    while len(vcGrowPt)!=0 and searched_point_count < MAX_SEARCH_POINT:
        i_grow_time_start = time.time()
        anchor_point = vcGrowPt[0]
        vcGrowPt.pop(0)
        anchor_point_src_value = src_volume.get_value_in_ijk(anchor_point.position_ijk)
        for neighbor in REGION:
            grow_point_position_ijk = anchor_point.position_ijk + np.array(neighbor)
            if not ijk_within_size(grow_point_position_ijk,src_volume.size):continue

            grow_point_mask_value = mask_volume.get_value_in_ijk(grow_point_position_ijk)
            grow_point_src_value = src_volume.get_value_in_ijk(grow_point_position_ijk)

            if grow_point_mask_value == 0:
                mask_volume.set_value_in_ijk(grow_point_position_ijk, VESSEL_LABEL)
                searched_point_count += 1
                if upper_value > grow_point_src_value and \
                    lower_value < grow_point_src_value and \
                        anchor_point_src_value * 0.6 < grow_point_src_value:
                    dst_volume.set_value_in_ijk(grow_point_position_ijk, VESSEL_LABEL)
                    grow_vessel_point = Vessel_Point()
                    grow_vessel_point.position_ijk = grow_point_position_ijk
                    grow_vessel_point.position_geometry = src_volume.convert_ijk_to_geometry(
                        grow_point_position_ijk)
                    grow_vessel_point.parent_point_index = anchor_point.self_point_index
                    grow_vessel_point.self_point_index = len(all_searched_vessel_point)
                    vcGrowPt.append(grow_vessel_point)
                    all_searched_vessel_point.append(grow_vessel_point)

        print(f"each region grow time cost: +{(time.time() - i_grow_time_start):.4f}")

    print(f"region grow time cost: +{(time.time() - grow_time_start):.4f}")
    # for surface_vessel_point in vcGrowPt:
    #     dst_volume.set_value_in_ijk(surface_vessel_point.position_ijk, VESSEL_SURFACE_LABEL)

    dst_volume.fresh_image_data_array_from_itk_image()
    # dst_volume.image_data_array = distance_filter(dst_volume.image_data_array)
    # dst_volume.fresh_itk_image_from_image_data_array()
    # GetCenterlineOfVesselVolumeonErosion(dst_volume)
    # print("dest volume none zero points:" + str(((np.nonzero(dst_volume.image_data_array))[0]).size))


    mask_volume.fresh_image_data_array_from_itk_image()
    # print("mask volume none zero points:" + str(((np.nonzero(mask_volume.image_data_array))[0]).size))

    separate_surface_time_start = time.time()
    surfaces = separate_vessel_surface_points(vcGrowPt)
    print("seperate vessel surface:" + str(len(surfaces)))
    print(f"separate vesel surfce time cost: +{(time.time() - separate_surface_time_start):.4f}")

    vessel_section_seed_points = []

    for surface_index,surface in enumerate(surfaces):
        for surface_vessel_point in surface:
            dst_volume.set_value_in_ijk(surface_vessel_point.position_ijk, VESSEL_SURFACE_LABEL + surface_index)
        i_surface_center_point_index = find_center_point_in_vessel_surface_points(surface)
        vessel_section_seed_points.append(surface[i_surface_center_point_index])
    # points of every vessel section
    candidates_vessel_point.clear()
    for surface_index,seed_point in enumerate(vessel_section_seed_points):
        isinstance(seed_point,Vessel_Point)
        i_section_points = []
        parent_point_index = seed_point.parent_point_index
        while parent_point_index != 0:
            i_point = all_searched_vessel_point[parent_point_index]
            dst_volume.set_value_in_ijk(i_point.position_ijk, VESSEL_CENTERLINE_LABEL + surface_index)
            parent_point_index = i_point.parent_point_index
            i_section_points.append(i_point)
        candidates_vessel_point.append(i_section_points)

def FAST_FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_POINT(large_scale_vessel_volume:VolumeImage, dst_volume:VolumeImage, root_point:Vessel_Point, candidates_vessel_point):
    candidates_vessel_point.clear()
    mask_volume = ConstantVolumeImage(large_scale_vessel_volume,0)
    all_searched_vessel_point = []
    vcGrowPt = []

    root_point.self_point_index = 0
    root_point.parent_point_index = -1
    vcGrowPt.append(root_point)
    all_searched_vessel_point.append(root_point)

    REGION = get_region_3d(1)
    searched_point_count = 0
    grow_time_start = time.time()
    mask_data_array = copy.deepcopy(mask_volume.image_data_array)
    src_data_array = copy.deepcopy(large_scale_vessel_volume.image_data_array)
    dst_data_array = copy.deepcopy(dst_volume.image_data_array)
    while len(vcGrowPt)!=0 and searched_point_count < MAX_SEARCH_POINT:
        i_grow_time_start = time.time()
        anchor_point = vcGrowPt[0]
        vcGrowPt.pop(0)
        anchor_x,anchor_y,anchor_z = anchor_point.position_ijk.astype(int)
        anchor_point_src_value = src_data_array[anchor_x,anchor_y,anchor_z]

        grow_point_position_ijk_array = REGION + np.array([anchor_x,anchor_y,anchor_z])
        grow_point_position_ijk_array = np.minimum(grow_point_position_ijk_array, np.array(large_scale_vessel_volume.size) - 1)
        grow_point_position_ijk_array = np.maximum(grow_point_position_ijk_array,np.array([0,0,0]))
        grow_point_position_ijk_array = np.array(list(set([tuple(point) for point in grow_point_position_ijk_array])))
        grow_point_mask_value_array = mask_data_array[
            grow_point_position_ijk_array[:, 0], grow_point_position_ijk_array[:, 1], grow_point_position_ijk_array[:, 2]]
        unchecked_mask_point_ijk = grow_point_position_ijk_array[np.where(grow_point_mask_value_array == 0)]
        mask_data_array[
            unchecked_mask_point_ijk[:, 0], unchecked_mask_point_ijk[:, 1], unchecked_mask_point_ijk[:, 2]] = VESSEL_LABEL
        unchecked_grow_point_src_value_array =src_data_array[
            unchecked_mask_point_ijk[:, 0], unchecked_mask_point_ijk[:, 1], unchecked_mask_point_ijk[:,2]]
        labeling_index = np.where(unchecked_grow_point_src_value_array == VESSEL_LABEL)
        labeling_src_point_ijk = unchecked_mask_point_ijk[labeling_index]
        dst_data_array[
            labeling_src_point_ijk[:, 0], labeling_src_point_ijk[:, 1], labeling_src_point_ijk[:, 2]] = VESSEL_LABEL
        for grow_point_position_ijk in labeling_src_point_ijk:
            grow_vessel_point = Vessel_Point()
            grow_vessel_point.position_ijk = grow_point_position_ijk
            grow_vessel_point.position_geometry = large_scale_vessel_volume.convert_ijk_to_geometry(
                grow_point_position_ijk)
            grow_vessel_point.parent_point_index = anchor_point.self_point_index
            grow_vessel_point.self_point_index = len(all_searched_vessel_point)
            vcGrowPt.append(grow_vessel_point)
            all_searched_vessel_point.append(grow_vessel_point)
        searched_point_count+= len(unchecked_mask_point_ijk)
        # print(f"each region grow time cost: +{(time.time() - i_grow_time_start):.4f}")
    print(f"region grow time cost: +{(time.time() - grow_time_start):.4f}")
    # for surface_vessel_point in vcGrowPt:
    #     dst_volume.set_value_in_ijk(surface_vessel_point.position_ijk, VESSEL_SURFACE_LABEL)

    dst_volume.set_image_data_array(dst_data_array)
    # dst_volume.image_data_array = distance_filter(dst_volume.image_data_array)
    # dst_volume.fresh_itk_image_from_image_data_array()
    # GetCenterlineOfVesselVolumeonErosion(dst_volume)
    print("dest volume none zero points:" + str(((np.nonzero(dst_volume.image_data_array))[0]).size))


    mask_volume.set_image_data_array(mask_data_array)
    # print("mask volume none zero points:" + str(((np.nonzero(mask_volume.image_data_array))[0]).size))

    separate_surface_time_start = time.time()
    vcGrowPt_array = [np.array(point.position_ijk) for point in vcGrowPt]
    surfaces = FAST_separate_vessel_surface_points(vcGrowPt_array)
    print("seperate vessel surface:" + str(len(surfaces)))
    print(f"separate vesel surfce time cost: +{(time.time() - separate_surface_time_start):.4f}")

    vessel_section_seed_points = []

    for surface_index,surface in enumerate(surfaces):
        surface = [vcGrowPt[find_same_postion_ijk_in_vessel_surface_points(point,vcGrowPt)] for point in surface]
        for surface_vessel_point in surface:
            point_position_ijk = surface_vessel_point.position_ijk
            # point_position_ijk = surface_vessel_point
            dst_volume.set_value_in_ijk(point_position_ijk, VESSEL_SURFACE_LABEL + surface_index)
        i_surface_center_point_index = find_center_point_in_vessel_surface_points(surface)
        vessel_section_seed_points.append(surface[i_surface_center_point_index])
    # points of every vessel section
    candidates_vessel_point.clear()
    for surface_index,seed_point in enumerate(vessel_section_seed_points):
        isinstance(seed_point,Vessel_Point)
        i_section_points = []
        parent_point_index = seed_point.parent_point_index
        while parent_point_index != 0:
            i_point = all_searched_vessel_point[parent_point_index]
            # dst_volume.set_value_in_ijk(i_point.position_ijk, VESSEL_CENTERLINE_LABEL + surface_index)
            parent_point_index = i_point.parent_point_index
            i_section_points.append(i_point)
        candidates_vessel_point.append(i_section_points)

def FAST_FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_DISTANCE(src_volume:VolumeImage, dst_volume:VolumeImage, root_point:Vessel_Point, upper_value, lower_value,max_search_distance = MAX_SEARCH_DISTANCE):
    all_searched_vessel_point = []
    vcGrowPt = []

    root_point.self_point_index = 0
    root_point.parent_point_index = -1
    vcGrowPt.append(root_point)
    all_searched_vessel_point.append(root_point)

    REGION = get_region_3d(1)
    searched_point_count = 0
    grow_time_start = time.time()

    src_data_array = copy.deepcopy(src_volume.image_data_array)
    dst_data_array = copy.deepcopy(dst_volume.image_data_array)
    mask_volume = ConstantVolumeImage(src_volume,0)
    mask_data_array = copy.deepcopy(mask_volume.image_data_array)
    searched_point_distance = 0
    while len(vcGrowPt)!=0 and vcGrowPt[0].distance_from_root_point < max_search_distance:
        i_grow_time_start = time.time()
        anchor_point = vcGrowPt[0]
        vcGrowPt.pop(0)
        anchor_x,anchor_y,anchor_z = anchor_point.position_ijk.astype(int)
        anchor_point_src_value = src_data_array[anchor_x,anchor_y,anchor_z]

        grow_point_position_ijk_array = REGION + np.array([anchor_x,anchor_y,anchor_z])
        grow_point_position_ijk_array = np.minimum(grow_point_position_ijk_array,np.array(src_volume.size) - 1)
        grow_point_position_ijk_array = np.maximum(grow_point_position_ijk_array,np.array([0,0,0]))
        grow_point_position_ijk_array = np.array(list(set([tuple(point) for point in grow_point_position_ijk_array])))
        grow_point_mask_value_array = mask_data_array[
            grow_point_position_ijk_array[:, 0], grow_point_position_ijk_array[:, 1], grow_point_position_ijk_array[:, 2]]
        unchecked_mask_point_ijk = grow_point_position_ijk_array[np.where(grow_point_mask_value_array == 0)]
        mask_data_array[
            unchecked_mask_point_ijk[:, 0], unchecked_mask_point_ijk[:, 1], unchecked_mask_point_ijk[:, 2]] = VESSEL_LABEL
        unchecked_grow_point_src_value_array =src_data_array[
            unchecked_mask_point_ijk[:, 0], unchecked_mask_point_ijk[:, 1], unchecked_mask_point_ijk[:,2]]
        labeling_index = np.intersect1d(np.where(unchecked_grow_point_src_value_array < upper_value),
                                        np.where(unchecked_grow_point_src_value_array > lower_value),
                                        np.where(unchecked_grow_point_src_value_array > 0.6 * anchor_point_src_value))
        labeling_src_point_ijk = unchecked_mask_point_ijk[labeling_index]
        dst_data_array[
            labeling_src_point_ijk[:, 0], labeling_src_point_ijk[:, 1], labeling_src_point_ijk[:, 2]] = VESSEL_LABEL
        for grow_point_position_ijk in labeling_src_point_ijk:
            grow_vessel_point = Vessel_Point()
            grow_vessel_point.position_ijk = grow_point_position_ijk
            grow_vessel_point.position_geometry = src_volume.convert_ijk_to_geometry(
                grow_point_position_ijk)
            grow_vessel_point.parent_point_index = anchor_point.self_point_index
            grow_vessel_point.self_point_index = len(all_searched_vessel_point)
            grow_vessel_point.distance_from_root_point = np.linalg.norm(grow_vessel_point.position_geometry - anchor_point.position_geometry) + anchor_point.distance_from_root_point
            vcGrowPt.append(grow_vessel_point)
            all_searched_vessel_point.append(grow_vessel_point)
        searched_point_count+= len(unchecked_mask_point_ijk)

    print(f"region grow time cost: +{(time.time() - grow_time_start):.4f}")

    dst_volume.set_image_data_array(dst_data_array)
    # print("dest volume none zero points:" + str(((np.nonzero(dst_volume.image_data_array))[0]).size))
    mask_volume.set_image_data_array(mask_data_array)
    # print("mask volume none zero points:" + str(((np.nonzero(mask_volume.image_data_array))[0]).size))
    return all_searched_vessel_point,vcGrowPt

def FAST_ARRAY_FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_DISTANCE(src_data_array:np.array, dst_data_array:np.array, root_point:np.array, upper_value, lower_value,max_search_distance = MAX_SEARCH_DISTANCE):
    dst_data_array = np.copy(dst_data_array)
    all_searched_vessel_point = []
    vcGrowPt = []
    self_index_record = []
    parent_index_record = []
    distance_record = []

    vcGrowPt.append(root_point)
    all_searched_vessel_point.append(root_point)
    self_index_record.append(0)
    parent_index_record.append(-1)
    distance_record.append(0.0)

    REGION = get_region_3d(1)
    searched_point_count = 0
    grow_time_start = time.time()


    mask_data_array = np.zeros_like(src_data_array)
    searched_point_distance = 0
    while len(vcGrowPt)!=0 and distance_record[-1] < max_search_distance:
        anchor_point = vcGrowPt[0]
        anchor_index = self_index_record[0]
        anchor_distance = distance_record[0]
        vcGrowPt.pop(0)
        parent_index_record.pop(0)
        distance_record.pop(0)
        self_index_record.pop(0)

        anchor_x,anchor_y,anchor_z = anchor_point
        anchor_point_src_value = src_data_array[anchor_x,anchor_y,anchor_z]

        grow_point_position_ijk_array = REGION + np.array([anchor_x,anchor_y,anchor_z])
        grow_point_position_ijk_array = np.minimum(grow_point_position_ijk_array,np.array(src_data_array.shape) - 1)
        grow_point_position_ijk_array = np.maximum(grow_point_position_ijk_array,np.array([0,0,0]))
        grow_point_position_ijk_array = np.array(list(set([tuple(point) for point in grow_point_position_ijk_array])))
        grow_point_mask_value_array = mask_data_array[
            grow_point_position_ijk_array[:, 0], grow_point_position_ijk_array[:, 1], grow_point_position_ijk_array[:, 2]]
        unchecked_mask_point_ijk = grow_point_position_ijk_array[np.where(grow_point_mask_value_array == 0)]
        mask_data_array[
            unchecked_mask_point_ijk[:, 0], unchecked_mask_point_ijk[:, 1], unchecked_mask_point_ijk[:, 2]] = VESSEL_LABEL
        unchecked_grow_point_src_value_array =src_data_array[
            unchecked_mask_point_ijk[:, 0], unchecked_mask_point_ijk[:, 1], unchecked_mask_point_ijk[:,2]]
        labeling_index = np.intersect1d(np.where(unchecked_grow_point_src_value_array < upper_value),
                                        np.where(unchecked_grow_point_src_value_array > lower_value),
                                        np.where(unchecked_grow_point_src_value_array > 0.6 * anchor_point_src_value))
        labeling_src_point_ijk = unchecked_mask_point_ijk[labeling_index]
        dst_data_array[
            labeling_src_point_ijk[:, 0], labeling_src_point_ijk[:, 1], labeling_src_point_ijk[:, 2]] = VESSEL_LABEL

        distance_array = np.linalg.norm(labeling_src_point_ijk - anchor_point,axis=-1) + anchor_distance

        vcGrowPt.extend(list(labeling_src_point_ijk))
        valid_vessel_points_count = len(all_searched_vessel_point)
        self_index_record.extend([i + valid_vessel_points_count for i in range(len(labeling_src_point_ijk))])
        parent_index_record.extend([anchor_index for i in range(len(labeling_src_point_ijk))])
        distance_record.extend(list(distance_array))
        all_searched_vessel_point.extend(list(labeling_src_point_ijk))
        searched_point_count += len(unchecked_mask_point_ijk)

    print(f"region grow time cost: +{(time.time() - grow_time_start):.4f}")
    return all_searched_vessel_point,vcGrowPt,self_index_record,parent_index_record,dst_data_array

def laplacian_filter(src_array:np.array):
    dst_array = copy.deepcopy(src_array)

def distance_filter(src_array:np.array):


    dst_array = copy.deepcopy(src_array)
    n_x,n_y,n_z = np.nonzero(src_array)

    #get surface points
    surface_points = []
    REGION =get_region_3d(1)
    for index in range(len(n_x)):
        x = n_x[index]
        y = n_y[index]
        z = n_z[index]
        for region in REGION:
            grow_x,grow_y,grow_z = np.array([x,y,z]) + region
            if src_array[int(grow_x),int(grow_y),int(grow_z)] == 0:
                surface_points.append(np.array([int(grow_x),int(grow_y),int(grow_z)]))
                break
    print("surface time:" + str(len(surface_points)))
    surface_points = np.array(surface_points)
    #cacualte min distance from surface points
    min_distance_array = np.zeros_like(n_x).astype(float)

    calculate_distance_start_time = time.time()
    for index in range(len(n_x)):
        x = n_x[index]
        y = n_y[index]
        z = n_z[index]
        i_min_distance = 1000
        every_point_start_time = time.time()
        distance = np.sum(np.square(surface_points - np.array([x, y, z])), axis=-1)
        min_distance_array[index] = np.sqrt(distance.min())

        # for surface_point in surface_points:
        #     every_surface_start_time = time.time()
        #     sx,sy,sz = surface_point
        #     vector = np.array([sx,sy,sz]) - np.array([x,y,z])
        #     i_distance = np.sqrt(np.sum(np.power(vector,2)))
        #     if i_distance < i_min_distance:
        #         i_min_distance = i_distance
        #         min_distance_array[index] = i_distance
            # print(f"every surface  time cost: +{(time.time() - every_surface_start_time):.4f}")
        # print(f"every point time cost: +{(time.time() - every_point_start_time):.4f}")
    print(f"calculate distance time cost: +{(time.time() - calculate_distance_start_time):.4f}")
    for index,min_distance in enumerate(min_distance_array):
        x = n_x[index]
        y = n_y[index]
        z = n_z[index]
        dst_array[x,y,z] = round(min_distance*100)
    return  dst_array

def LOG_filter(src_array:np.array):
    dst_array = copy.deepcopy(src_array)
    n_x,n_y,n_z = np.nonzero(src_array)

    return  dst_array

def gaussian_filter(src_array:np.array, K_size=3, sigma=1.3):
    img = np.asarray(src_array)
    H, W, D = img.shape

    ## Zero padding
    pad = K_size // 2
    out = np.zeros((H + pad * 2, W + pad * 2, D + pad * 2), dtype=np.float)
    out[pad: pad + H, pad: pad + W,pad: pad + D] = img.copy().astype(np.float)

    ## prepare Kernel
    K = np.zeros((K_size, K_size,K_size), dtype=np.float)
    for x in range(-pad, -pad + K_size):
        for y in range(-pad, -pad + K_size):
            for z in range(-pad, -pad + K_size):
                K[y + pad, x + pad] = np.exp(-(x ** 2 + y ** 2) / (2 * (sigma ** 2)))
    K /= (2 * np.pi * sigma * sigma)
    K /= K.sum()
    tmp = out.copy()

    # filtering
    for y in range(H):
        for x in range(W):
            for c in range(D):
                out[pad + y, pad + x, c] = np.sum(K * tmp[y: y + K_size, x: x + K_size, c])
    out = np.clip(out, 0, 255)
    out = out[pad: pad + H, pad: pad + W].astype(np.uint8)
    return out

def erosion_filter(src_array:np.array):
    dst_array = copy.deepcopy(src_array)
    n_x,n_y,n_z = np.nonzero(src_array)
    REGION =get_region_3d(1)
    for index in range(len(n_x)):
        x = n_x[index]
        y = n_y[index]
        z = n_z[index]
        for region in REGION:
            grow_x,grow_y,grow_z = np.array([x,y,z]) + region
            if src_array[int(grow_x),int(grow_y),int(grow_z)] == 0:
                dst_array[int(x),int(y),int(z)] = 20
                break
    return dst_array

def test_filter(src_volume:VolumeImage):
    data_array = src_volume.image_data_array
    erode_data_array = scipy.ndimage.binary_erosion(data_array,structure=np.ones((3,3,3)))
    data_array[erode_data_array] = 0
    src_volume.set_image_data_array(data_array)

def resample_centerline_between_two_points(distance_volume:VolumeImage, mask_volume:VolumeImage, point_pair:list, divide = 10):
    p1 = np.array(point_pair[0])
    p2 = np.array(point_pair[1])

    mask_data_array = mask_volume.image_data_array
    refine_centerline_points = [p1]
    direction_euler = p2 - p1
    direction_polar = rectangular2polar(direction_euler)
    L = np.linalg.norm(direction_euler)
    stride = round(L / divide)
    phi_stride = 5

    roi_radius = int(round(5 / np.average(np.array(distance_volume.spacing))/2))
    temp_interp_point = []
    for i in range(1,divide):
        direction_polar[0] = stride * i
        i_point = GetNewPosition_PolarDirection(p1,direction_polar)
        temp_interp_point.append(i_point)
        roi_middle_point_cutsurface = []
        for phi_step in range(int(360 / phi_stride)):
            vertical_direction_polar, _ = GetVerticalVectorOfSpecificVector_Polor2Polar(direction_polar,
                                                                                        phi_step * phi_stride)
            for r in range(0, roi_radius):
                vertical_direction_polar[0] = r
                vertical_point = GetNewPosition_PolarDirection(i_point, vertical_direction_polar)
                vertical_point = np.round(np.array(vertical_point)).astype(int)
                vertical_point_value = mask_data_array[vertical_point[0], vertical_point[1], vertical_point[2]]
                if vertical_point_value > 0:
                    roi_middle_point_cutsurface.append(vertical_point)
        roi_middle_point_cutsurface = np.array(roi_middle_point_cutsurface).astype(int)
        try:
            distance_array = distance_volume.image_data_array[
            roi_middle_point_cutsurface[:, 0], roi_middle_point_cutsurface[:, 1], roi_middle_point_cutsurface[:, 2]]
        except:
            print("error")
        max_distance_index = np.argmax(distance_array)
        max_distance = distance_array[max_distance_index]
        if sum(i_point.astype(int) == np.array([-1, 0, 0])) == 3 :
            # show_3d_points(roi_middle_point_cutsurface[:,0],roi_middle_point_cutsurface[:,1],roi_middle_point_cutsurface[:,2])
            for temp_point in roi_middle_point_cutsurface:
                x,y,z = temp_point
                mask_volume.set_value_in_ijk([x,y,z],88)
            print(roi_middle_point_cutsurface[max_distance_index])
            print(max_distance)
        center_point = roi_middle_point_cutsurface[max_distance_index]
        refine_centerline_points.append(np.array(center_point))
    # if len(temp_x)>0:
    #     temp_x.append(vertical_point[0])
    #     temp_y.append(vertical_point[1])
    #     temp_z.append(vertical_point[2])
    #     print("center point:")
    #     print(vertical_point)
    #     show_3d_points(temp_x,temp_y,temp_z)
    # for temp_point in temp_interp_point:
    #     x, y, z = np.round(temp_point).astype("int")
    #     mask_volume.set_value_in_ijk([x, y, z], 66)
    # if sum(p2 == np.array([356, 166, 192])) == 3 :
    #     test1 = rectangular2polar(direction_euler)
    #     test2 = polar2rectangular(test1)
        # for temp_point in temp_interp_point:
        #     x,y,z = np.round(temp_point).astype("int")
        #     mask_volume.set_value_in_ijk([x,y,z],66)

    # refine_centerline_points.append(p2)
    refine_centerline_points = np.array(refine_centerline_points)
    return refine_centerline_points

def resample_centerline_between_two_points_geometry(distance_volume:VolumeImage, mask_volume:VolumeImage, point_pair_geometry:list, divide = 10):
    p1 = np.array(point_pair_geometry[0])
    p2 = np.array(point_pair_geometry[1])

    refine_centerline_points = [p1]
    direction_euler = p2 - p1
    direction_polar = rectangular2polar(direction_euler)
    L = np.linalg.norm(direction_euler)
    stride = L / divide
    phi_stride = 5

    roi_radius = int(round(5 / np.average(np.array(distance_volume.spacing)))) * 2
    temp_interp_point = []
    for i in range(1,divide):
        direction_polar[0] = stride * i
        i_point = GetNewPosition_PolarDirection(p1,direction_polar)
        temp_interp_point.append(i_point)
        max_distance = 0
        max_distance_point = np.array([0,0,0])
        max_ditance_position = []
        for phi_step in range(int(360 / phi_stride)):
            vertical_direction_polar, _ = GetVerticalVectorOfSpecificVector_Polor2Polar(direction_polar,
                                                                                        phi_step * phi_stride)
            for r_step in range(0, roi_radius):
                vertical_direction_polar[0] = r_step * np.average(np.array(distance_volume.spacing)) / 2
                vertical_point = GetNewPosition_PolarDirection(i_point, vertical_direction_polar)
                vertical_point_value = mask_volume.get_value_in_geometry(vertical_point)
                if vertical_point_value > 0:
                    vertical_point_distance = distance_volume.get_value_in_geometry(vertical_point)
                    if vertical_point_distance > max_distance:
                        max_distance = vertical_point_distance
                        max_distance_point = vertical_point
                        max_ditance_position = [phi_step * phi_stride,r_step]
                else:
                    break
        # print(max_ditance_position)
        refine_centerline_points.append(np.copy(np.array(max_distance_point)))
    # if len(temp_x)>0:
    #     temp_x.append(vertical_point[0])
    #     temp_y.append(vertical_point[1])
    #     temp_z.append(vertical_point[2])
    #     print("center point:")
    #     print(vertical_point)
    #     show_3d_points(temp_x,temp_y,temp_z)
    for temp_point in temp_interp_point:
        x, y, z = np.round(temp_point).astype("int")
        # mask_volume.set_value_in_ijk([x, y, z], 66)
    if sum(p2 == np.array([356, 166, 192])) == 3 :
        test1 = rectangular2polar(direction_euler)
        test2 = polar2rectangular(test1)
        # for temp_point in temp_interp_point:
        #     x,y,z = np.round(temp_point).astype("int")
        #     mask_volume.set_value_in_ijk([x,y,z],66)

    # refine_centerline_points.append(p2)
    refine_centerline_points = np.array(refine_centerline_points)
    return refine_centerline_points

def find_best_centerline_of_vessel_section_on_distance_map(distance_volume:VolumeImage, rough_centerline_points:list, stride = 10):

    # find max distance around every 10 rough centerline piont
    low_index = 0
    num_points = len(rough_centerline_points)
    up_index = min(num_points-1,stride)
    refine_centerline_points = []
    roi_radius = int(round(5 / np.average(np.array(distance_volume.spacing))))
    while up_index > low_index and up_index < num_points:
        section_rough_centerline_points = rough_centerline_points[low_index:up_index + 1]
        roi_middle_point_cutsurface = get_intersection_surface_of_vessel(section_rough_centerline_points,roi_radius)
        distance_array = distance_volume.image_data_array[roi_middle_point_cutsurface[:,0],roi_middle_point_cutsurface[:,1],roi_middle_point_cutsurface[:,2]]
        max_distance_index = np.argmax(distance_array)
        max_distance = distance_array[max_distance_index]
        center_point = roi_middle_point_cutsurface[max_distance_index]
        refine_centerline_points.append(np.array(center_point))
        low_index  = up_index
        up_index += stride
        up_index = min(num_points - 1,up_index)
    return refine_centerline_points

def get_intersection_surface_of_vessel(anchor_point:np.array, vessel_direction_Euler:np.array, roi_radius:int, mask_volume:VolumeImage):
    # get roi
    ROI = []
    phi_stride = 5
    direction_polar = rectangular2polar(vessel_direction_Euler)
    for phi_step in range(int(360 / phi_stride)):
        vertical_direction_polar, _ = GetVerticalVectorOfSpecificVector_Polor2Polar(direction_polar,
                                                                                    phi_step * phi_stride)
        for r in range(0, roi_radius):
            vertical_direction_polar[0] = r
            vertical_point = GetNewPosition_PolarDirection(anchor_point, vertical_direction_polar)
            ROI.append(np.array(vertical_point))

    ROI = np.round(np.array(ROI)).astype(int)
    ROI = np.array(list(set([tuple(t) for t in np.round(ROI)])))
    return ROI

def get_connected_intersection_surface_of_vessel(anchor_point:np.array, vessel_direction_Euler:np.array, roi_radius:int, mask_volume:VolumeImage):
    # get roi
    ROI = []
    phi_stride = 5
    direction_polar = rectangular2polar(vessel_direction_Euler)
    mask_data_array = mask_volume.image_data_array
    for phi_step in range(int(360 / phi_stride)):
        vertical_direction_polar, _ = GetVerticalVectorOfSpecificVector_Polor2Polar(direction_polar,
                                                                                phi_step * phi_stride)
        for r in range(0, roi_radius):
            vertical_direction_polar[0] = r
            vertical_point = GetNewPosition_PolarDirection(anchor_point, vertical_direction_polar)
            vertical_point = np.round(np.array(vertical_point)).astype(int)
            vertical_point_value = mask_data_array[vertical_point[0],vertical_point[1],vertical_point[2]]
            if vertical_point_value > 0:
                ROI.append(tuple(vertical_point))
    ROI = np.array(list(set(ROI)))
    x = ROI[:,0]
    y = ROI[:,1]
    z = ROI[:,2]
    return ROI

def refine_rough_centerline_points_to_OnePoint(rough_centerline_points:np.array, vessel_volume:VolumeImage,distance_volume:VolumeImage,roi_radius):
    N = len(rough_centerline_points)
    assert N > 0
    assert roi_radius > 0
    # get vessel direction
    mean_point = np.average(rough_centerline_points, axis=0)
    delta = rough_centerline_points - mean_point
    _, _, vv = np.linalg.svd(delta)
    vessel_direction_Euler = vv[0]/np.linalg.norm(vv[0])

    # get anchor point
    round_mean_point = np.round(mean_point).astype(int)
    value_round_mean_point = vessel_volume.image_data_array[round_mean_point[0],round_mean_point[1],round_mean_point[2]]
    anchor_point = np.array([0,0,0])
    if value_round_mean_point > 0:
        anchor_point = mean_point
    else:
        anchor_point = rough_centerline_points[int(N/2)]

    intersection_surface_points = get_connected_intersection_surface_of_vessel(anchor_point,vessel_direction_Euler,roi_radius,vessel_volume)

    distance_array_of_intersection_surface = distance_volume.image_data_array[
        intersection_surface_points[:, 0], intersection_surface_points[:, 1], intersection_surface_points[:, 2]]
    max_distance_index = np.argmax(distance_array_of_intersection_surface)
    max_distance = distance_array_of_intersection_surface[max_distance_index]
    refine_centerline_point = intersection_surface_points[max_distance_index]

    return refine_centerline_point

def get_refine_sparse_centerline_points_of_single_vessel_section(vessel_volume:VolumeImage, distance_volume:VolumeImage, rough_centerline_points:list, stride = 10):

    # find max distance around every 10 rough centerline piont
    low_index = 0
    num_points = len(rough_centerline_points)
    up_index = min(num_points-1,stride)
    refine_centerline_points = []
    roi_radius = int(round(5 / np.average(np.array(vessel_volume.spacing))/2))
    while up_index > low_index and up_index < num_points:
        i_rough_centerline_points = rough_centerline_points[low_index:up_index + 1]
        center_point = refine_rough_centerline_points_to_OnePoint(i_rough_centerline_points,vessel_volume,distance_volume,roi_radius)
        refine_centerline_points.append(np.array(center_point))
        low_index  = up_index
        up_index += stride
        up_index = min(num_points - 1,up_index)
    return refine_centerline_points

def get_initial_parameters_of_angeioma(src_volume:VolumeImage):
    image_array = src_volume.image_data_array
    pixel_volume = src_volume.spacing[0] * src_volume.spacing[1] * src_volume.spacing[2]
    angeioma_array_index = np.where(image_array == ANGEIOMA_LABEL)
    angeioma_array = np.array(angeioma_array_index).swapaxes(0,1)
    center_point_position_ijk = np.average(angeioma_array,axis=0)
    angeioma_volume = angeioma_array.shape[0] * pixel_volume


    return center_point_position_ijk,angeioma_volume

def get_roi_at_middle_cutsurface(points,radius = 5):
    mean_point = np.average(points,axis=0)
    data = points - mean_point
    _, _, vv = np.linalg.svd(data)
    direction = vv[0]/np.linalg.norm(vv[0])
    # get roi
    ROI = []
    phi_stride = 5
    direction_polar = rectangular2polar(direction)
    for phi_step in range(int(360 / phi_stride)):
        vertical_direction_polar, _ = GetVerticalVectorOfSpecificVector_Polor2Polar(direction_polar,
                                                                                    phi_step * phi_stride)
        for r in range(0, radius):
            vertical_direction_polar[0] = r
            vertical_point = GetNewPosition_PolarDirection(mean_point, vertical_direction_polar)
            ROI.append(np.array(vertical_point))
    ROI = np.array(ROI).astype(int)
    ROI = np.array(list(set([tuple(t) for t in np.round(ROI)])))
    return ROI

def get_roi_cylinder(points,radius = 5):
    # points: [N,3]
    # get nomial_vector
    mean_point = np.average(points,axis=0)
    data = points - mean_point
    _, _, vv = np.linalg.svd(data)
    direction = vv[0]/np.linalg.norm(vv[0])
    nominal_length = int((np.linalg.norm(points[0] - points[-1]))/2)
    # get roi
    ROI = []
    phi_stride = 5
    for l in range(-nominal_length,nominal_length):
        anchor_point = mean_point + l * direction
        direction_polar = rectangular2polar(direction)
        for phi_step in range(int(360 /phi_stride)):
            vertical_direction_polar,_ = GetVerticalVectorOfSpecificVector_Polor2Polar(direction_polar,phi_step * phi_stride)
            for r in range(0,radius):
                vertical_direction_polar[0] = r
                vertical_point = GetNewPosition_PolarDirection(anchor_point,vertical_direction_polar)
                ROI.append(np.array(vertical_point))
    ROI = np.array(ROI).astype(int)
    ROI = np.array(list(set([tuple(t) for t in np.round(ROI)])))
    return ROI

def get_roi_cylinder_along_specifice_line(line,length_gap,length_step,r_gap,r_step):
    root_point = line[0]
    end_point = line[1]
    direction_euler = line[1] - line[0]
    direction_polar = rectangular2polar(direction_euler)
    #enumerate L
    # array of vector root->anchor:[N_L,3],N_L = round(direction_polar[0] / length_gap)
    L_num = round(direction_polar[0] / length_gap)
    root_2_anchor_polar_array = np.array([[index*length_gap,direction_polar[1],direction_polar[2]] for index in range(L_num)])
    root_2_anchor_euler_array = polar2rectangular_array(root_2_anchor_polar_array)
    anchor_points = root_point + root_2_anchor_euler_array

    #enumerate phi and R
    # array of vector anchor->(phi,r):[360,R_count,3],R_count = round(r_step)
    #phi_unit_polar_array:[360,3]
    phi_unit_polar_array = np.array([GetVerticalVectorOfSpecificVector_Polor2Polar(direction_polar,phi / 180 * math.pi)[0] for phi in range(360)])
    # phi_r_eular_array:[360 * R_count,3]
    R_num = round(r_step)
    phi_r_polar_array =np.repeat(phi_unit_polar_array,R_num,axis = 0)
    phi_r_polar_array = np.array([[(index % R_num) * r_gap, phi_r_polar_array[index,1], phi_r_polar_array[index,2]] for index in range(phi_r_polar_array.shape[0])])
    phi_r_eular_array = polar2rectangular_array(phi_r_polar_array)


    #root_2_phi_r_euler_array:[N_L,360 * R_count,3]
    root_2_phi_r_euler_array = np.repeat(root_2_anchor_euler_array[:,np.newaxis,:],360 * R_num,axis=1)
    root_2_phi_r_euler_array = root_2_phi_r_euler_array + phi_r_eular_array


    roi_points = root_2_phi_r_euler_array + root_point
    roi_points = roi_points.reshape((L_num,360,R_num,3))
    return roi_points,np.repeat(anchor_points[:,np.newaxis,:],360 * R_num ,axis=1).reshape((L_num,360,R_num,3))

def if_erosion_valid(src_array:np.array,dst_array:np.array, anchor_point:np.array):
    REGION =get_region_3d(1)
    count_src_nonzero = 0
    stack_region = []
    has_zero_region = False
    for region in REGION:
        x,y,z = anchor_point
        grow_x, grow_y, grow_z = np.array([x, y, z]) + region
        if src_array[int(grow_x), int(grow_y), int(grow_z)] == 0:
            has_zero_region = True
        else:
            stack_region.append(np.array(region))
            count_src_nonzero += 1
        # if dst_array[int(grow_x), int(grow_y), int(grow_z)] != 0:
        #     stack_region.append(np.array(region))
    if count_src_nonzero < 1:
        return False
    elif not has_zero_region:
        return False
    else:
        result = check_region_connected(stack_region)
        return result

def check_region_connected(stack_region):
    REGION =get_region_3d(1)
    vcGrowPt = []
    vcGrowPt.append(stack_region[0])
    stack_region.pop(0)
    while len(vcGrowPt)!=0:
        anchor_point = vcGrowPt[0]
        vcGrowPt.pop(0)
        for neighbor in REGION:
            grow_point_position_ijk = anchor_point + np.array(neighbor)
            for index,t_region in enumerate(stack_region):
                if grow_point_position_ijk[0] == t_region[0] and \
                        grow_point_position_ijk[1] == t_region[1] and \
                        grow_point_position_ijk[2] == t_region[2] :
                        vcGrowPt.append(stack_region[index])
                        stack_region.pop(index)
    if len(stack_region)>0:
        return False
    return True

def StayConnectedErodeFilter(src_volume:VolumeImage):
    src_array = copy.deepcopy(src_volume.image_data_array)
    dst_array = copy.deepcopy(src_volume.image_data_array)
    n_x,n_y,n_z = np.nonzero(src_array)
    REGION =get_region_3d(1)
    eroded_count = 0
    for index in range(len(n_x)):
        x = n_x[index]
        y = n_y[index]
        z = n_z[index]
        if if_erosion_valid(src_array, dst_array,np.array([x, y, z])):
            dst_array[int(x), int(y), int(z)] = 0
            eroded_count +=1
    print("erode points:" + str(eroded_count))
    return dst_array,eroded_count

def GetCenterlineOfVesselVolumeonErosion(src_volume:VolumeImage):
    vessel_data = src_volume.image_data_array
    for i in range(2):
        vessel_data = erosion_filter(vessel_data)
    src_volume.image_data_array = vessel_data
    src_volume.fresh_itk_image_from_image_data_array()
    # src_volume.saveImageAsNIFTI(dst_volume)



def projecting_points(points_cloud,plane_equation):

    # 定义平面方程
    A, B, C, D = plane_equation


    # 计算平面法向量的模长
    L = np.sqrt(A ** 2 + B ** 2 + C ** 2)

    # 计算平面位置向量
    p = np.array([-D * A / L ** 2, -D * B / L ** 2, -D * C / L ** 2])

    vector = np.array([A, B, C])

    # 计算每个点在平面上的投影坐标

    distance = (np.dot(points_cloud, vector) + D) / L
    project_points = points_cloud - distance[:,np.newaxis] * vector

    return  project_points
    # 输出投影后的点云坐标xs

def plot_2d_points(points_1,points_2,img_2d:np.array):
    points_1 = np.array(points_1)
    points_2 = np.array(points_2)
    # 绘制点云
    plt.scatter(points_1[:, 0], points_1[:, 1], s=5, c="red")
    plt.scatter(points_1[0, 0], points_1[0, 1], s=5, c="blue")
    plt.scatter(points_2[:, 0], points_2[:, 1], s=5, c="blue")
    plt.scatter(points_2[0, 0], points_2[0, 1], s=5, c="red")
    plt.imshow(np.swapaxes(img_2d,1,0))
    plt.gca().set_aspect('equal')

    plt.xlabel("X")
    plt.ylabel("Y")
    plt.show()
    pass
if __name__ == '__main__':
    src_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa.nii.gz"
    mask_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_mask_pipeline.nii.gz"
    dst_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_dst_fast.nii.gz"
    distance_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_distance_pipline.nii.gz"
    temp_file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_distance_temp.nii.gz"

    src_volume = VolumeImage()
    src_volume.get_image_from_nifti(src_file)
    dst_volume = ConstantVolumeImage(src_volume,0)
    distance_volume = ConstantVolumeImage(src_volume,0)
    mask_volume = ConstantVolumeImage(src_volume,0)
    temp_volume = ConstantVolumeImage(src_volume,0)

    angeioma_volume = get_volume_of_angeioma(src_volume)

    root_vessel_point = Vessel_Point()
    root_vessel_point.position_ijk = np.array([357,160,186],dtype=float)
    root_vessel_point.position_geometry = src_volume.convert_ijk_to_geometry(root_vessel_point.position_ijk)
    # end -> root
    candidate_vessel_points = []
    #dynamic volume
    MAX_SEARCH_POINT = math.pi * (3**2) * 50 / (src_volume.spacing[0] *src_volume.spacing[1] *src_volume.spacing[2])
    FAST_FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_DISTANCE(src_volume, mask_volume, dst_volume, root_vessel_point, 10000, 1000)
    dst_volume.saveImageAsNIFTI(dst_file)


    # for i in range(len(candidate_vessel_points)):
    #     for j in range(len(candidate_vessel_points[i])):
    #         x,y,z = candidate_vessel_points[i][j].position_ijk
    #         temp_volume.set_value_in_ijk([x,y,z], VESSEL_CENTERLINE_LABEL_TEMP + i)
    #
    # temp_volume.saveImageAsNIFTI(temp_file)
    # exit(0)


    # distance_volume.get_image_from_nifti(distance_file)
    # for vessel_index, rough_centerline_points in enumerate(candidate_vessel_points):
    #     rough_centerline_points_array  = [np.array(point.position_ijk) for point in rough_centerline_points]
    #     sparse_points =  get_refine_sparse_centerline_points_of_single_vessel_section(dst_volume, distance_volume, rough_centerline_points_array, 10)
    #     for point_index in range(len(sparse_points) ):
    #         x, y, z = sparse_points[point_index]
    #         dst_volume.set_value_in_ijk([x,y,z], VESSEL_CENTERLINE_LABEL + vessel_index)
    # dst_volume.saveImageAsNIFTI(dst_file)
    # exit(0)



    distance_volume.get_image_from_nifti(distance_file)
    for vessel_index, rough_centerline_points in enumerate(candidate_vessel_points):
        rough_centerline_points_array  = [np.array(point.position_ijk) for point in rough_centerline_points]
        sparse_points =  get_refine_sparse_centerline_points_of_single_vessel_section(dst_volume, distance_volume, rough_centerline_points_array, 10)
        # sparse_points.insert(0,rough_centerline_points[0].position_ijk)
        # sparse_points.insert(-1, rough_centerline_points[-1].position_ijk)
        for point_index in range(len(sparse_points) - 1):
            point_pair = [sparse_points[point_index], sparse_points[point_index + 1]]
            resample_points = resample_centerline_between_two_points(distance_volume,dst_volume,point_pair)
            for point in resample_points:
                # print(point)
                x, y, z = point
                dst_volume.set_value_in_ijk([x,y,z], VESSEL_CENTERLINE_LABEL + vessel_index)
    dst_volume.saveImageAsNIFTI(dst_file)
    exit(0)


    src_volume = VolumeImage()
    dst_volume = VolumeImage()
    distance_volume = VolumeImage()
    src_volume.get_image_from_nifti(src_file)

    dst_volume.get_image_from_nifti(dst_file)
    distance_volume.get_image_from_nifti(distance_file)
    i_points = np.load("D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\\section_points_array.npy")
    sparse_points =  find_best_centerline_of_vessel_section_on_distance_map(distance_volume, i_points)
    for point_index in range(len(sparse_points) - 2):
        point_pair = [sparse_points[point_index], sparse_points[point_index + 1]]
        resample_points = resample_centerline_between_two_points(distance_volume, point_pair)
        for point in resample_points:
            # print(point)
            x, y, z = point
            dst_volume.set_value_in_ijk(np.array([x, y, z]), 60)
    dst_volume.saveImageAsNIFTI(dst_file)

    exit(0)
    #

    src_volume = VolumeImage()
    src_volume.get_image_from_nifti(src_file)
    dst_volume = ConstantVolumeImage(src_volume,0)
    distance_volume = ConstantVolumeImage(src_volume,0)
    mask_volume = ConstantVolumeImage(src_volume,0)
    root_vessel_point = Vessel_Point()
    root_vessel_point.position_ijk = np.array([369,255,67],dtype=float)
    root_vessel_point.position_geometry = src_volume.convert_ijk_to_geometry(root_vessel_point.position_ijk)
    candidate_vessel_points = []
    FAST_FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_POINT(src_volume,mask_volume,dst_volume,root_vessel_point,10000,1000,candidate_vessel_points)
    dst_volume.saveImageAsNIFTI(dst_file)
    FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_POINT(src_volume,mask_volume,dst_volume,root_vessel_point,10000,1000,candidate_vessel_points)
    dst_volume.saveImageAsNIFTI(dst_file)


    distance_data_array = distance_filter(dst_volume.image_data_array)
    distance_data_array = scipy.ndimage.gaussian_filter(distance_data_array,sigma=1.3,radius=3)
    distance_volume.set_image_data_array(distance_data_array)
    distance_volume.saveImageAsNIFTI(distance_file)
    file = "D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\\section_points_array.csv"
    np.save("D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\\section_points_array.csv",i_points_array)
    for point_index, vessel_section in enumerate(candidate_vessel_points):
        i_points_array = [np.array(point.position_ijk) for point in vessel_section]
        i_centerline_points = find_best_centerline_of_vessel_section_on_distance_map(src_volume, distance_volume, i_points_array)
        for point in i_centerline_points:
            print(point)
            x,y,z = point
            dst_volume.set_value_in_ijk(np.array([x,y,z]), 60 + point_index)
    dst_volume.saveImageAsNIFTI(dst_file)
    exit(0)


    src_volume.get_image_from_nifti("D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_dst_50k.nii.gz")
    distance_volume = VolumeImage()
    distance_volume.get_image_from_nifti("D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_dst_gaussian.nii.gz")

    mask_volume = ConstantVolumeImage(src_volume,0)

    # n_x,n_y,n_z = np.where(src_volume.image_data_array == 32)
    # points = []
    # for index in range(len(n_x)):
    #     points.append(np.array([n_x[index],n_y[index],n_z[index]]))

    rough_centerline_points_array = np.array([
        [368,256,66],
        [353,264,46],

    ])
    dst_data_array = find_best_centerline_of_vessel_section_on_distance_map(src_volume, distance_volume, distance_volume, rough_centerline_points_array)
    # dst_data_array = scipy.ndimage.gaussian_laplace(dst_data_array,sigma=1.3,radius=3) * (-1)
    # dst_data_array[dst_data_array < 30] = 0
    # dst_data_array[dst_data_array >= 30] = 1
    src_volume.saveImageAsNIFTI("D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_dst_distance.nii.gz")
    exit(0)


    src_volume = VolumeImage()
    src_volume.get_image_from_nifti("D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_dst_50k.nii.gz")
    dst_volume = ConstantVolumeImage(src_volume,0)
    for i in range(0,1):
        dst_array,_ = StayConnectedErodeFilter(src_volume)
    dst_volume.set_image_data_array(dst_array)
    dst_volume.saveImageAsNIFTI("D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_dst_50k_connected_erode.nii.gz")

    exit(0)

    src_volume = VolumeImage()
    src_volume.get_image_from_nifti("D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_dst_50k.nii.gz")
    dst_data_array = distance_filter(src_volume.image_data_array)
    dst_data_array = scipy.ndimage.gaussian_filter(dst_data_array,sigma=1.3,radius=3)
    dst_data_array = scipy.ndimage.gaussian_laplace(dst_data_array,sigma=1.3,radius=3) * (-1)
    dst_data_array[dst_data_array < 30] = 0
    dst_data_array[dst_data_array >= 30] = 1
    src_volume.set_image_data_array(dst_data_array )
    src_volume.saveImageAsNIFTI("D:\Coding\\0_data\DAS_Benchmark\AI-20181212002402(L-C4)\dsa_dst_log.nii.gz")


    exit(0)

    src_volume = VolumeImage()
    src_volume.get_image_from_nifti(src_file)
    dst_volume = ConstantVolumeImage(src_volume,0)
    mask_volume = ConstantVolumeImage(src_volume,0)
    root_vessel_point = Vessel_Point()
    root_vessel_point.position_ijk = np.array([369,255,67],dtype=float)
    root_vessel_point.position_geometry = src_volume.convert_ijk_to_geometry(root_vessel_point.position_ijk)
    candidate_vessel_points = []
    FindRoughVesselBasedonRegionGrow3D_with_MAX_SEARCH_POINT(src_volume,mask_volume,dst_volume,root_vessel_point,10000,1000,candidate_vessel_points)

    exit(0)
    # chkpt
    src_volume = VolumeImage()
    src_volume.get_image_from_nifti(dst_file)
    dst_volume = ConstantVolumeImage(src_volume,0)


    exit(0)

