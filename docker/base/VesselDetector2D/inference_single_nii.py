import torch
import os
import numpy as np
import monai
import nibabel as nib
from torchvision.transforms.functional import resize
import re
import matplotlib.pyplot as plt

import torchvision.transforms as transforms
import torchio as ti
from monai.transforms import (
    Activations,
    EnsureChannelFirstd,
    AsDiscrete,
    Compose,
    LoadImaged,
    RandCropByPosNegLabeld,
    RandRotate90d,
    ScaleIntensityd,
    SqueezeDimd,
)
def load_model(model_path):
    """
    载入训练好的模型
    """
    model = monai.networks.nets.UNet(
        spatial_dims=2,
        in_channels=1,
        out_channels=1,
        channels=(16, 32, 64, 128, 256,512),
        strides=(2, 2, 2, 2,2),
        num_res_units=2,
    ) # 初始化模型，确保这里的UNet与你训练时的模型结构一致

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model.load_state_dict(torch.load(model_path,map_location=torch.device('cpu')))
    model = model.to(device)
    model.eval() # 切换到评估模式
    return model



def generateSameSizeImage(image, global_spacing = 1 ,global_size = [1536,1536,1], crop_ratio = 0):
    
    H,W,D = global_size
    origin_shape = image.shape
    origin_spacing = image.spacing
    transform = ti.transforms.Resample(global_spacing,image_interpolation="nearest",label_interpolation="nearest")
    image = transform(image)
    MODE = 2
    if MODE == 1:
        #same size
        D = max(D,image.shape[-1])
        transform = ti.transforms.CropOrPad((H,W,D),padding_mode = int(image.data.min()))
        image = transform(image) 
    else:
        transform = ti.transforms.Resize((H,W,D),image_interpolation="nearest",label_interpolation="nearest")
        image = transform(image)
    image.data = image.data[:1,:,:,:].float()
    return image,origin_shape,origin_spacing


def predict(model, input_path, pred_path):
    """
    对指定路径的nii.gz文件进行推理，并保存预测结果
    """
    # 读取nii文件
    origin_image = ti.ScalarImage(input_path)
    # print(origin_image.shape)
    img_preprocessed,origin_shape,origin_spacing = generateSameSizeImage(origin_image)
    img_preprocessed = ti.ZNormalization()(img_preprocessed)
    # img_preprocessed = origin_image

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    

    
    img_preprocessed.data = np.transpose(img_preprocessed.data.float() , (0,3, 1, 2))
    # print(img_preprocessed.shape)
    
    # img_preprocessed.save(os.path.join(os.getcwd(),"test.nii.gz"))
    # 推理
    with torch.no_grad():
        input = img_preprocessed.data.to(device)
        pred = model(input)
        pred = Compose([Activations(sigmoid=True), AsDiscrete(threshold=0.5)])(pred)
        pred = pred.cpu().numpy()
    pred = np.transpose(pred, (0,2, 3, 1)) 
    img_preprocessed.data = pred 
    
    # 将预测结果恢复到原始尺寸
    pred_resized,_,_ = generateSameSizeImage(img_preprocessed,origin_spacing,origin_shape[1:])
        
    # plt.imshow(pred_resized.data[0,:,:,0], cmap='gray')  # 使用灰度图显示
    # plt.axis('off')  # 不显示坐标轴
    # plt.savefig("/home/zou/vessel_seg/inference_nii/output/pre.jpg", bbox_inches='tight', pad_inches=0)
    # plt.close() 
    # 将预测结果转换为numpy数组，并保存为nii.gz文件
    # 假设模型输出是一个sigmoid激活，因此我们取大于0.5的作为预测结果
    case_name = os.path.basename(input_path).replace('.nii.gz', '_pred.nii.gz')
    # pred_resized.save(os.path.join(pred_path,case_name))
    pred_resized.save(pred_path)
    # output_image = transforms.ToPILImage()(pred[0,:,:,0])
    # output_image.save("/home/zou/vessel_seg/inference_nii/output/output.jpg")

    
def main(model_path, data_folder, output_folder):
    model = load_model(model_path)
    pattern = re.compile(r'^case_\d+\.nii\.gz$')
    for file in os.listdir(data_folder):
        if pattern.match(file):
            input_path = os.path.join(data_folder, file)
            print("inference case :",input_path)
            predict(model, input_path, output_folder)
            print("Save prediction file in ",output_folder)


if __name__ == "__main__":
    model_path = 'C:\\Users\\Administrator\\Desktop\\vessel_seg\\best_metric_model_segmentation2d_dict_150.pth'
    data_folder = 'C:\\Users\\Administrator\\Desktop\\vessel_seg\\roi'
    model = load_model(model_path)

    for case in os.listdir(data_folder):
        case_dir = os.path.join(data_folder,case)
        src_file = os.path.join(case_dir,case + ".nii.gz")
        predict(model, src_file, case_dir)